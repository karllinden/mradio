#!/usr/bin/env python
# encoding: utf-8

# FIXME: pkgconfig

from __future__ import print_function

import os
import sys

from mradio_waf import deps
from mradio_waf import module as mm
from mradio_waf.msg import print_error

from waflib import Logs, Options, TaskGen, Utils
from waflib.Tools import waf_unit_test

VERSION='1.0.0'
APPNAME='mradio'

# these variables are mandatory ('/' are converted automatically)
top = '.'
out = 'build'

auto_options = []
extra_cflags = [
    '-fipa-pure-const',
    '-fstrict-overflow'
]
warning_cflags = [
    '-Wall',
    '-Wconversion',
    '-Winline',
    '-Wmissing-declarations',
    '-Wshadow',
    '-Wsign-compare',
    '-Wsign-conversion',
    '-Wstrict-overflow=5',
    '-Wsuggest-attribute=const',
    '-Wsuggest-attribute=pure',
    '-Wtype-limits',
    '-pedantic'
]

class AutoOption(deps.Deps):
    """
    This class is the foundation for the auto options. It adds an option
    --foo=no|yes to the list of options and deals with all logic and
    checks for these options.

    Each option can have different dependencies that will be checked. If
    all dependencies are available and the user has not done any request
    the option will be enabled. If the user has requested to enable the
    option the class ensures that all dependencies are available and
    prints an error message otherwise. If the user disables the option,
    i.e. --foo=no, no checks are made.

    For each option it is possible to add packages that are required for
    the option using the add_package function. For dependency programs
    add_program should be used. For libraries (without pkg-config
    support) the add_library function should be used. For headers the
    add_header function exists. If there is another type of requirement
    or dependency the check hook (an external function called when
    configuring) can be used.

    When all checks have been made and the class has made a decision the
    result is saved in conf.env['NAME'] where 'NAME' by default is the
    uppercase of the name argument to __init__, but it can be changed
    with the conf_dest argument to __init__.

    The class will define a preprocessor symbol with the result. The
    default name is HAVE_NAME, but it can be changed using the define
    argument to __init__.
    """

    def __init__(self, opt, name, help, conf_dest=None, define=None):
        self.help = help
        self.option = '--' + name

        self.configured = False
        self.retvalue = True
        self.required_options = []

        deps.Deps.__init__(self, self.option)

        self.dest = 'auto_option_' + name
        if conf_dest:
            self.conf_dest = conf_dest
        else:
            self.conf_dest = name.upper()
        if not define:
            self.define = 'HAVE_' + name.upper()
        else:
            self.define = define
        opthelp = self.help + ' (enabled by default if possible)'
        opt.add_option(self.option, type='string', default='auto',
                       dest=self.dest, help=opthelp, metavar='no|yes')

    def add_option(self, option):
        """
        Some auto options require other options to be set. This function
        register a required auto option for this auto option.
        """
        self.required_options.append(option)

    def required_options_fulfilled(self, conf, require):
        missing = []
        for opt in self.required_options:
            opt.configure(conf)
            if not opt.result:
                missing.append(opt)
        if require:
            for opt in missing:
                print_error('%s requires %s to be enabled' %
                            (self.option, opt.option))
        if missing:
            return False
        else:
            return True

    def configure(self, conf):
        """
        This function configures the option examining the argument given
        too --foo (where foo is this option). This function sets
        self.result to the result of the configuration; True if the
        option should be enabled or False if not. If not all
        dependencies were found self.result will shall be False.
        conf.env['NAME'] will be set to the same value aswell as a
        preprocessor symbol will be defined according to the result.

        If --foo[=yes] was given, but some dependency was not found an
        error message is printed (foreach missing dependency).

        This function returns True on success and False on error.
        """

        # If this option is already configured do not configure again.
        if self.configured:
            return self.retvalue

        argument = getattr(Options.options, self.dest)
        if argument == 'no':
            self.result = False
            self.retvalue = True
        elif argument == 'yes':
            self.retvalue = self.check(conf, require=True)
            if self.retvalue:
                self.result = self.required_options_fulfilled(conf, True)
                self.retvalue = self.result
        elif argument == 'auto':
            self.retvalue = True
            self.check(conf, require=False)
            if self.result:
                self.result = self.required_options_fulfilled(conf, False)
        else:
            print_error('Invalid argument "' + argument + '" to ' +
                        self.option)
            self.result = False
            self.retvalue = False

        conf.env[self.conf_dest] = self.result
        if self.result:
            conf.define(self.define, 1)
        else:
            conf.define(self.define, 0)

        self.configured = True
        return self.retvalue

    def display_message(self, conf):
        """
        This function displays a result message with the help text and
        the result of the configuration.
        """
        if build:
            conf.msg(self.help, 'yes', 'GREEN')
        else:
            conf.msg(self.msg, 'no', 'YELLOW')

# This function adds an option to the list of auto options and returns
# the newly created option.
def add_auto_option(opt, name, help, conf_dest=None, define=None):
    option = AutoOption(opt, name, help, conf_dest=conf_dest,
                        define=define)
    auto_options.append(option)
    return option

# This function applies a hack that for each auto option --foo=no|yes
# replaces any occurence --foo in argv with --foo=yes, in effect
# interpreting --foo as --foo=yes. The function has to be called before
# waf issues the option parser, i.e. before the configure phase.
def auto_options_argv_hack():
    for option in auto_options:
        for x in range(1, len(sys.argv)):
            if sys.argv[x] == option.option:
                sys.argv[x] += '=yes'

# This function configures all auto options. It stops waf and prints an
# error message if there were unsatisfied requirements.
def configure_auto_options(conf):
    ok = True
    for option in auto_options:
        if not option.configure(conf):
            ok = False
    if not ok:
        conf.fatal('There were unsatisfied requirements.')

# This function displays all options and the configuration result.
def display_auto_options_messages(conf):
    for option in auto_options:
        option.display_message(conf)

def options(opt):
    opt.load('compiler_c')
    opt.load('gnu_dirs')
    opt.load('waf_unit_test')

    opt.add_option('--debug', action='store_true', default=False,
                   dest='debug',
                   help='Enable debug messages and run-time self-tests')

    opt.add_option('--no-bash', action='store_true', default=False,
                   dest='no_bash', help='Do not install mradiobash')

    # options with third party dependencies
    ltdl = add_auto_option(opt, 'ltdl',
                           'Build dynamic module loading interface')
    ltdl.add_header('ltdl.h')
    ltdl.add_library('ltdl')

    # this must be called before the configure phase
    auto_options_argv_hack()

    opt.recurse('modules')

# Require atomics to be fully (and thus also lock-freely) implemented.
def check_for_atomics(conf):
    conf.check(header_name='stdatomic.h')
    conf.check(fragment='''
                #include <stdatomic.h>
                int main(void) {
                #ifndef __STDC_NO_ATOMICS__
                    return 0;
                #else
                    return 1;
                #endif
                }''',
               execute=True,
               msg='Checking for __STDC_NO_ATOMICS__',
               okmsg='not defined',
               errmsg='defined')
    type_names = [
        'bool',
        'char',
        'char16_t',
        'char32_t',
        'wchar_t',
        'short',
        'int',
        'long',
        'llong',
        'pointer'
    ]
    for name in type_names:
        conf.check(fragment='''
                    #include <stdatomic.h>
                    int main(void) {
                    #if ATOMIC_%s_LOCK_FREE
                        return 0;
                    #else
                        return 1;
                    #endif
                    }''' % name.upper(),
                   execute=True,
                   msg='Checking if atomic_%s is lock free' % name,
                   okmsg='yes',
                   errmsg='no')

def configure(conf):
    conf.load('compiler_c')
    conf.load('gccdeps')
    conf.load('gnu_dirs')
    conf.load('waf_unit_test')

    conf.env.append_unique('CFLAGS', '-std=gnu11')
    conf.env.append_unique('CFLAGS', extra_cflags)
    conf.env.append_unique('CFLAGS', warning_cflags)

    conf.define('_GNU_SOURCE', 1)
    conf.define('PACKAGE', APPNAME)
    conf.define('VERSION', VERSION)

    # Optional bash detection.
    if not Options.options.no_bash:
        try:
            conf.find_program('bash', var='BASH')
        except conf.errors.ConfigureError:
            conf.env['BASH'] = None
    else:
        conf.env['BASH'] = None

    conf.check_cfg(package='gop-3', uselib_store='GOP',
                   args='--cflags --libs')
    conf.parse_flags('-pthread', 'PTHREAD')

    # configure all auto options
    configure_auto_options(conf)

    # FIXME: uncomment when replacements are tested
    #conf.check(function_name='asprintf', header_name='stdio.h',
    #           mandatory=False)
    #conf.check(function_name='vasprintf', header_name='stdio.h',
    #           mandatory=False)

    conf.check(function_name='lockf', header_name='unistd.h',
               mandatory=False)

    check_for_atomics(conf)

    conf.env['MRADIO_MODULE_PATH'] = conf.env['LIBDIR'] + '/mradio'
    conf.env['MRADIO_LIBEXEC'] = conf.env['LIBEXECDIR'] + '/mradio'

    install_dirs = [
        ('PREFIX'            ,'Install prefix'),
        ('BINDIR'            , 'Binary directory'),
        ('LIBDIR'            , 'Library directory'),
        ('MRADIO_MODULE_PATH', 'Module directory'),
        ('LIBEXECDIR'        , 'Library execution directory'),
        ('MRADIO_LIBEXEC'    , 'Command install directory'),
        ('LOCALSTATEDIR'     , 'Local state directory'),
        ('SYSCONFDIR'        , 'System configuration directory'),
    ]
    for name,descr in install_dirs:
        conf.define(name, conf.env[name])

    conf.recurse('modules')

    print()
    print('General configuration')

    if Options.options.debug:
        conf.define('DEBUG', 1)
        conf.define('NDEBUG', 0)
        debugresult = 'yes'
        debugcolor = 'GREEN'
    else:
        conf.define('DEBUG', 0)
        conf.define('NDEBUG', 1)
        debugresult = 'no'
        debugcolor = 'YELLOW'
    conf.msg('Debug messages and run-time self-tests', debugresult,
             color=debugcolor)

    conf.write_config_header('config.h')

    # display configuration result messages for auto options
    display_auto_options_messages(conf)

    print()
    print('Install directories')
    for name,descr in install_dirs:
        conf.msg(descr, conf.env[name], color='CYAN')

@TaskGen.feature('c')
@TaskGen.after_method('apply_incpaths')
def insert_blddir(self):
    includedir = self.bld.path.find_dir('include')
    if includedir:
        self.env.prepend_value('INCPATHS', includedir.abspath())
    self.env.prepend_value('INCPATHS', self.bld.bldnode.abspath())

def build(bld):
    bld.env.append_unique('CFLAGS', '-DHAVE_CONFIG_H=1')
    bld.env.append_unique('CFLAGS', '-DMRADIO_INTERNAL=1')

    bld.recurse('modules')

    lib_sources = [
        'lib/client.c',
        'lib/create-parent.c',
        'lib/info.c',
        'lib/log.c',
        'lib/mradio.c',
        'lib/mstr.c',
        'lib/pipe-client.c',
        'lib/read-file.c',
        'lib/return.c',
        'lib/sigpipe.c',
        'lib/sock.c',
        'lib/sock-client.c',
        'lib/user.c'
    ]

    common_sources = [
        'common/asprintf.c',
        'common/die.c',
        'common/prepend-envvar.c',
        'common/print-version.c',
        'common/run-shell.c'
    ]

    daemon_sources = [
        'daemon/arraypick.c',
        'daemon/avl.c',
        'daemon/bookmark.c',
        'daemon/builtin.c',
        'daemon/builtins.c',
        'daemon/builtin-commands.c',
        'daemon/command.c',
        'daemon/fetcher.c',
        'daemon/main.c',
        'daemon/ml.c',
        'daemon/module.c',
        'daemon/nonblock.c',
        'daemon/pidfile.c',
        'daemon/pipe-serv.c',
        'daemon/player.c',
        'daemon/serv.c',
        'daemon/sock-serv.c'
    ]

    program_sources = [
        'program/mradio.c',
    ]

    cmd_sources = [
        'program/cmd.c',
    ]

    list_sources = [
        'program/list.c'
    ]

    url_sources = [
        'program/url.c',
    ]

    genbuiltin = bld.srcnode.find_resource('./tools/genbuiltins.py').abspath()
    bld(
        rule='%s %s -o ${TGT}' % (genbuiltin, ' '.join(mm.mradio_builtins)),
        target='daemon/builtins.c',
    )

    bld.shlib(
        target       = 'mradio',
        name         = 'libmradio',
        source       = lib_sources,
        includes     = ['./include', '.'],
        use          = ['PTHREAD'],
        install_path = bld.env['LIBDIR']
    )

    bld.stlib(
        source       = common_sources,
        target       = 'common',
        use          = ['GOP'],
        install_path = False
    )

    bld.program(
        source       = daemon_sources,
        target       = 'mradiod',
        defines      = ['PROGRAM_NAME="mradiod"'],
        includes     = ['./daemon', '.'],
        use          = ['GOP', 'LTDL', 'PTHREAD', 'libmradio', 'common']
                       + mm.mradio_builtins_use,
        install_path = bld.env['BINDIR']
    )

    bld.program(
        source       = program_sources,
        target       = 'mradio',
        defines      = ['PROGRAM_NAME="mradio"'],
        includes     = ['./program', '.'],
        use          = ['GOP', 'libmradio', 'common'],
        install_path = bld.env['BINDIR']
    )

    bld.program(
        source       = cmd_sources,
        target       = 'mradio-cmd',
        defines      = ['PROGRAM_NAME="mradio-cmd"'],
        includes     = ['./program', '.'],
        use          = ['GOP', 'libmradio', 'common'],
        install_path = bld.env['BINDIR']
    )

    fwdname = 'load' # name of the actual forwarding binary
    commands = [ # commands to create (link to fwdname)
        'bookmark',
        'fetcher',
        'logdomain',
        'logfile',
        'loglevel',
        'pause',
        'pidfile',
        'play',
        'resume',
        'stop',
        'syslog',
        'toggle',
        'unload'
    ]
    bld.program(
        source       = 'program/fwd.c',
        target       = fwdname,
        install_path = bld.env['MRADIO_LIBEXEC']
    )
    for cmd in commands:
        bld.symlink_as(bld.env['MRADIO_LIBEXEC'] + '/' + cmd,
                       bld.env['MRADIO_LIBEXEC'] + '/' + fwdname)

    bld.program(
        source       = 'program/nonfatal.c',
        target       = 'nonfatal',
        install_path = bld.env['MRADIO_LIBEXEC']
    )

    bld.program(
        source       = url_sources,
        target       = 'bookmark-url',
        defines      = ['PROGRAM_NAME="bookmark-url"'],
        includes     = ['./program', '.'],
        use          = ['GOP', 'libmradio', 'common'],
        install_path = bld.env['MRADIO_LIBEXEC']
    )

    bld.program(
        source       = list_sources,
        target       = 'bookmark-list',
        defines      = ['PROGRAM_NAME="bookmark-list"'],
        includes     = ['./program', '.'],
        use          = ['GOP', 'libmradio', 'common'],
        install_path = bld.env['MRADIO_LIBEXEC']
    )

    bld.symlink_as(bld.env['MRADIO_LIBEXEC'] + '/cmd',
                   bld.env['BINDIR'] + '/mradio-cmd')

    bld.install_files(bld.env['MRADIO_LIBEXEC'], 'program/die',
                      chmod=Utils.O755)

    if bld.env['BASH']:
        bld(
            features     = 'subst',
            source       = 'mradiobash.in',
            target       = 'mradiobash',
            chmod        = Utils.O755,
            install_path = bld.env['BINDIR'],
            BASH         = bld.env['BASH'],
            BINDIR       = bld.env['BINDIR']
        )

    bld.install_files(bld.env['SYSCONFDIR'], 'data/mradio')

    # Unit tests
    bld.stlib(
        source       = ['tests/rng.c'],
        target       = 'test',
        includes     = ['./tests', '.'],
        use          = ['GOP', 'libmradio'],
        install_path = False
    )
    bld.program(
        features     = 'test',
        source       = [
                        'daemon/arraypick.c',
                        'tests/arraypick-test.c',
                       ],
        target       = 'arraypick-test',
        includes     = ['./tests', '.'],
        use          = ['GOP', 'libmradio'],
        install_path = False
    )
    bld.program(
        features     = 'test',
        source       = [
                        'daemon/avl.c',
                        'tests/avl-test.c',
                       ],
        target       = 'avl-test',
        includes     = ['./tests', '.'],
        use          = ['GOP', 'libmradio', 'test'],
        install_path = False
    )
    bld.program(
        features     = 'test',
        source       = [
                        'daemon/ml.c',
                        'tests/ml-fd-test.c',
                       ],
        target       = 'ml-fd-test',
        defines      = ['ML_NO_SIGNAL=1'],
        includes     = ['./tests', '.'],
        use          = ['GOP', 'libmradio', 'test'],
        install_path = False
    )
    bld.program(
        features     = 'test',
        source       = [
                        'daemon/avl.c',
                        'daemon/bookmark.c',
                        'lib/mstr.c',
                        'tests/bookmark-test.c',
                       ],
        target       = 'bookmark-test',
        defines      = ['BOOKMARK_NO_ML=1'],
        includes     = ['./tests', '.'],
        use          = ['GOP', 'libmradio', 'test'],
        install_path = False
    )


    bld.add_post_fun(waf_unit_test.summary)
    bld.add_post_fun(waf_unit_test.set_exit_code)
