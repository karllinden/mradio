#!/usr/bin/env python
#
#  This file is part of mradio.
#
#  Copyright (C) 2015-2016 Karl Linden <karl.j.linden@gmail.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#

import argparse
import sys

includedir = 'mradio'

prefix = 'mradio_'
uprefix = prefix.upper()
prefixlen = len(prefix)

maxlen        = 0
defines       = [] # name
flmacros      = [] # name
flmacros_args = []
typedefs      = []

def error(err):
    print(__file__ + ': error: ' + err)
    exit(1)

def remove_multi_line_comments(content):
    pieces = content.split('/*')
    content = pieces[0]
    n = len(pieces)
    for i in range(1,n):
        tmp = pieces[i].split('*/')
        if len(tmp) != 2:
            error('/* in multiline comment or unterminated multiline ' +
                  'comment')
        content += tmp[1]
    return content

def tolines(content):
    lines = content.split('\n')
    n = len(lines)
    i = 0
    while i < n:
        while lines[i].endswith(' '):
            lines[i] = lines[i][:-1]
        if lines[i].endswith('\\'):
            if i + 1 == n:
                error('\\ without following line')
            else:
                lines[i] = lines[i][:-1]
                lines[i] += lines[i+1]
                lines.pop(i+1)
                n -= 1
        else:
            i += 1
    return lines

def remove_one_line_comments(content):
    lines = tolines(content)
    content = ''
    for line in lines:
        content += line.split('//')[0] + '\n'
    return content

def remove_empty(tup):
    while True:
        try:
            tup.remove('')
        except ValueError:
            break

def split_and_remove(string, chars):
    s1 = [string]
    for char in chars:
        s2 = []
        for p in s1:
            p = p.split(char)
            remove_empty(p)
            s2.extend(p)
        s1 = s2
    return s1

def extract_preproc(line):
    line = line[1:]
    line = split_and_remove(line, ' ')
    return line

def add_define(name):
    defines.append(name)
    global maxlen
    l = len(name)
    if l > maxlen:
        maxlen = l

def add_flmacro(name, args):
    flmacros.append(name)
    flmacros_args.append(args)
    l = len(name) + 2 # "name()"
    if args:
        for arg in args:
            l += len(arg) + 2 # "arg, "
        l -= 2 # remove the last ", "
    global maxlen
    if l > maxlen:
        maxlen = l

def handle_define(preproc):
    name = preproc[1]
    if name.startswith(prefix) or name.startswith(uprefix):
        tmp = name.split('(')
        name = tmp[0]
        if len(tmp) == 1: # non-function-like
            add_define(name)
        elif len(tmp) == 2: # function-like
            argstr = tmp[1]
            i = 2
            while not argstr.endswith(')'):
                argstr += preproc[i]
                i += 1
            argstr = argstr[:-1]
            add_flmacro(name, argstr.split(','))
        else: # invalid
            error('cannot interpret macro %s' % name)
    elif not name.startswith('_' + prefix) and not name.startswith('_' + uprefix):
        error('%s is not in namespace' % name)

def interpret_preproc(filename, content):
    lines = tolines(content)
    content = ''
    expguard = '_' + uprefix + filename.split('/')[-1].upper().replace('-', '_').replace('.', '_') + '_'

    iflevel = 0
    guard = None
    for line in lines:
        if line.startswith('#'):
            preproc = extract_preproc(line)
            if preproc[0] == 'if' or preproc[0] == 'ifdef' or preproc[0] == 'ifndef':
                iflevel += 1
            elif preproc[0] == 'endif':
                iflevel -= 1
            elif iflevel == 1:
                if preproc[0] == 'define':
                    handle_define(preproc)
            if not guard:
                if preproc[0] == 'ifndef':
                    guard = preproc[1]
                    if guard != expguard:
                        error('guard in %s is %s but should be %s' %
                              (filename, guard, expguard))
                else:
                    error('%s lacks a header guard' % filename)
        else:
            content += line + '\n'
    return content

def interpret_code(code):
    global maxlen
    statements = code.split(';')
    igncount = 0 # the number of } to find before continuing as usual
    for s in statements:
        s = split_and_remove(s, ' \t\n')
        if not s:
            continue
        if s[0] == 'struct' or igncount > 0: # struct
            # There must be a leading { to pair with a }
            for p in s:
                if '{' in p:
                    igncount += 1
                if '}' in p:
                    igncount -= 1
        elif s[0] == 'enum':
            n = len(s)
            i = 0
            while i < n and not '{' in s[i]:
                i += 1
            p = s[i].split('{')[1]
            q = ''.join(s[i:])
            q = q.split('{')[1]
            q = q.split('}')[0]
            for enum in q.split(','):
                e = enum.split('=')[0]
                add_define(e)
        elif s[0] == 'typedef': # typedef
            name = None
            for p in s:
                if '(' in p:
                    name = p[1:].split(')')[0]
                    break
            if not name:
                name = s[-1]
            typedefs.append(name)
            # The prefixlen is later stripped of so it must be counted
            # twice.
            l = len(name) + prefixlen
            if l > maxlen:
                maxlen = l
        else: # function declaration or variable declaration
            name = None
            n = len(s)
            for i in range(0, n):
                if '(' in s[i] or '[' in s[i]:
                    if '(' in s[i]: # function declaration
                        p = s[i].split('(')
                    elif '[' in s[i]: # array declaration
                        p = s[i].split('[')
                    if p[0] != '':
                        name = p[0]
                    else:
                        name = s[i-1]
                    break
            if name: # function declaration
                add_define(name)
            else: # variable declaration
                add_define(s[-1])

def generate(out, filename):
    if filename.endswith('.h'):
        can = filename[:-2]
    else:
        error('input file is not a header')
    can = can.upper().replace('-', '_').replace('.', '_')
    guard = '_' + uprefix + can + '_INTERNAL_H_'

    out.write('/*\n' +
              ' * Autogenerated from ' + filename + ' by ' +
              __file__.split('/')[-1] + '.\n' +
              ' * Do not edit. Manual changes will be lost.\n' +
              ' */\n')
    out.write('#ifndef %s\n' % guard)
    out.write('# define %s 1\n\n' % guard)
    out.write('# include <%s/%s>\n\n' % (includedir, filename))

    global maxlen
    maxlen -= prefixlen

    for define in defines:
        out.write('# define %s %s\n' %
                  (define[prefixlen:].ljust(maxlen), define))
    if defines:
        out.write('\n')

    n = len(flmacros)
    for i in range(0, n):
        flmacro = flmacros[i]
        args = flmacros_args[i]
        l = maxlen
        out.write('# define %s(' % flmacro[prefixlen:])
        l -= len(flmacro) - prefixlen + 2 # for the last ) too
        if args:
            out.write(args[0])
            l -= len(args[0])
            for arg in args[1:]:
                out.write(', %s' % arg)
                l -= len(arg) + 2
        out.write(')')
        out.write((l+1) * ' ')
        out.write('%s(' % flmacro)
        if args:
            if args[0] == '...':
                out.write('__VA_ARGS__')
            else:
                out.write(args[0])
            for arg in args[1:]:
                out.write(', ')
                if arg == '...':
                    out.write('__VA_ARGS__')
                else:
                    out.write(arg)
        out.write(')')
        out.write('\n')
    if flmacros:
        out.write('\n')

    for typedef in typedefs:
        #         '# define '
        out.write('typedef  %s %s;\n' %
                  (typedef.ljust(maxlen), typedef[prefixlen:]))
    if typedefs:
        out.write('\n')

    out.write('#endif /* !%s */\n' % guard)

def main():
    parser = argparse.ArgumentParser(description='Generate internalization header file.')
    parser.add_argument('filename', metavar='FILENAME',
                        help='input header')
    parser.add_argument('-o', '--output', dest='output',
                        default=None,
                        help='output header file')
    args = parser.parse_args()

    f = open(args.filename)
    content = f.read()
    f.close()

    content = remove_multi_line_comments(content)
    content = remove_one_line_comments(content)

    content = interpret_preproc(args.filename, content)
    interpret_code(content)

    out = sys.stdout
    if args.output is not None:
        out = open(args.output, mode='w')
    generate(out, args.filename.split('/')[-1])
    if out is not sys.stdout:
        out.close()

if __name__ == "__main__":
    main()
