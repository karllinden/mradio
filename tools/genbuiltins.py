#!/usr/bin/env python
#
#  This file is part of mradio.
#
#  Copyright (C) 2015-2016 Karl Linden <karl.j.linden@gmail.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#

import argparse
import sys

def generate(out, builtins):
    maxlen = 0

    builtins.sort()
    out.write('/* Generated by %s. */\n'
              '\n'
              '#if HAVE_CONFIG_H\n'
              '# include <config.h>\n'
              '#endif /* HAVE_CONFIG_H */\n'
              '\n'
              '#include <stddef.h>\n'
              '\n'
              '#include <mradio/module.h>\n'
              '\n'
              '#include "builtin.h"\n'
              '#include "builtins.h"\n'
              '\n' % __file__.split('/')[-1])
    for b in builtins:
        out.write('extern const module_info_t %s_info;\n' % b)
        maxlen = max(maxlen, len(b))
    if builtins:
        out.write('\n')
    out.write('const builtin_t builtins[] = {\n')
    for b in builtins:
        out.write('    {"%s", %s&%s_info},\n' % (b, ' ' * (maxlen - len(b)), b))
    # Write a null builtin (which does not count in builtins_size) to
    # avoid an array of size 0.
    out.write('    {NULL, NULL}\n')
    out.write('};\n'
              'const size_t builtins_size = %d;\n' % len(builtins))

def main():
    parser = argparse.ArgumentParser(description='Generate internalization header file.')
    parser.add_argument('builtins', metavar='BUILTIN', nargs='*',
                        help='builtin module')
    parser.add_argument('-o', '--output', dest='output',
                        default=None,
                        help='output header file')
    args = parser.parse_args()

    out = sys.stdout
    if args.output is not None:
        out = open(args.output, mode='w')
    generate(out, args.builtins)
    if out is not sys.stdout:
        out.close()

if __name__ == "__main__":
    main()
