/*
 * This file is part of mradio.
 *
 * Copyright (C) 2016-2017 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#ifndef _MRADIO_MRADIO_H_
# define _MRADIO_MRADIO_H_ 1

# include <mradio/common.h>
# include <mradio/info.h>
# include <mradio/return.h>

typedef struct mradio_s mradio_t;

/* Spawns a new server respecting the environment configuration as a
 * child process. */
mradio_t * mradio_spawn     (void);

/* Spawns a private server (with no respect to environment variables) as
 * a child process. */
mradio_t * mradio_spawn_priv(void);

/* Connects to an already online server. If this fails and autospawn is
 * non-zero a server is spawned instead. */
mradio_t * mradio_connect   (mradio_info_t * info,
                             int autospawn);

/* Connect to a server that is hooked up to standard I/O. */
mradio_t * mradio_stdio     (void);

/* Disconnect an mradio client from the server (without telling the
 * server to stop). */
void       mradio_disconnect(mradio_t * mradio);

int        mradio_command   (mradio_t * mradio,
                             uint8_t argc,
                             const char * argv[],
                             mradio_return_t ** retp) MRADIO_NONNULL;

#endif /* !_MRADIO_MRADIO_H_ */
