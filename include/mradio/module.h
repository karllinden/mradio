/*
 * This file is part of mradio.
 *
 * Copyright (C) 2015-2017 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#ifndef _MRADIO_MODULE_H_
# define _MRADIO_MODULE_H_ 1

# include <mradio/player.h>

enum mradio_module_type_e {
    MRADIO_MODULE_TYPE_DECODER,
    MRADIO_MODULE_TYPE_FETCHER,
    MRADIO_MODULE_TYPE_OUTPUT
};
typedef enum mradio_module_type_e mradio_module_type_t;

typedef int  (mradio_module_init_fn_t)  (void);
typedef void (mradio_module_deinit_fn_t)(void);

struct mradio_module_info_s
{
    mradio_module_type_t        type;
    mradio_module_init_fn_t *   init;
    mradio_module_deinit_fn_t * deinit;
};
typedef struct mradio_module_info_s mradio_module_info_t;

typedef int (mradio_fetch_fn_t)(mradio_player_t *);

/* Info structure that fetchers should use.
 * Must be compatible with mradio_module_info_t. */
struct mradio_fetcher_info_s
{
    mradio_module_type_t        type; /* MRADIO_MODULE_TYPE_FETCHER */
    mradio_module_init_fn_t *   init;
    mradio_module_deinit_fn_t * deinit;
    mradio_fetch_fn_t *         fetch;
};
typedef struct mradio_fetcher_info_s mradio_fetcher_info_t;

# if MRADIO_INTERNAL
#  include <mradio/module-internal.h>
# endif /* MRADIO_INTERNAL */

#endif /* !_MRADIO_MODULE_H_ */
