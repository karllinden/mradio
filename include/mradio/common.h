/*
 * This file is part of mradio.
 *
 * Copyright (C) 2014-2016 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#ifndef _MRADIO_COMMON_H_
# define _MRADIO_COMMON_H_

/* non function like */
# define MRADIO_COLD               __attribute__((cold))
# define MRADIO_CONST              __attribute__((const))
# define MRADIO_NONNULL            __attribute__((nonnull))
# define MRADIO_PURE               __attribute__((pure))
# define MRADIO_RETURNS_NONNULL    __attribute__((returns_nonnull))
# define MRADIO_UNUSED             __attribute__((unused))
# define MRADIO_WARN_UNUSED_RESULT __attribute__((warn_unused_result))

/* function like */
# define MRADIO_NONNULLA(...)   __attribute__((nonnull(__VA_ARGS__)))
# define MRADIO_PRINTF(si, ftc) __attribute__((format(printf,si,ftc)))

# define MRADIO_ARRAY_SIZE(array) (sizeof(array)/sizeof(array[0]))

# if MRADIO_INTERNAL
#  include <mradio/common-internal.h>
# endif /* MRADIO_INTERNAL */

#endif /* !_MRADIO_COMMON_H_ */
