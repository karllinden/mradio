/*
 * This file is part of mradio.
 *
 * Copyright (C) 2016 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#ifndef _MRADIO_INFO_H_
# define _MRADIO_INFO_H_ 1

# include <mradio/common.h>

# ifdef unix
#  undef unix
# endif

enum mradio_info_type_e {
    MRADIO_INFO_TYPE_INET = 0,
    MRADIO_INFO_TYPE_UNIX,
    MRADIO_INFO_TYPE_LEN,
    MRADIO_INFO_TYPE_NONE = MRADIO_INFO_TYPE_LEN
};
typedef enum mradio_info_type_e mradio_info_type_t;

struct mradio_info_s {
    mradio_info_type_t type;
    union {
        struct {
            char * addr; /* mstr */
            char * port; /* mstr */
        };
        struct {
            char * unix; /* mstr */
        };
    };
    char * name; /* mstr */
    char * password; /* mstr */
};
typedef struct mradio_info_s mradio_info_t;

const char *       mradio_info_strtype      (mradio_info_type_t type) MRADIO_RETURNS_NONNULL;
const char **      mradio_info_properties   (mradio_info_type_t type);

/* returns mstr */
char *             mradio_info_default      (const char * prop) MRADIO_NONNULL;

/* Initializes info structure to default. */
void               mradio_info_init         (mradio_info_t * info) MRADIO_NONNULL;
void               mradio_info_deinit       (mradio_info_t * info) MRADIO_NONNULL;

mradio_info_type_t mradio_info_get_type     (const mradio_info_t * info) MRADIO_NONNULL;

int                mradio_info_set_name     (mradio_info_t * info,
                                             const char * name) MRADIO_NONNULLA(1);
int                mradio_info_set_password (mradio_info_t * info,
                                             const char * password) MRADIO_NONNULLA(1);

/* returns mstr */
char *             mradio_info_get_name     (const mradio_info_t * name) MRADIO_NONNULL;
char *             mradio_info_get_password (const mradio_info_t * info) MRADIO_NONNULL;
char *             mradio_info_get_prop     (const mradio_info_t * info,
                                             const char * prop) MRADIO_NONNULL;

/* Loads info from environment. */
int                mradio_info_getenv       (mradio_info_t * info) MRADIO_NONNULL;

/* Exports info to environment. */
int                mradio_info_setenv       (mradio_info_t * info) MRADIO_NONNULL;

int                mradio_info_inet         (mradio_info_t * info,
                                             const char * addr,
                                             const char * port) MRADIO_NONNULLA(1);
int                mradio_info_unix         (mradio_info_t * info,
                                             const char * unix) MRADIO_NONNULLA(1);

# if MRADIO_INTERNAL
#  include <mradio/info-internal.h>
# endif /* MRADIO_INTERNAL */

#endif /* !_MRADIO_INFO_H_ */
