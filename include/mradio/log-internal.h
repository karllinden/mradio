/*
 * Autogenerated from log.h by geninternal.py.
 * Do not edit. Manual changes will be lost.
 */
#ifndef _MRADIO_LOG_INTERNAL_H_
# define _MRADIO_LOG_INTERNAL_H_ 1

# include <mradio/log.h>

# define LOG_LEVEL_NONE         MRADIO_LOG_LEVEL_NONE
# define LOG_LEVEL_FATAL        MRADIO_LOG_LEVEL_FATAL
# define LOG_LEVEL_ERROR        MRADIO_LOG_LEVEL_ERROR
# define LOG_LEVEL_WARN         MRADIO_LOG_LEVEL_WARN
# define LOG_LEVEL_INFO         MRADIO_LOG_LEVEL_INFO
# define LOG_LEVEL_DEBUG        MRADIO_LOG_LEVEL_DEBUG
# define LOG_DOMAIN_AVL         MRADIO_LOG_DOMAIN_AVL
# define LOG_DOMAIN_BKMARK      MRADIO_LOG_DOMAIN_BKMARK
# define LOG_DOMAIN_CLIENT      MRADIO_LOG_DOMAIN_CLIENT
# define LOG_DOMAIN_COMMON      MRADIO_LOG_DOMAIN_COMMON
# define LOG_DOMAIN_DAEMON      MRADIO_LOG_DOMAIN_DAEMON
# define LOG_DOMAIN_INFO        MRADIO_LOG_DOMAIN_INFO
# define LOG_DOMAIN_LOG         MRADIO_LOG_DOMAIN_LOG
# define LOG_DOMAIN_ML          MRADIO_LOG_DOMAIN_ML
# define LOG_DOMAIN_MSTR        MRADIO_LOG_DOMAIN_MSTR
# define LOG_DOMAIN_NBLOCK      MRADIO_LOG_DOMAIN_NBLOCK
# define LOG_DOMAIN_PID         MRADIO_LOG_DOMAIN_PID
# define LOG_DOMAIN_RETURN      MRADIO_LOG_DOMAIN_RETURN
# define LOG_DOMAIN_RF          MRADIO_LOG_DOMAIN_RF
# define LOG_DOMAIN_RNG         MRADIO_LOG_DOMAIN_RNG
# define LOG_DOMAIN_RUN         MRADIO_LOG_DOMAIN_RUN
# define LOG_DOMAIN_SERV        MRADIO_LOG_DOMAIN_SERV
# define LOG_DOMAIN_SOCK        MRADIO_LOG_DOMAIN_SOCK
# define LOG_DOMAIN_TEST        MRADIO_LOG_DOMAIN_TEST
# define LOG_DOMAIN_USER        MRADIO_LOG_DOMAIN_USER
# define LOG_DOMAIN_ALL         MRADIO_LOG_DOMAIN_ALL
# define log_open               mradio_log_open
# define log_close              mradio_log_close
# define log_syslog             mradio_log_syslog
# define log_redirect           mradio_log_redirect
# define log_set_level          mradio_log_set_level
# define log_set_name           mradio_log_set_name
# define log_set_domain         mradio_log_set_domain
# define log_set_quiet          mradio_log_set_quiet
# define log_parse_level        mradio_log_parse_level
# define log_level_from_string  mradio_log_level_from_string
# define log_domain_from_string mradio_log_domain_from_string
# define log_print_real         mradio_log_print_real
# define log_getenv             mradio_log_getenv

# define log_print(...)         mradio_log_print(__VA_ARGS__)
# define log_print_errno(...)   mradio_log_print_errno(__VA_ARGS__)
# define log_fatal(...)         mradio_log_fatal(__VA_ARGS__)
# define log_error(...)         mradio_log_error(__VA_ARGS__)
# define log_warn(...)          mradio_log_warn(__VA_ARGS__)
# define log_info(...)          mradio_log_info(__VA_ARGS__)
# define log_debug(...)         mradio_log_debug(__VA_ARGS__)
# define log_fatal_errno(...)   mradio_log_fatal_errno(__VA_ARGS__)
# define log_error_errno(...)   mradio_log_error_errno(__VA_ARGS__)
# define log_warn_errno(...)    mradio_log_warn_errno(__VA_ARGS__)
# define log_info_errno(...)    mradio_log_info_errno(__VA_ARGS__)
# define log_debug_errno(...)   mradio_log_debug_errno(__VA_ARGS__)

typedef  mradio_log_level_t     log_level_t;
typedef  mradio_log_domain_t    log_domain_t;
typedef  mradio_log_func_t      log_func_t;

#endif /* !_MRADIO_LOG_INTERNAL_H_ */
