/*
 * This file is part of mradio.
 *
 * Copyright (C) 2014-2017 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#ifndef _MRADIO_LOG_H_
# define _MRADIO_LOG_H_ 1

# include <stdarg.h>
# include <stdint.h>

# include <mradio/common.h>

/* The MRADIO_LOG_DOMAIN macro must be set to the desired log domain
 * *before* #including. */
/* #define MRADIO_LOG_DOMAIN */

# define mradio_log_print(...)       mradio_log_print_real(0, MRADIO_LOG_DOMAIN, __VA_ARGS__)
# define mradio_log_print_errno(...) mradio_log_print_real(1, MRADIO_LOG_DOMAIN, __VA_ARGS__)

# define mradio_log_fatal(...)       mradio_log_print(MRADIO_LOG_LEVEL_FATAL, __VA_ARGS__)
# define mradio_log_error(...)       mradio_log_print(MRADIO_LOG_LEVEL_ERROR, __VA_ARGS__)
# define mradio_log_warn(...)        mradio_log_print(MRADIO_LOG_LEVEL_WARN,  __VA_ARGS__)
# define mradio_log_info(...)        mradio_log_print(MRADIO_LOG_LEVEL_INFO,  __VA_ARGS__)
# define mradio_log_debug(...)       mradio_log_print(MRADIO_LOG_LEVEL_DEBUG, __VA_ARGS__)

# define mradio_log_fatal_errno(...) mradio_log_print_errno(MRADIO_LOG_LEVEL_FATAL, __VA_ARGS__)
# define mradio_log_error_errno(...) mradio_log_print_errno(MRADIO_LOG_LEVEL_ERROR, __VA_ARGS__)
# define mradio_log_warn_errno(...)  mradio_log_print_errno(MRADIO_LOG_LEVEL_WARN, __VA_ARGS__)
# define mradio_log_info_errno(...)  mradio_log_print_errno(MRADIO_LOG_LEVEL_INFO, __VA_ARGS__)
# define mradio_log_debug_errno(...) mradio_log_print_errno(MRADIO_LOG_LEVEL_DEBUG, __VA_ARGS__)

# define MRADIO_LOG_LEVEL_NONE  0
# define MRADIO_LOG_LEVEL_FATAL 1
# define MRADIO_LOG_LEVEL_ERROR 2
# define MRADIO_LOG_LEVEL_WARN  3
# define MRADIO_LOG_LEVEL_INFO  4
# define MRADIO_LOG_LEVEL_DEBUG 5
typedef uint8_t mradio_log_level_t;

# define MRADIO_LOG_DOMAIN_AVL    0
# define MRADIO_LOG_DOMAIN_BKMARK (MRADIO_LOG_DOMAIN_AVL + 1)
# define MRADIO_LOG_DOMAIN_CLIENT (MRADIO_LOG_DOMAIN_BKMARK + 1)
# define MRADIO_LOG_DOMAIN_COMMON (MRADIO_LOG_DOMAIN_CLIENT + 1)
# define MRADIO_LOG_DOMAIN_DAEMON (MRADIO_LOG_DOMAIN_COMMON + 1)
# define MRADIO_LOG_DOMAIN_INFO   (MRADIO_LOG_DOMAIN_DAEMON + 1)
# define MRADIO_LOG_DOMAIN_LOG    (MRADIO_LOG_DOMAIN_INFO + 1)
# define MRADIO_LOG_DOMAIN_ML     (MRADIO_LOG_DOMAIN_LOG + 1)
# define MRADIO_LOG_DOMAIN_MSTR   (MRADIO_LOG_DOMAIN_ML + 1)
# define MRADIO_LOG_DOMAIN_NBLOCK (MRADIO_LOG_DOMAIN_MSTR + 1)
# define MRADIO_LOG_DOMAIN_PID    (MRADIO_LOG_DOMAIN_NBLOCK + 1)
# define MRADIO_LOG_DOMAIN_RETURN (MRADIO_LOG_DOMAIN_PID + 1)
# define MRADIO_LOG_DOMAIN_RF     (MRADIO_LOG_DOMAIN_RETURN + 1)
# define MRADIO_LOG_DOMAIN_RNG    (MRADIO_LOG_DOMAIN_RF + 1)
# define MRADIO_LOG_DOMAIN_RUN    (MRADIO_LOG_DOMAIN_RNG + 1)
# define MRADIO_LOG_DOMAIN_SERV   (MRADIO_LOG_DOMAIN_RUN + 1)
# define MRADIO_LOG_DOMAIN_SOCK   (MRADIO_LOG_DOMAIN_SERV + 1)
# define MRADIO_LOG_DOMAIN_TEST   (MRADIO_LOG_DOMAIN_SOCK + 1)
# define MRADIO_LOG_DOMAIN_USER   (MRADIO_LOG_DOMAIN_TEST + 1)
# define MRADIO_LOG_DOMAIN_ALL    (MRADIO_LOG_DOMAIN_USER + 1)
typedef uint8_t mradio_log_domain_t;

typedef void (mradio_log_func_t)(int has_errno,
                                 mradio_log_domain_t domain,
                                 mradio_log_level_t level,
                                 const char * fmt,
                                 va_list ap,
                                 void * data);

int                 mradio_log_open              (const char * const logfile)       MRADIO_NONNULL;
void                mradio_log_close             (void);

void                mradio_log_syslog            (int syslog);
int                 mradio_log_redirect          (mradio_log_func_t * const func,
                                                  void * data);

void                mradio_log_set_level         (mradio_log_domain_t domain,
                                                  mradio_log_level_t level);
void                mradio_log_set_name          (const char * const name);
void                mradio_log_set_domain        (int domain);
void                mradio_log_set_quiet         (int quiet);

int                 mradio_log_parse_level       (const char * const string)        MRADIO_NONNULL;

mradio_log_level_t  mradio_log_level_from_string (const char * string)              MRADIO_NONNULL MRADIO_PURE;
mradio_log_domain_t mradio_log_domain_from_string(const char * string)              MRADIO_NONNULL MRADIO_PURE;

void                mradio_log_print_real        (int has_errno,
                                                  mradio_log_domain_t domain,
                                                  mradio_log_level_t level,
                                                  const char * const fmt, ...)      MRADIO_NONNULL MRADIO_PRINTF(4,5);

int                 mradio_log_getenv            (void);

# if MRADIO_INTERNAL
#  include <mradio/log-internal.h>
# endif /* MRADIO_INTERNAL */

#endif /* !_MRADIO_LOG_H_ */
