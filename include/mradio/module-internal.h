/*
 * Autogenerated from module.h by geninternal.py.
 * Do not edit. Manual changes will be lost.
 */
#ifndef _MRADIO_MODULE_INTERNAL_H_
# define _MRADIO_MODULE_INTERNAL_H_ 1

# include <mradio/module.h>

# define MODULE_TYPE_DECODER       MRADIO_MODULE_TYPE_DECODER
# define MODULE_TYPE_FETCHER       MRADIO_MODULE_TYPE_FETCHER
# define MODULE_TYPE_OUTPUT        MRADIO_MODULE_TYPE_OUTPUT

typedef  mradio_module_type_t      module_type_t;
typedef  mradio_module_init_fn_t   module_init_fn_t;
typedef  mradio_module_deinit_fn_t module_deinit_fn_t;
typedef  mradio_module_info_t      module_info_t;
typedef  mradio_fetch_fn_t         fetch_fn_t;
typedef  mradio_fetcher_info_t     fetcher_info_t;

#endif /* !_MRADIO_MODULE_INTERNAL_H_ */
