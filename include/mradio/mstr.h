/*
 * This file is part of mradio.
 *
 * Copyright (C) 2016-2017 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#ifndef _MRADIO_MSTR_H_
# define _MRADIO_MSTR_H_ 1

# include <mradio/common.h>

/* Note 1. mstr strings may be modified if their reference count is 1,
 * i.e. there is no other thread or module that can reference it. There
 * is in general no way to check it, but if it is objvious from the
 * context, such as directly after a call to mstr_alloc, this feature
 * may be used.
 *
 * Note 2. All mstr allocating/creating functions return NULL on failure
 * and a pointer to the string on success. The initial reference count
 * is always 1. */

/* Allocates a new mstr of length LEN. Must be filled with the string
 * which should be of the same length (excluding nul char). */
char *   mradio_mstr_alloc(unsigned len);

/* Creates a new mstr from an ordinary string (whose length must be
 * atleast LEN). The mstr will be truncated to length LEN. */
char *   mradio_mstr_with_len(const char * str,
                              unsigned len)       MRADIO_NONNULL;

/* Creates a new mstr from an ordinary string. */
char *   mradio_mstr         (const char * str) MRADIO_NONNULL;

/* References an mstr. Returns p. */
char *   mradio_mstr_ref     (char * mstr);

/* Unreferences an mstr. When reference count drops to zero the mstr is
 * destroyed. */
void     mradio_mstr_unref   (char * mstr);

unsigned mradio_mstr_len     (char * mstr)      MRADIO_NONNULL MRADIO_PURE;

# if MRADIO_INTERNAL
#  include <mradio/mstr-internal.h>
# endif /* MRADIO_INTERNAL */

#endif /* !_MRADIO_MSTR_H_ */
