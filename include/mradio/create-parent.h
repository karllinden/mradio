/*
 * This file is part of mradio.
 *
 * Copyright (C) 2016 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#ifndef _MRADIO_CREATE_PARENT_H_
# define _MRADIO_CREATE_PARENT_H_ 1

# include <mradio/common.h>

/* Creates the parent directory of a file. */
int mradio_create_parent(const char * filename) NONNULL;

# if MRADIO_INTERNAL
#  include <mradio/create-parent-internal.h>
# endif /* MRADIO_INTERNAL */

#endif /* !_MRADIO_CREATE_PARENT_H_ */
