/*
 * Autogenerated from info.h by geninternal.py.
 * Do not edit. Manual changes will be lost.
 */
#ifndef _MRADIO_INFO_INTERNAL_H_
# define _MRADIO_INFO_INTERNAL_H_ 1

# include <mradio/info.h>

# define INFO_TYPE_INET     MRADIO_INFO_TYPE_INET
# define INFO_TYPE_UNIX     MRADIO_INFO_TYPE_UNIX
# define INFO_TYPE_LEN      MRADIO_INFO_TYPE_LEN
# define INFO_TYPE_NONE     MRADIO_INFO_TYPE_NONE
# define info_strtype       mradio_info_strtype
# define info_properties    mradio_info_properties
# define info_default       mradio_info_default
# define info_init          mradio_info_init
# define info_deinit        mradio_info_deinit
# define info_get_type      mradio_info_get_type
# define info_set_name      mradio_info_set_name
# define info_set_password  mradio_info_set_password
# define info_get_name      mradio_info_get_name
# define info_get_password  mradio_info_get_password
# define info_get_prop      mradio_info_get_prop
# define info_getenv        mradio_info_getenv
# define info_setenv        mradio_info_setenv
# define info_inet          mradio_info_inet
# define info_unix          mradio_info_unix

typedef  mradio_info_type_t info_type_t;
typedef  mradio_info_t      info_t;

#endif /* !_MRADIO_INFO_INTERNAL_H_ */
