/*
 * This file is part of mradio.
 *
 * Copyright (C) 2017 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#ifndef _MRADIO_RETURN_H_
# define _MRADIO_RETURN_H_ 1

# include <mradio/common.h>

enum mradio_return_type_e
{
    MRADIO_RETURN_TYPE_NONE = 0,
    MRADIO_RETURN_TYPE_ARRAY,
    MRADIO_RETURN_TYPE_STRING
};
typedef enum mradio_return_type_e mradio_return_type_t;
typedef struct mradio_return_data_s mradio_return_data_t;
typedef struct mradio_return_s mradio_return_t;

mradio_return_t * mradio_return_new  (unsigned len);
mradio_return_t * mradio_return_ref  (mradio_return_t * r);
void              mradio_return_unref(mradio_return_t * r);
unsigned          mradio_return_len  (const mradio_return_t * r)    MRADIO_NONNULL;
void              mradio_return_set  (mradio_return_t * r,
                                      unsigned index,
                                      mradio_return_type_t type,
                                      void * ptr)                   MRADIO_NONNULL;
void *            mradio_return_get  (const mradio_return_t * r,
                                      unsigned index,
                                      mradio_return_type_t * typep) MRADIO_NONNULL;

# if MRADIO_INTERNAL
#  include <mradio/return-internal.h>
# endif /* MRADIO_INTERNAL */

#endif /* !_MRADIO_RETURN_H_ */
