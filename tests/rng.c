/*
 * This file is part of mradio.
 *
 * Copyright (C) 2015-2016 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#define MRADIO_LOG_DOMAIN MRADIO_LOG_DOMAIN_RNG

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>

#include <mradio/log.h>

#include <common/massert.h>

#include "rng.h"

#define RAND48_MAX 2147483648 /* 2^31, see man drand48 */

struct rng_seed_s {
    unsigned char seed[6];
    unsigned char pos;
};
typedef struct rng_seed_s rng_seed_t;

static const char * rng_seed_hex = NULL;

static gop_callback_t rng_seed_hex_given;

static gop_option_t rng_options[] = {
    {"seed", '\0', GOP_STRING, &rng_seed_hex, &rng_seed_hex_given,
        "Explicitly seed random number generator with SEED", "SEED"},
    GOP_TABLEEND
};

static gop_return_t NONNULL
rng_seed_hex_given(gop_t * const gop)
{
    massert(rng_seed_hex != NULL);

    unsigned short seed[3];
    const size_t len = strlen(rng_seed_hex);

    if (len != 12) {
        log_error("seed %s has invalid length %ld, 12 expect",
                  rng_seed_hex, len);
        goto error;
    }

    if (rng_hex_to_seed(rng_seed_hex, seed)) {
        goto error;
    }

    rng_seed_exact(seed);

    return GOP_DO_CONTINUE;
error:
    gop_set_exit_status(gop, EXIT_FAILURE);
    return GOP_DO_EXIT;
}

static void
rng_seed_init(rng_seed_t * const seed)
{
    memset(seed, 0, sizeof(rng_seed_t));
    return;
}

static void
rng_seed_mix(rng_seed_t * const seed,
             void * const data,
             const size_t size)
{
    unsigned char * const c = data;
    for (size_t i = 0; i < size; ++i) {
        seed->seed[seed->pos] ^= c[i];
        seed->pos++;
        if (seed->pos == 6) {
            seed->pos = 0;
        }
    }
    return;
}

static unsigned short *
rng_seed_get(rng_seed_t * const seed)
{
    return (unsigned short *)seed->seed;
}

void
rng_seed_exact(unsigned short seed[3])
{
    char hex[13];
    rng_seed_to_hex(seed, hex);
    log_info("seeding random number generator with %s", hex);
    seed48(seed);
    return;
}

void
rng_seed(void)
{
    rng_seed_t seed;
    struct timespec ts;
    gid_t gid;
    pid_t pid;
    pid_t ppid;
    uid_t uid;

    /* Refuse to overwrite a user specified seed. */
    if (rng_seed_hex != NULL) {
        return;
    }

    rng_seed_init(&seed);

    clock_gettime(CLOCK_REALTIME, &ts);
    rng_seed_mix(&seed, &ts, sizeof(struct timespec));

    gid = getgid();
    pid = getpid();
    ppid = getppid();
    uid = getuid();
    rng_seed_mix(&seed, &gid, sizeof(uid_t));
    rng_seed_mix(&seed, &pid, sizeof(pid_t));
    rng_seed_mix(&seed, &ppid, sizeof(pid_t));
    rng_seed_mix(&seed, &uid, sizeof(uid_t));

    rng_seed_exact(rng_seed_get(&seed));

    return;
}

/* Returns a random number in the mathematical interval [0,range]. */
static long int
rng_range(long int range)
{
    long int max = RAND48_MAX;
    long int remaining;
    long int r;

    /* Since the number of possible return values (in [0,range]) is
     * range + 1, range must incremented. Remove this to get random
     * integers in the range [0,range). */
    range++;

    /* To get an interval that contains equally many of each restclass
     * an intervall on the form [0,range*n-1], where n is a natural
     * number. Such an interval is [0,max-(max%range)-1)], thus the
     * remaining numbers from max-(max%range)-1 to max need to be
     * removed. */
    remaining = max % range + 1;

    massert(remaining <= range);
    if (remaining != range) {
        max -= remaining;
    }

    do {
        r = lrand48();
    } while (r > max);

    return r % range;
}

long int
rng_max(long int max)
{
    massert(max >= 0);
    return rng_range(max);
}

long int
rng_min_max(long int min, long int max)
{
    massert(min <= max);
    return min + rng_range(max - min);
}

static inline void
rng_ushort_to_hex(unsigned short u, char hex[4])
{
    for (int i = 3; i >= 0; --i) {
        hex[i] = u & 0xf;
        massert(hex[i] >= 0x0);
        massert(hex[i] <= 0xf);
        if (hex[i] < 10) {
            hex[i] = (char)(hex[i] + '0');
        } else {
            hex[i] = (char)(hex[i] - 10 + 'a');
        }
        u >>= 4;
    }
    return;
}

void
rng_seed_to_hex(const unsigned short seed[3], char hex[13])
{
    for (unsigned int i = 0, j = 0; i < 3; ++i, j += 4) {
        rng_ushort_to_hex(seed[i], hex+j);
    }
    hex[12] = '\0';
    return;
}

static inline int
rng_hex_to_ushort(const char hex[4], unsigned short * const res)
{
    unsigned short u = 0;
    for (unsigned int i = 0; i < 4; ++i) {
        u = (unsigned short)(u << 4);
        if (hex[i] >= 'A' && hex[i] <= 'F') {
            u = u | (unsigned short)(10 + hex[i] - 'A');
        } else if (hex[i] >= 'a' && hex[i] <= 'f') {
            u = u | (unsigned short)(10 + hex[i] - 'a');
        } else if (hex[i] >= '0' && hex[i] <= '9') {
            u = u | (unsigned short)(hex[i] - '0');
        } else {
            char hex5[5];
            memcpy(hex5, hex, 4);
            hex5[4] = '\0';
            log_error("invalid char %c in hex %s", hex[i], hex5);
            return 1;
        }
    }
    *res = u;
    return 0;
}

int
rng_hex_to_seed(const char hex[13], unsigned short seed[3])
{
    for (unsigned int i = 0, j = 0; i < 3; ++i, j += 4) {
        if (rng_hex_to_ushort(hex+j, seed+i)) {
            return 1;
        }
    }
    return 0;
}

void NONNULL
rng_add_gop_table(gop_t * const gop)
{
    gop_add_table(gop, "Random number generator options:", rng_options);
    return;
}
