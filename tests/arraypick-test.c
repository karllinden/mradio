/*
 * This file is part of mradio.
 *
 * Copyright (C) 2016 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#define MRADIO_LOG_DOMAIN MRADIO_LOG_DOMAIN_TEST

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <stdlib.h>
#include <string.h>

#include <gop.h>

#include <mradio/log.h>

#include <common/log-debug.h>

#include <daemon/arraypick.h>

#define PROGRAM_NAME "arraypick-test"

#define MAX 4096

typedef struct bt_node_s bt_node_t;
struct bt_node_s
{
    unsigned    key;
    bt_node_t * left;
    bt_node_t * right;
};

static unsigned    size  = 0;
static unsigned    count = 0;

static bt_node_t   nodes[MAX];
static unsigned    next  = 0;
static bt_node_t * root  = NULL;

static int
bt_insert(unsigned key)
{
    bt_node_t ** nodep;
    bt_node_t * node;

    if (next >= MAX)
    {
        log_error("out of nodes");
        return 1;
    }
    node = nodes + next++;
    node->key   = key;
    node->left  = NULL;
    node->right = NULL;

    nodep = &root;
    while (*nodep != NULL)
    {
        if (key < (*nodep)->key)
        {
            nodep = &(*nodep)->left;
        }
        else if (key > (*nodep)->key)
        {
            nodep = &(*nodep)->right;
        }
        else
        {
            log_error("%u picked twice", key);
            next--;
            return 1;
        }
    }
    *nodep = node;

    return 0;
}

/* returns maximum path */
static unsigned
bt_check_node(const bt_node_t * node)
{
    unsigned left_path_max;
    unsigned right_path_max;
    unsigned path_max;
    unsigned diff;
    if (node == NULL)
    {
        return 0;
    }
    else
    {
        left_path_max = bt_check_node(node->left);
        right_path_max = bt_check_node(node->right);
        if (left_path_max >= right_path_max)
        {
            path_max = left_path_max;
            diff = left_path_max - right_path_max;
        }
        else
        {
            path_max = right_path_max;
            diff = right_path_max - left_path_max;
        }
        if (diff > 1)
        {
            log_error("at %u, left_path_max=%u, right_path_max=%u, "
                      "diff = %u",
                      node->key, left_path_max, right_path_max,
                      diff);
            abort();
        }
        path_max++;
        return path_max;
    }
}

static void
bt_check(void)
{
    bt_check_node(root);
    return;
}

static int
pick_func(unsigned index, void * data)
{
    log_debug("pick %u", index);
    if (index >= size)
    {
        log_error("index (%u) >= size (%u)", index, size);
        return 1;
    }
    count++;
    return bt_insert(index);
}

int
main(int argc, char ** argv)
{
    int   exit_status = EXIT_FAILURE;

    log_set_name(PROGRAM_NAME);
    log_set_level(LOG_DOMAIN_ALL, LOG_LEVEL_WARN);
    log_set_level(LOG_DOMAIN_RNG,  LOG_LEVEL_INFO);

    gop_t * gop = gop_new();
    if (gop == NULL)
    {
        goto error;
    }

    gop_set_program_name(gop, PROGRAM_NAME);
    gop_autohelp(gop);
    gop_parse(gop, &argc, argv);
    gop_destroy(gop);

    if (log_getenv())
    {
        goto error;
    }

    for (size = 1; size <= MAX; ++size)
    {
        count = 0;
        next  = 0;
        root  = NULL;

        log_debug("--------------");
        log_debug("size = %u", size);
        log_debug("--------------");
        if (arraypick(size, &pick_func, NULL))
        {
            goto error;
        }
        else if (count != size)
        {
            log_debug("count (%u) != size (%u)", count, size);
            goto error;
        }
        bt_check();
        log_debug(" ");
    }

    exit_status = EXIT_SUCCESS;
error:
    return exit_status;
}
