/*
 * This file is part of mradio.
 *
 * Copyright (C) 2015-2016 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#define MRADIO_LOG_DOMAIN MRADIO_LOG_DOMAIN_TEST

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <stdbool.h>
#include <stdlib.h>

#include <gop.h>

#include <mradio/log.h>

#include <common/massert.h>

#include <daemon/ml.h>

#include "rng.h"

#define PROGRAM_NAME "ml-fd-test"

/* Number of addition runs. */
#define ADD_RUNS 512

/* Maximum number of ml_fds per addition run. */
#define ADD_MAX  256

/* Probability, in percent, that a run is going to be addition (in
 * opposition to a removal). */
#define ADD_RATE 60

static bool * ml_fd_table       = NULL;
static int    ml_fd_table_size  = 0;

static int *  ml_fd_array       = NULL;
static size_t ml_fd_array_len   = 0;
static size_t ml_fd_array_size  = 0;

/* priority queue */
static int *  ml_fd_expect      = NULL;
static size_t ml_fd_expect_len  = 1;
static size_t ml_fd_expect_size = 1;

static int
ml_fd_array_insert(const int ml_fd)
{
    if (ml_fd_array_len == ml_fd_array_size) {
        int * const tmp = ml_fd_array;
        ml_fd_array_size++;
        ml_fd_array = realloc(ml_fd_array,
                              ml_fd_array_size * sizeof(int));
        if (ml_fd_array == NULL) {
            ml_fd_array = tmp;
            ml_fd_array_size--;
            log_error_errno("could not allocate memory");
            return 1;
        }
    }
    ml_fd_array[ml_fd_array_len++] = ml_fd;
    return 0;
}

static int
ml_fd_array_remove_any(void)
{
    const size_t index = (size_t)rng_max((long int)ml_fd_array_len - 1);
    const int    ml_fd = ml_fd_array[index];
    ml_fd_array_len--;
    ml_fd_array[index] = ml_fd_array[ml_fd_array_len];
    return ml_fd;
}

static int
ml_fd_insert(const int ml_fd)
{
    if (ml_fd >= ml_fd_table_size) {
        const int new_size = ml_fd + 1;
        bool * new;
        new = realloc(ml_fd_table, (unsigned)new_size * sizeof(bool));
        if (new == NULL) {
            log_error_errno("could not allocate memory");
            return 1;
        }
        ml_fd_table = new;
        while (ml_fd_table_size < new_size) {
            ml_fd_table[ml_fd_table_size] = false;
            ml_fd_table_size++;
        }
    }

    if (ml_fd_array_insert(ml_fd)) {
        return 1;
    }

    if (ml_fd_table[ml_fd]) {
        log_error("ml_fd=%d already exists", ml_fd);
        return 1;
    }
    ml_fd_table[ml_fd] = true;

    return 0;
}

static int
ml_fd_remove(const int ml_fd)
{
    if (!ml_fd_table[ml_fd]) {
        log_error("ml_fd=%d does not exist", ml_fd);
        return 1;
    }
    ml_fd_table[ml_fd] = false;
    return 0;
}

static int
ml_fd_expect_push(const int ml_fd)
{
    /* If the code reaches this point the ml_fd that was destroyed is
     * in the ml_fds array (in between used ml_fds). Thus, the ml_fd
     * must be added to the priority queue so that it can be reused
     * later on. */
    if (ml_fd_expect_len == ml_fd_expect_size) {
        int * const tmp = ml_fd_expect;
        ml_fd_expect_size++;
        ml_fd_expect =
            realloc(ml_fd_expect,
                    (unsigned)ml_fd_expect_size * sizeof(int));
        if (ml_fd_expect == NULL) {
            log_error_errno("could not allocate memory");
            ml_fd_expect = tmp;
            ml_fd_expect_size--;
            return 1;
        }
    }
    massert(ml_fd_expect_len < ml_fd_expect_size);

    /* Insert ml_fd at the end of the heap and swap it up the heap. */
    size_t this = ml_fd_expect_len;
    size_t next = ml_fd_expect_len >> 1; /* ml_fd_len / 2 */
    while (next > 0 && ml_fd_expect[next] > ml_fd) {
        ml_fd_expect[this] = ml_fd_expect[next];
        this = next;
        next >>= 1; /* next /= 2; */
    }
    ml_fd_expect[this] = ml_fd;
    ml_fd_expect_len++;

    return 0;
}

static int
ml_fd_expect_pull(void)
{
    int ml_fd = -1;

    if (ml_fd_expect_len > 1) {
        /* (1) Pull out the highest priority element at the top of the
         *     heap.
         * (2) Save the last element in the heap array (it should be
         *     re-added somewhere in the heap).
         * (*) Note that there is a gap where either the last element
         *     should be inserted or on of the children should be moved
         *     up.
         * (3) Move children of the top node upwards until the gap
         *     should contain the last element (that is, above the gap
         *     the ml_fd is less, but below the gap ml_fd is more. */
        ml_fd = ml_fd_expect[1];

        const int last = ml_fd_expect[--ml_fd_expect_len];

        /* For a description look at the comment in ml_fd_queue_test. */
        size_t p = 1; /* this is the parent (the gap) */
        size_t l = 2;
        size_t r = 3;

        /* Run as long as there is a right child. */
        while (r < ml_fd_expect_len) {
            massert(ml_fd_expect[l] != ml_fd_expect[r]);

            size_t min; /* the minimum child, either l or r */
            if (ml_fd_expect[l] < ml_fd_expect[r]) {
                min = l;
            } else {
                min = r;
            }

            /* The gap should be filled with either the minimum child
             * (at min) or the last element. For the heap property to
             * hold the lowest one must be picked. If the minimum is
             * picked the new gap (parent) is where min was. */
            massert(ml_fd_expect[min] != last);
            if (ml_fd_expect[min] < last) {
                ml_fd_expect[p] = ml_fd_expect[min];
                p = min;
                l = p << 1; /* p*2 */
                r = l + 1;
            } else {
                /* This trick breaks this loop and disables the
                 * following if-clause. */
                l = ml_fd_expect_len;
                r = ml_fd_expect_len;
            }
        }

        /* It is known there is not a right child, which means
         * r >= ml_fd_queue_len. Now there might be a left child, but
         * the left child does not have any other children since if the
         * left child would have children l*2 < ml_fd_queue_len but
         * l*2 >= r >= ml_fd_queue_len, which is a contradiction. Thus
         * no loop is needed here. */
        if (l < ml_fd_expect_len) {
            massert(ml_fd_expect[l] != last);
            if (ml_fd_expect[l] < last) {
                ml_fd_expect[p] = ml_fd_expect[l];

                /* Since l = 2*p, the assignment p = l is the same as
                 * p <<= 1. */
                p <<= 1;
            }
        }

        massert(p <= ml_fd_expect_len);
        ml_fd_expect[p] = last;
    }

    return ml_fd;
}

static int
add_run(void)
{
    int last = -1;
    int ml_fd;
    int expect;
    for (long int add = rng_max(ADD_MAX); add > 0; add--) {
        expect = ml_fd_expect_pull();
        ml_fd = ml_fd_new(0, NULL);
        if (ml_fd < 0) {
            return 1;
        } else if (expect >= 0 && ml_fd != expect) {
            log_error("the returned ml_fd=%d was not the expected=%d",
                      ml_fd, expect);
            return 1;
        }
        if (ml_fd_insert(ml_fd)) {
            return 1;
        }

        /* The main loop should return the lowest ml_fds first, thus
         * ml_fd must be more than last. */
        if (ml_fd <= last) {
            log_error("ml_fd=%d, but last=%d", ml_fd, last);
            return 1;
        }

        last = ml_fd;

        log_info("added ml_fd=%d", ml_fd);
    }

    return 0;
}

static int
remove_run(void)
{
    int ml_fd;
    for (long int remove = rng_max((long int)ml_fd_array_len);
         remove > 0;
         remove--)
    {
        ml_fd = ml_fd_array_remove_any();
        log_info("removing ml_fd=%d", ml_fd);
        if (ml_fd_remove(ml_fd)) {
            return 1;
        }
        if (ml_fd_destroy(ml_fd)) {
            return 1;
        }
        if (ml_fd_expect_push(ml_fd)) {
            return 1;
        }
    }
    return 0;
}

static int
remove_rest(void)
{
    int ml_fd;
    while (ml_fd_array_len > 0) {
        ml_fd = ml_fd_array_remove_any();
        if (ml_fd_remove(ml_fd)) {
            return 1;
        }
        if (ml_fd_destroy(ml_fd)) {
            return 1;
        }
    }
    return 0;
}

int
main(int argc, char ** const argv)
{
    int          exit_status = EXIT_FAILURE;
    unsigned int add_runs = ADD_RUNS;

    log_set_name(PROGRAM_NAME);
    log_set_level(LOG_DOMAIN_ALL, LOG_LEVEL_WARN);
    log_set_level(LOG_DOMAIN_RNG,  LOG_LEVEL_INFO);

    gop_t * const gop = gop_new();
    if (gop == NULL) {
        goto error;
    }

    gop_set_program_name(gop, PROGRAM_NAME);
    rng_add_gop_table(gop);
    gop_autohelp(gop);
    gop_parse(gop, &argc, argv);
    gop_destroy(gop);

    if (log_getenv())
    {
        goto error;
    }

    rng_seed();

    while (add_runs > 0) {
        long int rate = rng_max(99);
        if (rate < ADD_RATE) {
            add_runs--;
            if (add_run()) {
                goto error;
            }
        } else {
            if (remove_run()) {
                goto error;
            }
        }
    }
    if (remove_rest()) {
        goto error;
    }

    exit_status = EXIT_SUCCESS;
error:
    free(ml_fd_table);
    free(ml_fd_array);
    free(ml_fd_expect);
    ml_free();
    return exit_status;
}
