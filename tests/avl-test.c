/*
 * This file is part of mradio.
 *
 * Copyright (C) 2016 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#define MRADIO_LOG_DOMAIN MRADIO_LOG_DOMAIN_TEST

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#include <gop.h>

#include <mradio/common.h>
#include <mradio/log.h>

#include <common/massert.h>

#include <daemon/avl.h>

#include "rng.h"

#define PROGRAM_NAME "avl-test"

/* Minimum and maximum number of nodes. Make sure it does not overflow
 * an int. */
#define NODES_MIN  65536
#define NODES_MAX 262144

#define KEY_MIN 16
#define KEY_MAX 32

struct node_s {
    char   key[KEY_MAX];
    size_t keylen;
    struct node_s * left;
    struct node_s * right;
};
typedef struct node_s node_t;

static const char allowed[] = {
    'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
    'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'x', 'y', 'z',
    'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
    'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'X', 'Y', 'Z',
    '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'
};

/* Data to be associated with the keys in the AVL tree. */
struct data_s
{
    const node_t * node;
};
typedef struct data_s data_t;

/* Root of the binary tree containing all nodes. */
static node_t *       tree           = NULL;

static node_t *       nodes          = NULL;
static unsigned int   nodes_size     = 0;
static unsigned int   nodes_len      = 0;

static unsigned int * node_queue     = NULL;
static unsigned int   node_queue_len = 0;

static int
cmp_func(const void * k, const void * p, const void * e)
{
    const char *   key = k;
    const data_t * data = p;
    return strcmp(key, data->node->key);
}

static void NONNULL
strgen(char * str, size_t len)
{
    long int max = sizeof(allowed) - 1;
    for (size_t i = 0; i < len; ++i) {
        str[i] = allowed[rng_max(max)];
    }
    return;
}

static void NONNULL
node_keygen(node_t * const node)
{
    strgen(node->key, node->keylen);
    return;
}

/* Insert the new node NODE into the tree rooted at ROOTP.
 * Returns 0 on success or 1 if the key already exists. */
static int
node_insert(node_t * const node)
{
    node_t ** rootp = &tree;
    node_t * root;
    int cmp;

    while ((root = *rootp) != NULL) {
        cmp = strcmp(node->key, root->key);
        if (cmp < 0) {
            rootp = &root->left;
        } else if (cmp > 0) {
            rootp = &root->right;
        } else {
            return 1;
        }
    }
    *rootp = node;

    return 0;
}

static node_t * PURE
node_search(const char * const key)
{
    node_t * node = tree;
    int cmp;

    while (node != NULL) {
        cmp = strcmp(key, node->key);
        if (cmp < 0) {
            node = node->left;
        } else if (cmp > 0) {
            node = node->right;
        } else {
            return node;
        }
    }

    return NULL;
}

static int
node_init(node_t * const node)
{
    node->keylen = (size_t)rng_min_max(KEY_MIN, KEY_MAX);
    strgen(node->key, node->keylen);
    node->key[node->keylen] = '\0';

    node->left = NULL;
    node->right = NULL;

    /* Try to insert the new category into the destination and generate
     * a new key if the key already exist. */
    while (node_insert(node)) {
        node_keygen(node);
    }

    return 0;
}

static int
nodes_alloc(void)
{
    nodes_size = (unsigned int)rng_min_max(NODES_MIN, NODES_MAX);
    nodes = malloc(nodes_size * sizeof(node_t));
    if (nodes == NULL) {
        log_error_errno("could not allocate memory");
        return 1;
    }

    node_queue = malloc(nodes_size * sizeof(unsigned int));
    if (node_queue == NULL) {
        log_error_errno("could not allocate memory");
        return 1;
    }

    return 0;
}

static void
nodes_free(void)
{
    free(nodes);
    nodes = NULL;
    nodes_size = 0;

    free(node_queue);
    node_queue = NULL;
    node_queue_len = 0;

    return;
}

static int
nodes_prepare(void)
{
    if (nodes_alloc()) {
        return 1;
    }

    for (nodes_len = 0; nodes_len < nodes_size; ++nodes_len) {
        if (node_init(nodes + nodes_len)) {
            return 1;
        }
    }

    return 0;
}

static void
node_queue_fill(void)
{
    node_queue_len = nodes_len;
    for (unsigned int i = 0; i < node_queue_len; ++i) {
        node_queue[i] = i;
    }
    return;
}

static const node_t *
node_queue_pull(void)
{
    if (node_queue_len > 0) {
        unsigned int r;
        unsigned int index;

        node_queue_len--;
        r = (unsigned int)rng_max((long int)node_queue_len);
        index = node_queue[r];
        node_queue[r] = node_queue[node_queue_len];

        return nodes + index;
    } else {
        return NULL;
    }
}

static int NONNULL
nodes_insert(avl_t * const avl)
{
    const node_t * node;
    data_t *       data;

    node_queue_fill();
    while ((node = node_queue_pull()) != NULL) {
        data = avl_insert(avl, node->key, sizeof(data_t));
        if (data == NULL)
        {
            return 1;
        }
        data->node = node;
    }

    return 0;
}

static int NONNULL
nodes_find(avl_t * const avl)
{
    const node_t * node;
    const data_t * data;

    node_queue_fill();
    while ((node = node_queue_pull()) != NULL) {
        data = avl_search(avl, node->key);
        if (data == NULL) {
            log_error("expected node at %s", node->key);
            return 1;
        }
        if (data->node != node) {
            log_error("data of %s is incorrect: %p but expected %p",
                      node->key, (void *)data->node, (void *)node);
            return 1;
        }
    }

    return 0;
}

static int NONNULL
nodes_find_non_existing(avl_t * avl)
{
    char         key[KEY_MAX + 1];
    unsigned int keys = (unsigned int)rng_min_max(NODES_MIN, NODES_MAX);
    unsigned int keylen;
    void *       data;

    while (keys--) {
        keylen = (unsigned int)rng_min_max(KEY_MIN, KEY_MAX);
        key[keylen] = '\0';
        do {
            strgen(key, keylen);
        } while (node_search(key) != NULL);
        data = avl_search(avl, key);
        if (data != NULL) {
            log_error("did not expect to find a node at %s", key);
            return 1;
        }
    }

    return 0;
}

static int NONNULL
nodes_delete_some(avl_t * avl)
{
    const node_t * node;
    unsigned       remove = (unsigned)rng_min_max(nodes_len/4,
                                                  nodes_len);
    node_queue_fill();
    while (remove && (node = node_queue_pull()) != NULL)
    {
        if (avl_delete(avl, node->key))
        {
            log_error("expected to remove node at %s", node->key);
            return 1;
        }
        remove--;
    }
    return 0;
}

static int
data_init_func(void * k, void * p, void * e)
{
    data_t * data = p;
    data->node = e;
    return 0;
}

static int NONNULL
nodes_insearch(avl_t * avl)
{
    const node_t * node;
    void *         data;
    node_queue_fill();
    while ((node = node_queue_pull()) != NULL)
    {
        data = avl_insearch(avl, node->key, sizeof(data_t),
                            &data_init_func, (void *)node);
        if (data == NULL)
        {
            log_error("could not insearch %s", node->key);
            return 1;
        }
    }
    return 0;
}

int
main(int argc, char ** argv)
{
    int   exit_status = EXIT_FAILURE;
    avl_t avl;

    avl_init(&avl, &cmp_func, NULL, NULL);

    log_set_name(PROGRAM_NAME);
    log_set_level(LOG_DOMAIN_ALL, LOG_LEVEL_WARN);
    log_set_level(LOG_DOMAIN_RNG,  LOG_LEVEL_INFO);

    gop_t * const gop = gop_new();
    if (gop == NULL)
    {
        goto error;
    }

    gop_set_program_name(gop, PROGRAM_NAME);
    rng_add_gop_table(gop);
    gop_autohelp(gop);
    gop_parse(gop, &argc, argv);
    gop_destroy(gop);

    if (log_getenv())
    {
        goto error;
    }

    rng_seed();

    if (nodes_prepare())
    {
        goto error;
    }

    log_info("inserting nodes");
    if (nodes_insert(&avl)) {
        goto error;
    }

    log_info("finding nodes");
    if (nodes_find(&avl))
    {
        goto error;
    }

    log_info("finding non-existing");
    if (nodes_find_non_existing(&avl))
    {
        goto error;
    }

    log_info("deleting some nodes");
    if (nodes_delete_some(&avl))
    {
        goto error;
    }

    log_info("finding non-existing");
    if (nodes_find_non_existing(&avl))
    {
        goto error;
    }

    log_info("insearching all nodes");
    if (nodes_insearch(&avl))
    {
        goto error;
    }

    log_info("finding nodes");
    if (nodes_find(&avl))
    {
        goto error;
    }

    log_info("finding non-existing");
    if (nodes_find_non_existing(&avl))
    {
        goto error;
    }

    exit_status = EXIT_SUCCESS;
error:
    avl_deinit(&avl);
    nodes_free();
    return exit_status;
}
