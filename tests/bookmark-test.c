/*
 * This file is part of mradio.
 *
 * Copyright (C) 2015-2017 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#define MRADIO_LOG_DOMAIN MRADIO_LOG_DOMAIN_TEST

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#include <gop.h>

#include <mradio/common.h>
#include <mradio/log.h>
#include <mradio/mstr.h>

#include <common/massert.h>

#include <daemon/bookmark.h>

#include "rng.h"

#define PROGRAM_NAME     "bookmark-test"
#define PROGRAM_NAME_LEN 13

#define MINIMAL 0

#if MINIMAL
# define CATS_MIN 1
# define CATS_MAX 1
# define NODES_MIN 8
# define NODES_MAX 8
# define KEY_MIN 4
# define KEY_MAX 4
#else /* !MINIMAL */
# define CATS_MIN 32
# define CATS_MAX 64
# define NODES_MIN 32768
# define NODES_MAX 65536
# define KEY_MIN 16
# define KEY_MAX 32
#endif /* !MINIMAL */

/* The likeliness, in percent, of a category being a child of another
 * category. */
#define CAT_CHILD_RATE 30

struct node_s {
    char * key;
    size_t keylen;
    struct node_s * left;
    struct node_s * right;
};
typedef struct node_s node_t;

/* Each category can have a number of children nodes (that can be both
 * categories and nodes). */
struct cat_s {
    node_t          node;
    struct cat_s *  parent;
    node_t *        nodes;
};
typedef struct cat_s cat_t;

struct stat_s {
    node_t node;
    cat_t * cat;
    char * value; /* mstr */
};
typedef struct stat_s stat_t;

static char allowed[] = {
    'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
    'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'x', 'y', 'z',
    'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
    'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'X', 'Y', 'Z',
    '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'
};

/* An array of categories. */
static cat_t *        cats           = NULL;
static unsigned int   cats_len       = 0;
static unsigned int   cats_size      = 0;

/* An array of stations. */
static stat_t *       stats          = NULL;
static unsigned int   stats_len      = 0;
static unsigned int   stats_size     = 0;

/* Root of the binary tree containing all nodes. */
static node_t *       nodes          = NULL;

/* The size of this array is the same as stats_size. */
static unsigned int * stat_queue     = NULL;
static unsigned int   stat_queue_len = 0;

/* This is saved between calls to stat_path so to save calls to realloc.
 */
static char *         path           = NULL;
static size_t         path_len       = 0;
static size_t         path_size      = 0;
static const cat_t ** cat_stack      = NULL;
static size_t         cat_stack_size = 0;

/* Buffer that is used in stat_init to hold the generated value string
 * before it is converted to an mstr. */
static char           tmp_value_buf[KEY_MAX];

static void NONNULL
strgen(char * const str, const size_t len)
{
    long int max = sizeof(allowed) - 1;
    for (size_t i = 0; i < len; ++i) {
        long int j = rng_max(max);
        str[i] = allowed[j];
    }
    return;
}

static void NONNULL
node_keygen(node_t * const node)
{
    strgen(node->key, node->keylen);
    return;
}

static int
node_init(node_t * const node)
{
    node->keylen = (size_t)rng_min_max(KEY_MIN, KEY_MAX);
    node->key = malloc(node->keylen + 1);
    if (node->key == NULL) {
        log_error_errno("could not allocate memory");
        return 1;
    }
    node->key[node->keylen] = '\0';
    node_keygen(node);

    node->left = NULL;
    node->right = NULL;

    return 0;
}

static void
node_deinit(node_t * const node)
{
    free(node->key);
    return;
}

/* Insert the new node NODE into the tree rooted at ROOTP.
 * Returns 0 on success or 1 if the key already exists. */
static int
node_insert(node_t ** rootp, node_t * const node)
{
    node_t * root;
    int cmp;

    while ((root = *rootp) != NULL) {
        cmp = strcmp(node->key, root->key);
        if (cmp < 0) {
            rootp = &root->left;
        } else if (cmp > 0) {
            rootp = &root->right;
        } else {
            return 1;
        }
    }
    *rootp = node;

    return 0;
}

static int NONNULL
cat_init(cat_t * const cat)
{
    if (node_init(&cat->node)) {
        return 1;
    }
    cat->parent = NULL;
    cat->nodes  = NULL;

    /* Find out where this category should be inserted. */
    node_t ** rootp = &nodes;
    if (cats_len > 0) {
        long int rate = rng_max(99);
        if (rate < CAT_CHILD_RATE) {
            cat->parent = cats + rng_max((long)cats_len - 1);
            rootp = &cat->parent->nodes;
        }
    }

    /* Try to insert the new category into the destination and generate
     * a new key if things fail. */
    while (node_insert(rootp, &cat->node)) {
        node_keygen(&cat->node);
    }

    return 0;
}

static void NONNULL
cat_deinit(cat_t * const cat)
{
    node_deinit(&cat->node);
    return;
}

static int
cats_alloc(void)
{
    cats_size = (unsigned int)rng_min_max(CATS_MIN, CATS_MAX);
    cats = malloc(cats_size * sizeof(cat_t));
    if (cats == NULL) {
        log_error_errno("could not allocate memory");
        return 1;
    }
    return 0;
}

static void
cats_free(void)
{
    while (cats_len > 0) {
        cats_len--;
        cat_deinit(cats + cats_len);
    }
    free(cats);
    cats = NULL;
    cats_size = 0;
    return;
}

static int
cats_prepare(void)
{
    if (cats_alloc()) {
        return 1;
    }

    for (cats_len = 0; cats_len < cats_size; ++cats_len) {
        if (cat_init(cats + cats_len)) {
            return 1;
        }
    }

    return 0;
}

static int NONNULL
stat_init(stat_t * const stat)
{
    unsigned  len;
    node_t ** rootp;
    unsigned  index;

    if (node_init(&stat->node)) {
        return 1;
    }

    len = (unsigned)rng_min_max(KEY_MIN, KEY_MAX);
    strgen(tmp_value_buf, len);
    stat->value = mstr_with_len(tmp_value_buf, len);
    if (!stat->value)
    {
        return 1;
    }

    stat->cat = NULL;

    /* Find out where this station should be inserted. */
    rootp = &nodes;
    index = (unsigned int)rng_max((long int)cats_len);
    if (index < cats_len) {
        stat->cat = cats + index;
        rootp = &stat->cat->nodes;
    }

    /* Try to insert the new category into the destination and generate
     * a new key if the key already exist. */
    while (node_insert(rootp, &stat->node)) {
        node_keygen(&stat->node);
    }

    return 0;
}

static void
stat_deinit(stat_t * const stat)
{
    node_deinit(&stat->node);
    mstr_unref(stat->value);
    return;
}

static const char * NONNULL
stat_path(const stat_t * const stat)
{
    if (stat->cat == NULL) {
        return stat->node.key;
    } else {
        size_t        size          = stat->node.keylen + 1;
        size_t        cat_stack_len = 0;
        size_t        i;

        for (const cat_t * cat = stat->cat;
             cat != NULL;
             cat = cat->parent)
        {
            cat_stack_len++;
            size += cat->node.keylen + 1;
        }

        if (size > path_size) {
            char * const new = realloc(path, size);
            if (new == NULL) {
                log_error_errno("could not allocate memory");
                return NULL;
            }
            path = new;
            path_size = size;
        }

        if (cat_stack_len > cat_stack_size) {
            const cat_t ** new;
            new = realloc(cat_stack, cat_stack_len * sizeof(cat_t *));
            if (new == NULL) {
                log_error_errno("could not allocate memory");
                return NULL;
            }
            cat_stack = new;
            cat_stack_size = cat_stack_len;
        }

        i = 0;
        for (const cat_t * cat = stat->cat;
             cat != NULL;
             cat = cat->parent)
        {
            massert(i < cat_stack_size);
            cat_stack[i++] = cat;
        }

        path_len = 0;
        while (cat_stack_len--) {
            const cat_t * const cat = cat_stack[cat_stack_len];
            memcpy(path + path_len, cat->node.key, cat->node.keylen);
            path_len += cat->node.keylen;
            path[path_len++] = '/';
        }

        memcpy(path + path_len, stat->node.key, stat->node.keylen);
        path_len += stat->node.keylen;
        path[path_len] = '\0';

        return path;
    }
}

static int
stats_alloc(void)
{
    stats_size = (unsigned int)rng_min_max(NODES_MIN, NODES_MAX);
    stats = malloc(stats_size * sizeof(stat_t));
    if (stats == NULL) {
        log_error_errno("could not allocate memory");
        return 1;
    }
    stat_queue = malloc(stats_size * sizeof(int));
    if (stat_queue == NULL) {
        log_error_errno("could not allocate memory");
        return 1;
    }
    return 0;
}

static void
stats_free(void)
{
    while (stats_len > 0) {
        stats_len--;
        stat_deinit(stats + stats_len);
    }
    free(stats);
    free(stat_queue);
    stats = NULL;
    stat_queue = NULL;
    stats_size = 0;
    return;
}

static int
stats_prepare(void)
{
    if (stats_alloc()) {
        return 1;
    }

    for (stats_len = 0; stats_len < stats_size; ++stats_len) {
        if (stat_init(stats + stats_len)) {
            return 1;
        }
    }

    return 0;
}

static void
stat_queue_fill(void)
{
    stat_queue_len = stats_len;
    for (unsigned int i = 0; i < stat_queue_len; ++i) {
        stat_queue[i] = i;
    }
    return;
}

static const stat_t *
stat_queue_pull(void)
{
    if (stat_queue_len > 0) {
        unsigned int   r;
        unsigned int   index;

        r = (unsigned int)rng_max((long int)stat_queue_len - 1);
        index = stat_queue[r];
        stat_queue[r] = stat_queue[--stat_queue_len];

        return stats + index;
    } else {
        return NULL;
    }
}

static int
stats_insert(void)
{
    const stat_t * stat;
    const char *   p;

    stat_queue_fill();
    while ((stat = stat_queue_pull()) != NULL) {
        p = stat_path(stat);
        if (p == NULL) {
            return 1;
        }
        if (bookmark_add(p, stat->value)) {
            return 1;
        }
    }

    return 0;
}

static int
stats_find(void)
{
    const stat_t * stat;
    const char *   p;
    char *         url;
    int            cmp;

    stat_queue_fill();
    while ((stat = stat_queue_pull()) != NULL) {
        p = stat_path(stat);
        if (p == NULL) {
            return 1;
        }
        url = bookmark_url(p);
        if (url == NULL) {
            return 1;
        }
        cmp = strcmp(url, stat->value);
        mstr_unref(url);
        if (cmp) {
            log_error("expected url and actual url differed");
            log_error("got %s, but expected %s", url, stat->value);
            return 1;
        }
    }

    return 0;
}

static int
stats_insert_again(void)
{
    const stat_t * stat;
    const char *   p;
    int            ret;

    stat_queue_fill();
    while ((stat = stat_queue_pull()) != NULL) {
        p = stat_path(stat);
        if (p == NULL) {
            return 1;
        }
        log_set_quiet(1);
        ret = bookmark_add(p, stat->value);
        log_set_quiet(0);
        if (!ret) {
            log_error("inserting duplicate %s succeeded", p);
            return 1;
        }
    }

    return 0;
}

#define FILENAME_LEN 23 /* "bookmark-test." + "XXXXXXXX" + '\0' */
int
main(int argc, char ** const argv)
{
    int    exit_status = EXIT_FAILURE;
    char   buf[FILENAME_LEN];
    char * filename = NULL;

    log_set_name(PROGRAM_NAME);
    log_set_level(LOG_DOMAIN_ALL, LOG_LEVEL_WARN);
    log_set_level(LOG_DOMAIN_RNG,  LOG_LEVEL_INFO);

    bookmark_init();

    gop_t * const gop = gop_new();
    if (gop == NULL) {
        goto error;
    }

    gop_set_program_name(gop, PROGRAM_NAME);
    rng_add_gop_table(gop);
    gop_autohelp(gop);
    gop_parse(gop, &argc, argv);
    gop_destroy(gop);

    if (log_getenv())
    {
        goto error;
    }

    rng_seed();

    if (cats_prepare()) {
        goto error;
    }

    if (stats_prepare()) {
        goto error;
    }

    /* Find a filename that does not already exist. Cross your fingers
     * that it will not be read or modified by another process since
     * this test is all but atomic. */
    memset(buf, 0, FILENAME_LEN);
    filename = mstr_with_len(buf, FILENAME_LEN);
    if (!filename)
    {
        goto error;
    }
    memcpy(filename, PROGRAM_NAME, PROGRAM_NAME_LEN);
    filename[PROGRAM_NAME_LEN] = '.';
    filename[PROGRAM_NAME_LEN + 9] = '\0';
    do {
        strgen(filename + PROGRAM_NAME_LEN + 1, 8);
    } while (!access(filename, F_OK));
    bookmark_file(filename);

    log_info("inserting stations");
    if (stats_insert()) {
        goto error;
    }

    log_info("finding stations");
    if (stats_find()) {
        goto error;
    }

    log_info("inserting duplicates");
    if (stats_insert_again()) {
        goto error;
    }

    log_info("saving bookmarks");
    if (bookmark_save()) {
        goto error;
    }

    bookmark_deinit();
    bookmark_init();

    /* This must be reset. */
    bookmark_file(filename);

    log_info("loading bookmarks");
    if (bookmark_load()) {
        goto error;
    }

    log_info("finding stations");
    if (stats_find()) {
        goto error;
    }

    log_info("inserting duplicates");
    if (stats_insert_again()) {
        goto error;
    }

    if (unlink(filename)) {
        log_error_errno("could not unlink %s", filename);
        goto error;
    }

    exit_status = EXIT_SUCCESS;
error:
    mstr_unref(filename);
    free(path);
    free(cat_stack);
    path           = NULL;
    path_len       = 0;
    path_size      = 0;
    cat_stack      = NULL;
    cat_stack_size = 0;

    bookmark_deinit();
    stats_free();
    cats_free();
    return exit_status;
}
