/*
 * This file is part of mradio.
 *
 * Copyright (C) 2014-2016 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#ifndef PROTO_H
# define PROTO_H

# include <stdint.h>

/* Version of the current protocol.
 * HOWTO
 * -----
 * 1. If the protocol is changed during developement increment the last
 *    16 bits.
 * 2. When a release is made and there were protocol changes increment
 *    the first 16 bits and reset the last 16 bits. */
# define PROTO_VERSION       0x00000001

/* This is the least supported version which the server or client is
 * willing to adapt to. All older versions are deprecated. */
# define PROTO_LEAST_VERSION 0x00000001

/* Strings are sent by first sending their length (uint16_t) and then
 * sending the actual string, excluding nul character. */

/* CLIENT -> SERVER */

/* Marks beginning and end of communication. */
# define PROTO_BEGIN        1
# define PROTO_END          2

/* The next part of the message is a command, i.e. the next is an
 * uint8_t giving argc followed by ARGC string(s) (constituting argv).
 * The first argument must be the command name. */
# define PROTO_CMD          3

/* SERVER -> CLIENT */

/* This is the response of the server if the client is too old. */
# define PROTO_DEPRECATED    0

/* The server wants the client to authenticate using a password. */
# define PROTO_PASSWORD      1

/* The authentication succeeded and regular communication may begin. */
# define PROTO_AUTH          2

/* The return at the next index is empty, so dismiss it.
 * This indicates a programming error on the server side. */
# define PROTO_RETURN_NONE   3

/* The next part is a return array.
 * This token is followed by the length of the array (uint32_t) and then
 * each return. */
# define PROTO_RETURN_ARRAY  4

/* The next is a returned string.
 * After this token the length of the string is sent followed by the
 * string excluding the nul character. */
# define PROTO_RETURN_STRING 5

/* The next part is an int32_t giving the exit status of the command.
 * This is the last part that is sent as part of each command. */
# define PROTO_STATUS        6

/* The following is a log message from the server. */
# define PROTO_LOG           7


#endif /* !PROTO_H */
