/*
 * This file is part of mradio.
 *
 * Copyright (C) 2014-2016 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#ifndef RUN_SHELL_H
# define RUN_SHELL_H 1

# if HAVE_CONFIG_H
#  include <config.h>
# endif /* HAVE_CONFIG_H */

# include <mradio/common.h>

/* May not be called from a multithreaded process.
 * Returns a negative value if the shell could not be run, die was
 * called or if the shell was terminated by a signal. Otherwise the
 * exit status of the shell is returned. */
int run_shell(char ** argv) MRADIO_NONNULL;

#endif /* !RUN_SHELL_H */
