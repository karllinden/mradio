/*
 * This file is part of mradio.
 *
 * Copyright (C) 2014-2016 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#define MRADIO_LOG_DOMAIN MRADIO_LOG_DOMAIN_RUN

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <signal.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include <mradio/common.h>
#include <mradio/common-internal.h>
#include <mradio/info.h>
#include <mradio/log.h>
#include <mradio/mradio.h>

#include "asprintf.h"
#include "call.h"
#include "log-debug.h"
#include "massert.h"
#include "prepend-envvar.h"
#include "run-shell.h"

static int prepared = 0;

int NONNULL
run_shell(char ** argv)
{
    int      exit_status = -1;
    int      ret;
    int      sig;
    pid_t    ppid;
    char *   str;
    sigset_t old;
    sigset_t set;
    pid_t    pid;
    int      wstatus;
    pid_t    w;

    /* If the environment is not prepared, prepare it. */
    if (!prepared)
    {
        /* Make sure the shell uses the commands. */
        if (prepend_envvar("PATH", MRADIO_LIBEXEC ":"))
        {
            return -1;
        }

        /* Set MRADIO_PID to the PID of this process (the parent) so
         * that children can the USR1 signal to die. */
        ppid = getpid();
        ret = asprintf(&str, "%ld", (long)ppid);
        if (ret < 0)
        {
            log_error("asprintf failed");
            return -1;
        }
        ret = setenv("MRADIO_PID", str, 1);
        if (ret)
        {
            log_error_errno("could not set MRADIO_PID");
        }
        free(str);
        if (ret)
        {
            return -1;
        }

        /* Shell is now prepared. */
        prepared = 1;
    }

    CALL(sigemptyset, &set);
    CALL(sigaddset, &set, SIGUSR1);
    CALL(sigaddset, &set, SIGCHLD);
    CALL(sigprocmask, SIG_BLOCK, &set, &old);

    /* Spawn the shell. */
    pid = fork();
    if (pid < 0)
    {
        log_error_errno("could not fork");
        goto cleanup;
    }
    else if (pid == 0)
    {
        /* Child process. */
        execv(argv[0], argv);
        log_error_errno("could not execute %s", argv[0]);
        exit(1);
    }

    while (1)
    {
        CALL(sigwait, &set, &sig);
        if (sig == SIGUSR1)
        {
            /* If die was called send SIGTERM to the shell. This has the
             * desired effect both if the shell is interactive and when
             * it is not. If the shell is interactive this signal is
             * ignored and the user only sees an error message. If the
             * shell is not interactive then it is terminated and
             * control is returned. */
            CALL(kill, pid, SIGTERM);
        }
        else
        {
            /* SIGCHLD - something happened to the child. */
            massert(sig == SIGCHLD);

            /* WNOHANG to avoid locking up this process in case of a
             * spurious (or malicius) SIGCHLD. */
            w = waitpid(pid, &wstatus, WNOHANG);
            if (w < 0)
            {
                log_error_errno("could not wait");
                goto cleanup;
            }
            massert(w == pid);

            if (WIFEXITED(wstatus))
            {
                exit_status = WEXITSTATUS(wstatus);
                goto cleanup;
            }
            else if (WIFSIGNALED(wstatus))
            {
#if DEBUG
                sig = WTERMSIG(wstatus);
                log_debug("shell terminated by signal %d", sig);
#endif /* DEBUG */
                goto cleanup;
            }
        }
    }

cleanup:
    CALL(sigprocmask, SIG_SETMASK, &old, NULL);
    return exit_status;
}
