/*
 * This file is part of mradio.
 *
 * Copyright (C) 2016 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#define MRADIO_LOG_DOMAIN MRADIO_LOG_DOMAIN_COMMON

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <stdlib.h>
#include <string.h>

#include <mradio/common.h>
#include <mradio/log.h>

#include "prepend-envvar.h"

int NONNULL
prepend_envvar(const char * name, const char * prepend)
{
    const char * cur;
    size_t       curlen;
    size_t       prelen; /* len of libexec path to prepend */
    char *       new; /* new path */
    size_t       newlen;
    int          ret;

    cur = getenv(name);
    if (cur != NULL)
    {
        curlen = strlen(cur);
    }
    else
    {
        curlen = 0;
    }
    prelen = strlen(prepend);
    newlen = prelen + curlen;

    new = malloc(newlen + 1);
    if (new == NULL)
    {
        log_error_errno("could not allocate new %s variable", name);
        return 1;
    }

    memcpy(new, prepend, prelen);
    memcpy(new + prelen, cur, curlen);
    new[newlen] = '\0';

    ret = setenv(name, new, 1);
    free(new);
    if (ret)
    {
        log_error_errno("could not set %s variable", name);
        return 1;
    }

    return 0;
}
