/*
 * This file is part of mradio.
 *
 * Copyright (C) 2016 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

/* Macros that can be used to call functions without error checking in
 * distribution builds, but check errors in debug builds. */

#ifndef CALL_H
# define CALL_H

# if HAVE_CONFIG_H
#  include <config.h>
# endif /* HAVE_CONFIG_H */

# if NDEBUG
#  define CALL(function, ...) function(__VA_ARGS__)
#  define PCALL(function, ...) function(__VA_ARGS__)
# else /* !NDEBUG */
#  include <errno.h>
#  include <stdlib.h>
#  include <mradio/log.h>
#  define CALL(function, ...) \
    if (function(__VA_ARGS__)) \
    { \
        mradio_log_error_errno(#function); \
        abort(); \
    }
#  define PCALL(function, ...) \
    { \
        int r = function(__VA_ARGS__); \
        if (r) \
        { \
            errno = r; \
            mradio_log_error_errno(#function); \
            abort(); \
        } \
    }
# endif /* !NDEBUG */

#endif /* CALL_H */
