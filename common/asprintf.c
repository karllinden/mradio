/*
 * This file is part of mradio.
 *
 * Copyright (C) 2016 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <mradio/common.h>

#include "asprintf.h"

#if !HAVE_ASPRINTF
int NONNULL PRINTF(2,3)
asprintf(char ** strp, const char * fmt, ...)
{
    va_list ap;
    int     ret;
    va_start(ap, fmt);
    ret = vasprintf(strp, fmt, ap);
    va_end(ap);
    return ret;
}
#endif /* !HAVE_ASPRINTF */

#if !HAVE_VASPRINTF
int NONNULL
vasprintf(char ** strp, const char * fmt, va_list ap)
{
    va_list apc;
    int     len;
    size_t  siz;

    va_copy(apc, ap);
    len = vsnprintf(NULL, 0, fmt, apc);
    va_end(apc);
    if (len < 0)
    {
        return -1;
    }

    siz = (size_t)len;
    *strp = malloc(siz + 1);
    if (*strp == NULL)
    {
        return -1;
    }
    memset(*strp, 0, siz + 1);
    len = vsnprintf(*strp, siz + 1, fmt, ap);
    if (len < 0)
    {
        free(*strp);
        return -1;
    }

    return len;
}
#endif /* !HAVE_VASPRINTF */
