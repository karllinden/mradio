/*
 * This file is part of mradio.
 *
 * Copyright (C) 2017 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#define MRADIO_LOG_DOMAIN MRADIO_LOG_DOMAIN_CLIENT
#undef MRADIO_INTERNAL

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <errno.h>
#include <signal.h>
#include <stdlib.h>
#include <sys/types.h>

#include <mradio/log.h>

#include "die.h"

void
die(void)
{
    char * strpid;
    char * end;
    long   longpid;
    pid_t  pid;
    strpid = getenv("MRADIO_PID");
    if (strpid)
    {
        errno = 0;
        longpid = strtol(strpid, &end, 10);
        if (errno)
        {
            mradio_log_error_errno("could not convert %s to long",
                                   strpid);
        }
        else if (*end != '\0')
        {
            mradio_log_error("could not convert %s to long: string not "
                             "fully read", strpid);
        }
        else if (longpid < 0)
        {
            mradio_log_error("invalid pid %ld", longpid);
        }
        else
        {
            pid = (pid_t)longpid;
            if (kill(pid, SIGUSR1))
            {
                mradio_log_error_errno("could not send USR1 to PID %ld",
                                longpid);
            }
        }
    }
    return;
}
