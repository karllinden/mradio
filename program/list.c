/*
 * This file is part of mradio.
 *
 * Copyright (C) 2017 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#define MRADIO_LOG_DOMAIN MRADIO_LOG_DOMAIN_CLIENT
#undef MRADIO_INTERNAL

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#define DEFAULT_LOG_LEVEL MRADIO_LOG_LEVEL_INFO

#include <stdlib.h>

#include <gop.h>

#include <mradio/common.h>
#include <mradio/common-internal.h>
#include <mradio/info.h>
#include <mradio/log.h>
#include <mradio/mradio.h>
#include <mradio/mstr.h>
#include <mradio/return.h>

#include <common/die.h>
#include <common/print-version.h>

static void
print_indent(unsigned indent)
{
    while (indent--)
    {
        putchar(' ');
    }
    return;
}

static int NONNULL
print_category(mradio_return_t * ret, int url, unsigned indent)
{
    unsigned             len;
    mradio_return_type_t type;
    char *               name;
    void *               ptr;
    char *               urlstr;
    mradio_return_t *    arr;
    int                  err = 0;

    if (url)
    {
        len = mradio_return_len(ret) / 2;
        for (unsigned i = 0; i < len; ++i)
        {
            name = mradio_return_get(ret, 2 * i, &type);
            if (!name)
            {
                return 1;
            }
            if (type != MRADIO_RETURN_TYPE_STRING)
            {
                mradio_log_error("invalid return type");
                return 1;
            }
            ptr = mradio_return_get(ret, 2 * i + 1, &type);
            if (!ptr)
            {
                return 1;
            }
            print_indent(indent);
            fputs(name, stdout);
            switch (type)
            {
                case MRADIO_RETURN_TYPE_STRING:
                    urlstr = ptr;
                    putchar(' ');
                    puts(urlstr);
                    mradio_mstr_unref(urlstr);
                    break;
                case MRADIO_RETURN_TYPE_ARRAY:
                    arr = ptr;
                    putchar('\n');
                    err = print_category(arr, url, indent + 1);
                    mradio_return_unref(arr);
                    break;
                default:
                    putchar('\n');
                    mradio_log_error("invalid return type");
                    err = 1;
                    break;
            }
            mradio_mstr_unref(name);
            if (err)
            {
                return err;
            }
        }
    }
    else
    {
        len = mradio_return_len(ret);
        for (unsigned i = 1; i < len; ++i)
        {
            print_indent(indent);
            ptr = mradio_return_get(ret, i, &type);
            switch (type)
            {
                case MRADIO_RETURN_TYPE_STRING:
                    name = ptr;
                    puts(name);
                    mradio_mstr_unref(name);
                    break;
                case MRADIO_RETURN_TYPE_ARRAY:
                    arr = ptr;
                    name = mradio_return_get(arr, 0, &type);
                    if (type != MRADIO_RETURN_TYPE_STRING)
                    {
                        mradio_log_error("invalid return type");
                        mradio_return_unref(arr);
                        return 1;
                    }
                    puts(name);
                    mradio_mstr_unref(name);
                    err = print_category(arr, url, indent + 1);
                    mradio_return_unref(arr);
                    if (err)
                    {
                        return err;
                    }
                    break;
                default:
                    mradio_log_error("invalid return type");
                    return 1;
            }
        }
    }
    return 0;
}

static int NONNULL
print_list(mradio_t * mradio, char * path, int url)
{
    int                  retval;
    mradio_return_t *    ret = NULL;
    const char *         argv[] =
    {
        "bookmark",
        "list",
        NULL,
        NULL,
        NULL,
    };
    uint8_t              argc = 2;

    if (path)
    {
        argv[argc] = path;
        argc++;
    }
    if (url)
    {
        argv[argc] = "--url";
        argc++;
    }

    retval = mradio_command(mradio, argc, argv, &ret);
    if (retval)
    {
        return retval;
    }
    if (!ret)
    {
        mradio_log_error("return array is empty");
        return 1;
    }

    retval = print_category(ret, url, 0);

    mradio_return_unref(ret);
    return retval;
}

int NONNULL
main(int argc, char ** argv)
{
    int               exit_status = EXIT_FAILURE;
    char *            path = NULL;
    int               url = 0;
    mradio_t *        mradio = NULL;
    mradio_info_t     info;

    mradio_info_init(&info);

    const gop_option_t options[] = {
        {"url", 'u', GOP_NONE, &url, NULL, "print URLs", NULL},
        {"version", 'v', GOP_NONE, NULL, &print_version,
            "print version and exit", NULL},
        GOP_TABLEEND
    };

    gop_t * gop = gop_new();
    if (gop == NULL) {
        goto error;
    }

    gop_add_table(gop, "Program options:", options);
    gop_add_usage(gop, "[OPTIONS...] [PATH]");
    gop_description(gop,
                    "Lists all bookmarks that are rooted at PATH. "
                    "If PATH is not given all bookmarks are listed.");
    gop_autohelp(gop);
    gop_parse(gop, &argc, argv);
    gop_destroy(gop);

    if (argc <= 2)
    {
        path = argv[1];
    }
    else
    {
        mradio_log_error("too many arguments");
        goto error;
    }

    mradio_log_set_level(MRADIO_LOG_DOMAIN_ALL, DEFAULT_LOG_LEVEL);
    if (mradio_log_getenv())
    {
        goto error;
    }

    if (getenv("MRADIO_STDIO"))
    {
        /* The server is hooked up to standard I/O, but this program
         * prints to standard out. */
        mradio_log_fatal("cannot print list when server is on stdio");
        goto error;
    }

    if (mradio_info_getenv(&info))
    {
        goto error;
    }
    mradio = mradio_connect(&info, 0);
    if (mradio == NULL)
    {
        goto error;
    }

    exit_status = print_list(mradio, path, url);

error:
    mradio_disconnect(mradio);
    mradio_info_deinit(&info);
    if (exit_status)
    {
        die();
    }
    return exit_status;
}
