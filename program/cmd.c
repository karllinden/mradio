/*
 * This file is part of mradio.
 *
 * Copyright (C) 2016-2017 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#define MRADIO_LOG_DOMAIN MRADIO_LOG_DOMAIN_CLIENT
#undef MRADIO_INTERNAL

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#define DEFAULT_LOG_LEVEL MRADIO_LOG_LEVEL_INFO

#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <gop.h>

#include <mradio/common.h>
#include <mradio/common-internal.h>
#include <mradio/info.h>
#include <mradio/log.h>
#include <mradio/mradio.h>
#include <mradio/return.h>

#include <common/die.h>
#include <common/print-version.h>

/* Some commands are useless to run with this command runner, since the
 * returned data is important and needs special care.
 * For these use cases specially written programs are used, but to make
 * them easily invokable, redirections to those can be made. */
struct redirect_s
{
    const char *              name;
    char *                    cmd;
    const struct redirect_s * subcmds;
    size_t                    subcmds_len;
};
typedef struct redirect_s redirect_t;

static const redirect_t bookmark_redirections[] = {
    {"list", MRADIO_LIBEXEC "/bookmark-list", NULL, 0},
    {"url", MRADIO_LIBEXEC "/bookmark-url", NULL, 0},
};

static const redirect_t redirections[] = {
    {
        "bookmark",
        NULL,
        bookmark_redirections,
        ARRAY_SIZE(bookmark_redirections)
    },
};

static gop_return_t
at_gop_error(gop_t * gop)
{
    switch (gop_get_error(gop))
    {
        case GOP_ERROR_UNKLOPT:
        case GOP_ERROR_UNKSOPT:
            return GOP_DO_CONTINUE_WO_HANDLER;
        default:
            return GOP_DO_CONTINUE;
    }
}

static int
redirect_cmp(const void * p1, const void * p2)
{
    const redirect_t * r1 = p1;
    const redirect_t * r2 = p2;
    return strcmp(r1->name, r2->name);
}

static void NONNULL
exec_redirect(char ** argv, const redirect_t * array, size_t len)
{
    redirect_t   key;
    redirect_t * redirect;

    while (argv[0])
    {
        key.name = argv[0];
        redirect = bsearch(&key,
                           array,
                           len,
                           sizeof(redirect_t),
                           &redirect_cmp);
        if (!redirect)
        {
            return;
        }
        else
        {
            if (redirect->cmd)
            {
                /* Execute the redirection. */
                argv[0] = redirect->cmd;
                execv(redirect->cmd, argv);
                mradio_log_error_errno("could not execute %s",
                                       redirect->cmd);
                exit(1);
            }
            else
            {
                argv++;
                assert(redir->subcmds);
                array = redirect->subcmds;
                len   = redirect->subcmds_len;
            }
        }
    }

    return;
}

int NONNULL
main(int argc, char ** argv)
{
    int exit_status = EXIT_FAILURE;
    mradio_t * mradio = NULL;
    mradio_info_t info;
    mradio_return_t * ret = NULL;

    mradio_info_init(&info);

    const gop_option_t options[] = {
        {"version", 'v', GOP_NONE, NULL, &print_version,
            "print version and exit", NULL},
        GOP_TABLEEND
    };

    gop_t * gop = gop_new();
    if (gop == NULL) {
        goto error;
    }

    gop_add_table(gop, "Program options:", options);
    gop_add_usage(gop, "<command> [arguments]");
    gop_description(gop,
                    "Sends a command to the running mradio daemon.");
    gop_autohelp(gop);
    gop_aterror(gop, &at_gop_error);
    gop_parse(gop, &argc, argv);
    if (argc <= 1)
    {
        mradio_log_error("too few arguments");
        gop_print_help(gop);
    }
    gop_destroy(gop);
    if (argc <= 1)
    {
        goto error;
    }

    /* Execute a redirection if necessary. */
    exec_redirect(argv + 1, redirections, ARRAY_SIZE(redirections));

    mradio_log_set_level(MRADIO_LOG_DOMAIN_ALL, DEFAULT_LOG_LEVEL);
    if (mradio_log_getenv())
    {
        goto error;
    }

    if (getenv("MRADIO_STDIO"))
    {
        /* The server is hooked up to standard I/O. */
        mradio = mradio_stdio();
    }
    else
    {
        if (mradio_info_getenv(&info))
        {
            goto error;
        }
        mradio = mradio_connect(&info, 0);
    }
    if (mradio == NULL)
    {
        goto error;
    }

    exit_status = mradio_command(mradio,
                                 (uint8_t)(argc - 1),
                                 (const char **)(argv + 1),
                                 &ret);
    mradio_return_unref(ret);
error:
    mradio_disconnect(mradio);
    mradio_info_deinit(&info);
    if (exit_status)
    {
        die();
    }
    return exit_status;
}
