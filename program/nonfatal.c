/*
 * This file is part of mradio.
 *
 * Copyright (C) 2016 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

/* Run a command and make exceptional conditions (die, non-zero exit
 * status) non-fatal. */

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/wait.h>
#include <unistd.h>

int
main(int argc, char ** argv)
{
    pid_t pid;
    pid_t ret;
    int   wstatus;

    if (argc <= 1)
    {
        fprintf(stderr, "nonfatal: too few arguments");
        return EXIT_FAILURE;
    }

    unsetenv("MRADIO_PID");

    /* Perform a fork in order to be able to always exit with a success
     * status. */
    pid = fork();
    if (pid < 0)
    {
        perror("fork");
        return EXIT_FAILURE;
    }
    else if (pid == 0)
    {
        /* Child. */
        execvp(argv[1], argv + 1);
        perror("execv");
        exit(EXIT_FAILURE);
    }

    /* Parent. */
    do {
        ret = waitpid(pid, &wstatus, 0);
        if (ret < 0)
        {
            perror("waitpid");
            return EXIT_FAILURE;
        }
        assert(ret == pid);
    } while (!WIFEXITED(wstatus) && !WIFSIGNALED(wstatus));

    return EXIT_SUCCESS;
}
