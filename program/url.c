/*
 * This file is part of mradio.
 *
 * Copyright (C) 2017 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#define MRADIO_LOG_DOMAIN MRADIO_LOG_DOMAIN_CLIENT
#undef MRADIO_INTERNAL

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#define DEFAULT_LOG_LEVEL MRADIO_LOG_LEVEL_INFO

#include <stdlib.h>

#include <gop.h>

#include <mradio/common.h>
#include <mradio/common-internal.h>
#include <mradio/info.h>
#include <mradio/log.h>
#include <mradio/mradio.h>
#include <mradio/mstr.h>
#include <mradio/return.h>

#include <common/die.h>
#include <common/print-version.h>

static int NONNULL
print_url(mradio_t * mradio, char * path)
{
    int                  retval;
    mradio_return_t *    ret = NULL;
    mradio_return_type_t type = MRADIO_RETURN_TYPE_NONE;
    char *               url;
    const char *         argv[] =
    {
        "bookmark",
        "url",
        path,
        NULL
    };

    retval = mradio_command(mradio, 3, argv, &ret);
    if (retval)
    {
        return retval;
    }
    if (!ret)
    {
        mradio_log_error("return array is empty");
        return 1;
    }

    url = mradio_return_get(ret, 0, &type);
    if (type != MRADIO_RETURN_TYPE_STRING)
    {
        mradio_log_error("invalid return type");
        retval = 1;
    }
    else
    {
        puts(url);
        mradio_mstr_unref(url);
    }
    mradio_return_unref(ret);
    return retval;
}

int NONNULL
main(int argc, char ** argv)
{
    int               exit_status = EXIT_FAILURE;
    mradio_t *        mradio = NULL;
    mradio_info_t     info;

    mradio_info_init(&info);

    const gop_option_t options[] = {
        {"version", 'v', GOP_NONE, NULL, &print_version,
            "print version and exit", NULL},
        GOP_TABLEEND
    };

    gop_t * gop = gop_new();
    if (gop == NULL) {
        goto error;
    }

    gop_add_table(gop, "Program options:", options);
    gop_add_usage(gop, "[PATH]...");
    gop_description(gop, "Prints the url of the station at each PATH.");
    gop_autohelp(gop);
    gop_parse(gop, &argc, argv);
    gop_destroy(gop);

    mradio_log_set_level(MRADIO_LOG_DOMAIN_ALL, DEFAULT_LOG_LEVEL);
    if (mradio_log_getenv())
    {
        goto error;
    }

    if (getenv("MRADIO_STDIO"))
    {
        /* The server is hooked up to standard I/O, but this program
         * prints to standard out. */
        mradio_log_fatal("cannot print url when server is on stdio");
        goto error;
    }

    if (mradio_info_getenv(&info))
    {
        goto error;
    }
    mradio = mradio_connect(&info, 0);
    if (mradio == NULL)
    {
        goto error;
    }

    exit_status = 0;
    for (int i = 1; i < argc; ++i)
    {
        exit_status += print_url(mradio, argv[i]);
    }

error:
    mradio_disconnect(mradio);
    mradio_info_deinit(&info);
    if (exit_status)
    {
        die();
    }
    return exit_status;
}
