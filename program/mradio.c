/*
 * This file is part of mradio.
 *
 * Copyright (C) 2014-2017 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#define MRADIO_LOG_DOMAIN MRADIO_LOG_DOMAIN_CLIENT
#undef MRADIO_INTERNAL

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#define DEFAULT_LOG_LEVEL MRADIO_LOG_LEVEL_INFO

#include <stdlib.h>

#include <gop.h>

#include <mradio/common.h>
#include <mradio/common-internal.h>
#include <mradio/info.h>
#include <mradio/log.h>
#include <mradio/mradio.h>


#include <common/prepend-envvar.h>
#include <common/print-version.h>
#include <common/run-shell.h>

/* Ignore unknown options. */
static gop_return_t
aterror_gop(gop_t * gop)
{
    gop_error_t error = gop_get_error(gop);
    if (error == GOP_ERROR_UNKLOPT || error == GOP_ERROR_UNKSOPT)
    {
        return GOP_DO_CONTINUE_WO_HANDLER;
    }
    else
    {
        return GOP_DO_CONTINUE;
    }
}

int NONNULL
main(int argc, char ** argv)
{
    int           exit_status  = EXIT_FAILURE;
    mradio_t *    mradio       = NULL;
    mradio_info_t info;

    int           no_autospawn = 0;
    char *        shell        = "/bin/sh";

    mradio_info_init(&info);

    const gop_option_t options[] = {
        {"no-autospawn", 'n', GOP_NONE, &no_autospawn, NULL,
            "do not spawn a server if connection fails", NULL},
        {"shell", 's', GOP_STRING, &shell, NULL,
            "shell to spawn [/bin/sh]", "SHELL"},
        {"version", 'v', GOP_NONE, NULL, &print_version,
            "print version and exit", NULL},
        GOP_TABLEEND
    };

    gop_t * const gop = gop_new();
    if (gop == NULL) {
        goto error;
    }

    gop_set_program_name(gop, PROGRAM_NAME);
    gop_add_table(gop, "Program options:", options);
    gop_autohelp(gop);
    gop_extra_help(gop,
                   "Options not understood by this program will be "
                    "passed to the shell.");
    gop_aterror(gop, &aterror_gop);
    gop_parse(gop, &argc, argv);
    gop_destroy(gop);

    mradio_log_set_level(MRADIO_LOG_DOMAIN_ALL, DEFAULT_LOG_LEVEL);
    if (mradio_log_getenv())
    {
        goto error;
    }

    if (mradio_info_getenv(&info))
    {
        goto error;
    }

    /* The ordinary autospawning implemented in libmradio cannot be
     * used since it spawns a private server, which will not be
     * accessible to child processes. */
    mradio = mradio_connect(&info, 0);
    if (mradio == NULL)
    {
        if (no_autospawn)
        {
            goto error;
        }
        else
        {
            mradio = mradio_spawn();
            if (mradio == NULL)
            {
                goto error;
            }
        }
    }

    /* Let future children know about the server. */
    if (mradio_info_setenv(&info))
    {
        goto error;
    }

    if (prepend_envvar("PS1", "(mradio) "))
    {
        goto error;
    }

    /* In case the shell respects HISTFILE, make sure HISTFILE is not
     * defined in order to avoid silly commands in the usual shell
     * history. */
    unsetenv("HISTFILE");

    argv[0] = shell;
    exit_status = run_shell(argv);
    if (exit_status < 0)
    {
        exit_status = EXIT_FAILURE;
    }

error:
    mradio_disconnect(mradio);
    mradio_info_deinit(&info);
    return exit_status;
}
