/*
 * This file is part of mradio.
 *
 * Copyright (C) 2015-2017 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#ifndef MODULE_H
# define MODULE_H 1

# if HAVE_CONFIG_H
#  include <config.h>
# endif /* HAVE_CONFIG_H */

# if HAVE_LTDL
#  include <ltdl.h>
# endif /* HAVE_LTDL */

# include <mradio/common.h>
# include <mradio/module.h>

typedef struct module_s module_t;
typedef struct module_handle_s module_handle_t;

/* A module handle keeps track of where a module is being used.
 * This is part of the safe way of loading modules, since the module
 * will be loaded as long as there are handles in the handles list of
 * module_t.
 * Before loading a handle it must have been initialized with
 * MODULE_HANDLE_INITIALIZER.
 * A handle may only be used for one module at a time - calling
 * module_handle_load on a handle which is in use will result in heavy
 * memory corruption. */
struct module_handle_s
{
    module_t *        module;
    module_handle_t * prev;
    module_handle_t * next;
};
# define MODULE_HANDLE_INITIALIZER {NULL, NULL, NULL}
# define module_handle_loaded(handle) ((handle)->module != NULL)

struct module_s
{
    char *                name; /* mstr, never NULL */
    unsigned              count;
    module_handle_t *     handles;
    const module_info_t * info;
    module_deinit_fn_t *  deinit;
# if HAVE_LTDL
    lt_dlhandle           handle;
# endif /* HAVE_LTDL */
};

/* Initialize and deinitialize the module loader. */
int  module_init     (void);
int  module_deinit   (void);

/* This function loads a module, but references it unsafely, i.e.
 * another (malicious) client may have unloaded it before it can be
 * used.
 * Arguments:
 *  name - an mstr giving the name of the module to load
 * Returns 0 on success or 1 otherwise. */
int  module_load_mstr      (char * name)                    NONNULL;

/* This function unloads a module which was loaded unsafely. */
void module_unload         (const char * name)              NONNULL;

/* This function loads a module safely, i.e. the module will be loaded
 * (through the handle) as long it is being used.
 * The handle must persist in memory (at the same address) as long as
 * the handle is loaded.
 * Arguments:
 *  name   - a string containing the name of the desired module
 *  handle - a pointer to an unloaded module_handle_t which will be
 *           filled in
 * Returns 0 on success or 1 otherwise. */
int  module_handle_load    (const char * name,
                            module_handle_t * handle)       NONNULL;

/* Same as module_handle_load, but uses takes an mstr in the name
 * argument. */
int module_handle_load_mstr(char * name,
                            module_handle_t * handle)       NONNULL;

/* This function unloads the module (if it is not used elsewhere).
 * The handle is released and may not be used again prior to a
 * reinitialization.
 * Arguments:
 *  handle - a pointer to a loaded module_handle_t which will be
 *           unloaded */
void module_handle_unload  (module_handle_t * handle)       NONNULL;

#endif /* !MODULE_H */
