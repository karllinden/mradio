/*
 * This file is part of mradio.
 *
 * Copyright (C) 2017 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#ifndef FETCHER_H
# define FETCHER_H 1

# if HAVE_CONFIG_H
#  include <config.h>
# endif /* HAVE_CONFIG_H */

# include <mradio/common.h>

/* Select a fetcher to be loaded upon fetcher_init. */
int  fetcher_set (char * name) NONNULL; /* mstr */

/* Intialize the fetcher of choice.
 * This function must not be called when the program runs more than one
 * thread.
 * During the lifetime of the daemon, this function shall be called only
 * once. */
int  fetcher_init(void);

/* Deinitialize the fetcher of choice.
 * Otherwise the same remarks as those for fetcher_init hold. */
void fetcher_deinit(void);

#endif /* !FETCHER_H */
