/*
 * This file is part of mradio.
 *
 * Copyright (C) 2016 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <mradio/common.h>

#include "arraypick.h"

int NONNULLA(2)
arraypick(unsigned size,
          arraypick_func_t * func,
          void * data)
{
    /* Find values m and n such that size = 2^m - 1 + n, where m is as
     * large as possible. Then m is the height of the completely filled
     * binary tree and n is the number of leaf nodes. Here m is as big
     * as possible. */
    int      ret;
    unsigned m    = 0;
    unsigned n    = 0;
    unsigned mask = 0; /* 2^m - 1 */
    unsigned i, j, k, l, p;

    while (size > mask)
    {
        mask <<= 1;
        mask |= 1;
        ++m;
    }
    if (mask > size)
    {
        mask >>= 1;
        --m;
    }
    n = size - mask;

    k = 1;
    for (unsigned level = 1u << (m-1); level; level /= 2)
    {
        p = 1;
        l = k;
        while (l--)
        {
            i = p * level;
            if (i <= n)
            {
                i *= 2;
            }
            else
            {
                i += n;
            }
            ret = (*func)(i-1, data);
            if (ret)
            {
                return ret;
            }
            p += 2;
        }
        k *= 2;
    }

    j = 1;
    while (n--)
    {
        ret = (*func)(j-1, data);
        if (ret)
        {
            return ret;
        }
        j += 2;
    }

    return 0;
}

