/*
 * This file is part of mradio.
 *
 * Copyright (C) 2017 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

/* FIXME: make the command interface available to modules */

/* FIXME: rwlock */
#define MRADIO_LOG_DOMAIN MRADIO_LOG_DOMAIN_DAEMON

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <stdlib.h>
#include <string.h>

#include <mradio/common.h>
#include <mradio/log.h>

#include <common/log-debug.h>

#include "arraypick.h"
#include "avl.h"
#include "command.h"

static avl_dp_t commands;

static int
command_cmp_func(const void * key,
                 const void * data,
                 const void * extra)
{
    const char *      name    = key;
    const command_t * command = data;
    return strcmp(name, command->name);
}

void
command_init(void)
{
    avl_dp_init(&commands, &command_cmp_func, NULL, NULL);
    return;
}

void
command_deinit(void)
{
    avl_dp_deinit(&commands);
    return;
}

int NONNULL
command_register(const command_t * command)
{
    return avl_dp_insert(&commands, command->name, command);
}

static int
command_pick(unsigned index, void * data)
{
    const command_t * array = data;
    return command_register(array + index);
}

int NONNULL
command_register_array(const command_t * array,
                       unsigned size)
{
    return arraypick(size, &command_pick, (void *)array);
}

static int NONNULL
command_cmp(const void * p1, const void * p2)
{
    const command_t * c1 = p1;
    const command_t * c2 = p2;
    return strcmp(c1->name, c2->name);
}

static int32_t
subcmd_call(const command_t * subcommands,
            size_t size,
            uint8_t argc,
            char ** argv,
            return_t ** retp)
{
    const command_t * command;
    command_t         key;

    if (argc == 1)
    {
        log_error("expected a subcommand to %s", argv[0]);
        return 1;
    }

    argc--;
    argv++;
    key.name = argv[0];
    command = bsearch(&key, subcommands, size, sizeof(command_t),
                      &command_cmp);
    if (!command)
    {
        log_error("could not find subcommand %s", key.name);
        return 1;
    }
    else if (command->func)
    {
        log_debug("invoking subcommand %s", argv[0]);
        return (*command->func)(argc, argv, retp);
    }
    else
    {
        log_debug("empty subcommand %s", argv[0]);
        return 0;
    }
}


/* First argument, argv[0], is the command name. */
int32_t NONNULL
command_call(uint8_t argc,
             char ** argv,
             return_t ** retp)
{
    const command_t * command = avl_dp_search(&commands, argv[0]);
    if (command == NULL)
    {
        log_error("command \"%s\" not found", argv[0]);
        return 1;
    }
    if (command->func)
    {
        log_debug("invoking command %s", argv[0]);
        return (*command->func)(argc, argv, retp);
    }
    else if (command->subcommands)
    {
        return subcmd_call(command->subcommands,
                           command->size,
                           argc,
                           argv,
                           retp);
    }
    else
    {
        log_debug("empty command %s", argv[0]);
        return 0;
    }
}
