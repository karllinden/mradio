/*
 * This file is part of mradio.
 *
 * Copyright (C) 2016-2017 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

/* This must be set before any #includes. */
#define MRADIO_LOG_DOMAIN MRADIO_LOG_DOMAIN_SERV

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <ctype.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <mradio/log.h>
#include <mradio/mstr.h>

#include <common/asprintf.h>
#include <common/log-debug.h>
#include <common/massert.h>
#include <common/proto.h>

#include "command.h"
#include "ml.h"
#include "nonblock.h"
#include "pipe-serv.h"
#include "serv.h"

struct pipe_serv_s
{
    int                     rdfd;
    int                     wrfd;
    pipe_serv_stop_func_t * stop_func;
    int                     flags;

    int                     rd_ml_fd;
    int                     wr_ml_fd;

    /* Protocol version to use. */
    uint32_t                version;

    /*
     * String reading.
     * strp - pointer to the string to read
     * lenp - length of the string to read.
     */
    char **                 strp; /* mstr pointer */
    uint8_t                 len;

    /* Stuff left to read. */
    size_t                  left;
    char *                  dest;

    /* Authentication stuff. */
    char *                  cpass; /* correct password */ /* mstr */
    char *                  rpass; /* received password */

    /*
     * What kind of stuff are we currently receiving.
     * Possible values:
     *   0 (not receiving anything yet)
     *   PROTO_CMD (receiving a command)
     */
    uint8_t                 token;

    /* Command stuff. */
    char **                 argv; /* argument vector, mstr */
    size_t                  argvsiz; /* allocated size of argv */
    uint8_t                 argc; /* argument count */
    uint8_t                 arg; /* next/current argument */

    /* Data for writing. */
    char *                  buf; /* buffer holding data to write */
    size_t                  siz; /* size of buffer */
    size_t                  rpos; /* position from where to write */
    size_t                  wpos; /* position where to enqueue */

    char                    data[];
};
typedef struct pipe_serv_s pipe_serv_t;

static int PURE
pipe_serv_isset(pipe_serv_t * ps,
                int flags)
{
    return ps->flags & flags;
}

static void
pipe_serv_set(pipe_serv_t * ps,
              int flags)
{
    ps->flags |= flags;
    return;
}

/* Enqueue data to be sent later on (when it is possible). */
static int NONNULL
pipe_serv_write_enqueue(pipe_serv_t * ps,
                        const void * buf,
                        size_t siz)
{
    char * new;
    size_t newsiz;

    log_debug("enqueing %ld bytes for writing", siz);

    newsiz = ps->wpos + siz;
    if (newsiz > ps->siz)
    {
        new = realloc(ps->buf, newsiz);
        if (new == NULL)
        {
            log_error_errno("could not allocate memory");
            return 1;
        }
        ps->buf = new;
        ps->siz = newsiz;
    }

    memcpy(ps->buf + ps->wpos, buf, siz);
    ps->wpos += siz;

    return 0;
}

/* Try to write as much of the buffer held at buf which is of size siz
 * as possible to the file descriptor.
 * Note that then caller cannot know if an error occured. */
static size_t NONNULL
pipe_serv_write_now(pipe_serv_t * ps,
                    const void * buf,
                    size_t siz)
{
    size_t wrt = 0; /* total bytes written */
    ssize_t ret = 0;

    log_debug("trying to write %ld bytes now", siz);

    /* Send data as long as it works. */
again:
    while (siz != 0 && (ret = write(ps->wrfd, buf, siz)) >= 0)
    {
        wrt += (size_t)ret;
        buf = ((char *)buf) + ret;
        siz -= wrt;
    }
    if (ret < 0)
    {
        /* Do not report EPIPE since it will be catched as a hungup by
         * the mainloop later on and trigger destruction or reset of
         * this server. */
        if (errno == EINTR)
        {
            goto again;
        }
        else if (errno != EAGAIN &&
                 errno != EWOULDBLOCK &&
                 errno != EPIPE)
        {
            /* Some kind of error occured, but data may have been
             * written and returning -1 or alike would throw away the
             * information how many bytes were written, which will
             * result in data loss.
             * Therefore always return how many bytes written and just
             * log all errors. */
            log_error_errno("could not write data");
        }
    }

    log_debug("wrote %ld bytes", wrt);

    return wrt;
}

static unsigned
pipe_serv_write_cb(int wr_ml_fd)
{
    serv_t *      serv;
    pipe_serv_t * ps;
    void *        buf;
    size_t        siz;
    size_t        wrt;

    serv = ml_fd_get_data(wr_ml_fd);
    ps = serv_data(serv);

    log_debug("write callback on pipe serv %p", (void *)ps);

    /* This callback shall only be registered as a callbac if there is
     * actually any data to write. */
    massert(ps->rpos < ps->wpos);
    buf = ((char *)ps->buf) + ps->rpos;
    siz = ps->wpos - ps->rpos;
    wrt = pipe_serv_write_now(ps, buf, siz);
    ps->rpos += wrt;

    /* If everything has been written reset values and detach this
     * callback. */
    if (ps->rpos == ps->wpos)
    {
        ps->rpos = 0;
        ps->wpos = 0;
        ml_fd_write_cb(ps->wr_ml_fd, NULL);
        if (pipe_serv_isset(ps, PIPE_SERV_STOP_AFTER_FLUSH))
        {
            pipe_serv_stop(serv);
        }
    }

    return 0;
}

static int NONNULL
pipe_serv_write(pipe_serv_t * ps,
                const void * buf,
                size_t siz)
{
    size_t wrt;

    log_debug("write of %ld bytes requested", siz);

    if (ps->wpos != 0)
    {
        /* There is already enqueued data which could not be written, so
         * enqueue also this data.
         * No need to update wr_ml_fd write cb since it must have been
         * updated when the data already in the queue was added. */
        return pipe_serv_write_enqueue(ps, buf, siz);
    }
    else /* if (ps->wpos == 0) */
    {
        /* There is no data enqueued so there is hope data can be sent
         * right away.
         * Try to send some data now and enqueue the rest. */
        wrt = pipe_serv_write_now(ps, buf, siz);
        if (wrt < siz)
        {
            /* Enqueue the data that was not sent and make sure it will
             * be sent later. */
            buf = ((char *)buf) + (size_t)wrt;
            siz -= (size_t)wrt;
            if (pipe_serv_write_enqueue(ps, buf, siz))
            {
                return 1;
            }
            ml_fd_write_cb(ps->wr_ml_fd, &pipe_serv_write_cb);
            return 0;
        }
        else if (pipe_serv_isset(ps, PIPE_SERV_STOP_AFTER_FLUSH))
        {
            /* Make the caller think an error occured so that it stops
             * the server. */
            log_debug("stop after flush; returning 1");
            return 1;
        }
        else
        {
            /* Everything was written and PIPE_SERV_STOP_AFTER_FLUSH is
             * not set so just return happily. */
            return 0;
        }
    }
}

/* Resets state. */
static void NONNULL
pipe_serv_reset(pipe_serv_t * ps)
{
    log_debug("reseting pipe serv %p", (void *)ps);
    ps->flags   &= PIPE_SERV_FLAGS;
    ps->version  = 0;
    ps->strp     = NULL;
    ps->len      = 0;
    ps->left     = 0;
    ps->dest     = NULL;
    ps->token    = 0;
    ps->argc     = 0;
    ps->arg      = 0;
    ps->wpos     = 0;
    ps->rpos     = 0;
    return;
}

/* Print a warning message and stop in case of an error. */
static unsigned
pipe_serv_error_cb(int ml_fd)
{
    serv_t *      serv;
    pipe_serv_t * ps;

    serv = ml_fd_get_data(ml_fd);
    ps = serv_data(serv);

    log_warn("error on pipe serv %p (fd %d)", (void *)ps,
             ml_fd_get_fd(ml_fd));
    pipe_serv_stop(serv);

    return 0;
}

static unsigned
pipe_serv_hangup_cb(int ml_fd)
{
    serv_t *      serv;
    pipe_serv_t * ps;

    serv = ml_fd_get_data(ml_fd);
    ps = serv_data(serv);

    log_debug("hangup on pipe serv %p", (void *)ps);

    pipe_serv_reset(ps);

    /* If the server should keep going just return from here and go on,
     * otherwise stop. */
    if (pipe_serv_isset(ps, PIPE_SERV_STOP_ON_HUP))
    {
        pipe_serv_stop(serv);
    }
    return 0;
}

static void NONNULL
pipe_serv_log(int has_errno,
              log_domain_t domain,
              log_level_t level,
              const char * fmt,
              va_list ap,
              void * data)
{
    pipe_serv_t * ps = data;
    uint8_t       log = PROTO_LOG;
    int           ret;
    char *        msg;
    uint16_t      siz;

    const char *  err;
    char *        fmtmsg;

    /* Because there is no guarantee that errno codes in client and
     * server are the same, the error message must be formatted here. */
    if (has_errno)
    {
        err = strerror(errno);
        if (err == NULL)
        {
            err = "unknown error";
        }

        ret = vasprintf(&fmtmsg, fmt, ap);
        if (ret < 0)
        {
            log_error("vasprintf failed");
            return;
        }

        ret = asprintf(&msg, "%s: %c%s", fmtmsg, tolower(*err), err+1);
        free(fmtmsg);
        if (ret < 0)
        {
            log_error("asprintf failed");
            return;
        }
    }
    else
    {
        ret = vasprintf(&msg, fmt, ap);
        if (ret < 0)
        {
            log_error("vasprintf failed");
            return;
        }
    }
    siz = (uint16_t)ret;

    pipe_serv_write(ps, &log, sizeof(uint8_t));
    pipe_serv_write(ps, &domain, sizeof(log_domain_t));
    pipe_serv_write(ps, &level, sizeof(log_level_t));
    pipe_serv_write(ps, &siz, sizeof(uint16_t));
    pipe_serv_write(ps, msg, siz);

    free(msg);

    return;
}

/* Just send a message that the client is authenticated and begin taking
 * up requests from the client. */
static int
pipe_serv_auth(pipe_serv_t * ps)
{
    uint8_t auth = PROTO_AUTH;
    pipe_serv_set(ps, PIPE_SERV_AUTH);
    if (pipe_serv_write(ps, &auth, sizeof(uint8_t)))
    {
        return 1;
    }
    return 0;
}

/* Challenges the client with a password (if any) otherwise it just
 * authenticates the client. */
static int NONNULL
pipe_serv_password(pipe_serv_t * ps)
{
    uint8_t       pass = PROTO_PASSWORD;

    log_debug("pipe_serv_password %p", (void *)ps);

    /* If the server is not password protected just authenticate. */
    if (ps->cpass == NULL)
    {
        if (pipe_serv_auth(ps))
        {
            return 1;
        }
        return 0;
    }

    /* Password authentication required. */
    if (pipe_serv_write(ps, &pass, sizeof(uint8_t)))
    {
        return 1;
    }

    return 0;
}

/* Return values:
 *  1 - there is more data to receive
 *  0 - all data received
 * -1 - error or shutdown (connection should be closed) */
static int
pipe_serv_read(pipe_serv_t * ps,
               void * buf,
               size_t len)
{
    size_t  left = len;
    char *  dest = buf;
    ssize_t ret;

    log_debug("atempting to read %lu bytes from client", len);

    while (left > 0)
    {
        ret = read(ps->rdfd, dest, left);
        if (ret < 0)
        {
            if (errno == EAGAIN || errno == EWOULDBLOCK)
            {
                log_debug("EAGAIN");
                break;
            }
            else if (errno == EINTR)
            {
                log_debug("EINTR");
                continue;
            }
            else
            {
                log_error_errno("could not read data");
                return -1;
            }
        }
        else if (ret == 0)
        {
            /* EOF */
            log_debug("read from %d returned EOF", ps->rdfd);
            return -1;
        }
        else
        {
            left -= (size_t)ret;
            dest += ret;
        }
    }

    ps->left = left;
    log_debug("%lu bytes left to read", ps->left);
    if (left == 0)
    {
        ps->dest = NULL;
        return 0;
    }
    else
    {
        ps->dest = dest;
        return 1;
    }
}

/* Second stage of reading a string, invoked after length has been fully
 * received.
 * Returns:
 *  + - more to read
 *  0 - success, everything read
 *  - - fatal errror */
static int NONNULL
pipe_serv_read_string2(pipe_serv_t * ps)
{
    char ** strp = ps->strp; /* mstr pointer */

    /* Reset to reading no string, since the low level code will take
     * care of the rest when we are done with this function. */
    ps->strp = NULL;

    if (ps->len == 0)
    {
        /* This is invalid. */
        log_error("invalid string length");
        return -1;
    }

    *strp = mstr_alloc(ps->len);
    if (*strp == NULL)
    {
        log_error_errno("could not allocate memory");
        return -1;
    }
    (*strp)[ps->len] = '\0';
    return pipe_serv_read(ps, *strp, ps->len);
}

/* Helper function for reading strings.
 * Returns:
 *  + - more to read
 *  0 - success, everything read
 *  - - fatal error */
static int NONNULL
pipe_serv_read_string(pipe_serv_t * ps,
                      char ** strp)
{
    int ret;
    ps->strp = strp;
    ret = pipe_serv_read(ps, &ps->len, sizeof(uint8_t));
    if (ret != 0)
    {
        return ret;
    }
    return pipe_serv_read_string2(ps);
}

static int NONNULL
pipe_serv_write_mstr(pipe_serv_t * ps, char * m)
{
    uint16_t len;
    len = (uint16_t)mstr_len(m);
    return pipe_serv_write(ps, &len, sizeof(uint16_t)) ||
           pipe_serv_write(ps, m, len);
}

static int NONNULL
pipe_serv_write_return(pipe_serv_t * ps, return_t * ret)
{
    uint32_t      len;
    void        * ptr;
    return_type_t type;
    uint8_t       token;

    token = PROTO_RETURN_ARRAY;
    len   = return_len(ret);
    if (pipe_serv_write(ps, &token, sizeof(uint8_t)) ||
        pipe_serv_write(ps, &len, sizeof(uint32_t)))
    {
        return 1;
    }

    for (uint32_t i = 0; i < len; ++i)
    {
        ptr = return_get(ret, i, &type);
        switch (type)
        {
            case RETURN_TYPE_ARRAY:
                if (pipe_serv_write_return(ps, ptr))
                {
                    return 1;
                }
                return_unref(ptr);
                break;
            case RETURN_TYPE_STRING:
                token = PROTO_RETURN_STRING;
                if (pipe_serv_write(ps, &token, sizeof(uint8_t)) ||
                    pipe_serv_write_mstr(ps, ptr))
                {
                    return 1;
                }
                mstr_unref(ptr);
                break;
            default:
                log_warn("server side programming error; "
                         "sending PROTO_RETURN_NONE");
                token = PROTO_RETURN_NONE;
                if (pipe_serv_write(ps, &token, sizeof(uint8_t)))
                {
                    return 1;
                }
                break;
        }
    }

    return 0;
}

/* Returns:
 *  + - more to read
 *  0 - success everything read
 *  - - fatal error */
static int NONNULL
pipe_serv_read_cmd(pipe_serv_t * ps)
{
    int        r;
    size_t     newsiz;
    uint8_t    token;
    int32_t    status;
    return_t * ret = NULL;

    if (ps->argc == 0)
    {
        r = pipe_serv_read(ps, &ps->argc, sizeof(uint8_t));
        if (r != 0)
        {
            return r;
        }
    }

    if (ps->argc >= ps->argvsiz)
    {
        free(ps->argv);

        newsiz = (size_t)ps->argc + 1;
        ps->argv = malloc(newsiz * sizeof(char *));
        if (ps->argv == NULL)
        {
            ps->argvsiz = 0;
            log_error_errno("could not allocate memory");
            return -1;
        }
        ps->argvsiz = newsiz;
        memset(ps->argv, 0, newsiz * sizeof(char *));
    }

    while (ps->arg < ps->argc)
    {
        r = pipe_serv_read_string(ps, &(ps->argv[ps->arg]));
        ++ps->arg;
        if (r != 0)
        {
            return r;
        }
    }

    /* This is not needed since argv is wiped upon allocation and a
     * complete read. */
    /* ps->argv[ps->argc] = NULL; */

    log_redirect(&pipe_serv_log, ps);
    status = command_call(ps->argc, ps->argv, &ret);
    log_redirect(NULL, NULL);

    if (ret)
    {
        r = pipe_serv_write_return(ps, ret);
    }
    return_unref(ret);
    if (ret && r)
    {
        return -1;
    }

    token = PROTO_STATUS;
    if (pipe_serv_write(ps, &token, sizeof(uint8_t)) ||
        pipe_serv_write(ps, &status, sizeof(int32_t)))
    {
        return -1;
    }

    /* Reset state. */
    for (uint8_t i = 0; i < ps->argc; ++i)
    {
        mstr_unref(ps->argv[i]);
        ps->argv[i] = NULL;
    }
    ps->argc = 0;
    ps->arg  = 0;

    return 0;
}

/* Returns:
 *  + - more to read
 *  0 - success
 *  - - fatal error */
static int NONNULL
pipe_serv_read_auth(pipe_serv_t * ps)
{
    int ret;

    if (ps->token == 0)
    {
        ret = pipe_serv_read(ps, &ps->token, sizeof(uint8_t));
        if (ret != 0)
        {
            return ret;
        }
    }

    switch (ps->token)
    {
        case PROTO_CMD:
            ret = pipe_serv_read_cmd(ps);
            break;
        case PROTO_END:
            /* Client wants to end communication, so reset the pipe
             * server, but do not close it since it might be listening
             * on a pipe, which should serve more clients. */
            log_debug("end on pipe server %p", (void *)ps);
            pipe_serv_reset(ps);
            ret = 0;
            break;
        default:
            log_error("invalid token %u", ps->token);
            ret = -1;
            break;
    }

    /* If everything has been read make sure to reset token so as to
     * reset the ordinary communication stage. */
    if (ret == 0)
    {
        ps->token = 0;
    }

    return ret;
}

static inline void
pipe_serv_stop_after_flush(pipe_serv_t * ps)
{
    pipe_serv_set(ps, PIPE_SERV_STOP_AFTER_FLUSH);
    ml_fd_read_cb(ps->rd_ml_fd, NULL);
    return;
}

/* Returns:
 *  + - more to read
 *  0 - success
 *  - - fatal error */
static int NONNULL
pipe_serv_read_pass(pipe_serv_t * ps)
{
    int           ret;

    if (ps->rpass == NULL)
    {
        ret = pipe_serv_read_string(ps, &ps->rpass);
        if (ret != 0)
        {
            return ret;
        }
    }

    if (strcmp(ps->rpass, ps->cpass) == 0)
    {
        /* Correct password. */
        if (pipe_serv_auth(ps))
        {
            return -1;
        }
    }
    else
    {
        /* Wrong password. */
        /* If server should stop on failed authentication, make sure it
         * stops after the last data has been written so that the client
         * can know that authentication failed without just seeing that
         * the server hungup. */
        if (pipe_serv_isset(ps, PIPE_SERV_STOP_ON_AUTH_FAIL))
        {
            pipe_serv_stop_after_flush(ps);
        }

        /* Re-issue the challenge, to tell the client that the
         * authentication failed. */
        if (pipe_serv_password(ps))
        {
            return -1;
        }
    }

    /* We made it this far, so everything is read and the password has
     * been checked.
     * No matter what the state must be reset so a new password can be
     * received later. */
    free(ps->rpass);
    ps->rpass = NULL;
    return 0;
}

/* Returns:
 *  + - more to read
 *  0 - success
 *  - - fatal error, please stop server */
static int
pipe_serv_read_real(pipe_serv_t * ps)
{
    uint8_t  deprecated = PROTO_DEPRECATED;
    uint32_t version    = PROTO_VERSION;
    int      ret;

    /* Low-level reading code. */
    if (ps->left > 0)
    {
        ret = pipe_serv_read(ps, ps->dest, ps->left);
        if (ret != 0)
        {
            return ret;
        }
    }

    /* Read a string as requested. */
    if (ps->strp != NULL)
    {
        /* The low-level code has read the length, so invoke the second
         * stage string reading. */
        ret = pipe_serv_read_string2(ps);
        if (ret != 0)
        {
            return ret;
        }
    }

    if (pipe_serv_isset(ps, PIPE_SERV_AUTH))
    {
        /* Ordinary communication. */
        ret = pipe_serv_read_auth(ps);
        if (ret != 0)
        {
            return ret;
        }
    }
    else if (pipe_serv_isset(ps, PIPE_SERV_VERSION_IS_CHECKED))
    {
        /* Authentication. */
        ret = pipe_serv_read_pass(ps);
        if (ret != 0)
        {
            return ret;
        }
    }
    else if (pipe_serv_isset(ps, PIPE_SERV_BEGUN))
    {
        if (ps->version == 0)
        {
            log_debug("reading client version");
            ret = pipe_serv_read(ps, &ps->version, sizeof(uint32_t));
            if (ret != 0)
            {
                return ret;
            }
        }

        /* Version not checked. */
        if (ps->version > PROTO_VERSION)
        {
            ps->version = PROTO_VERSION;
        }

        if (ps->version < PROTO_LEAST_VERSION)
        {
            /* Tell the client that it is too old. */
            if (pipe_serv_write(ps, &deprecated, sizeof(uint8_t)))
            {
                return -1;
            }

            if (pipe_serv_isset(ps, PIPE_SERV_STOP_ON_DEPRECATED))
            {
                /* Make sure the server is stopped as soon as all data
                 * has been completely written. */
                pipe_serv_stop_after_flush(ps);
            }
            else
            {
                pipe_serv_reset(ps);
            }
        }
        else
        {
            /* Good client version. */
            pipe_serv_set(ps, PIPE_SERV_VERSION_IS_CHECKED);
            if (pipe_serv_password(ps))
            {
                return -1;
            }
        }
    }
    else
    {
        if (ps->token == 0)
        {
            ret = pipe_serv_read(ps, &ps->token, sizeof(uint8_t));
            if (ret)
            {
                return ret;
            }
        }
        if (ps->token != 0)
        {
            if (ps->token == PROTO_BEGIN)
            {
                pipe_serv_set(ps, PIPE_SERV_BEGUN);
                log_debug("sending version %u to client", version);
                if (pipe_serv_write(ps, &version, sizeof(uint32_t)))
                {
                    return -1;
                }
            }
            else
            {
                log_warn("no begin at pipe serv %p", (void *)ps);
            }
        }
        ps->token = 0;
    }

    return 0;
}

static unsigned
pipe_serv_read_cb(int rd_ml_fd)
{
    serv_t *      serv;
    pipe_serv_t * ps;
    int           ret;

    serv = ml_fd_get_data(rd_ml_fd);
    ps = serv_data(serv);

    ret = pipe_serv_read_real(ps);
    if (ret < 0)
    {
        /* A fatal error occured so stop the server. */
        pipe_serv_stop(serv);
    }

    return 0;
}

serv_t *
pipe_serv_start(char * name, /* mstr */
                char * password, /* mstr */
                int rdfd,
                int wrfd,
                size_t data_size,
                pipe_serv_stop_func_t * stop_func,
                int flags)
{
    serv_t *      serv;
    pipe_serv_t * ps;

    serv = serv_new(name, SERV_PIPE, sizeof(pipe_serv_t) + data_size);
    if (serv == NULL)
    {
        return NULL;
    }

    /* stop_func must be NULL until just before this function returns
     * since otherwise the caller has no way of initializing the custom
     * data before it is destroyed by the stop function in case of an
     * error in this function */
    ps = serv_data(serv);
    log_debug("starting pipe serv %p at serv %p", (void *)ps,
              (void *)serv);

    ps->rdfd      = rdfd;
    ps->wrfd      = wrfd;
    ps->stop_func = NULL;
    ps->flags     = flags & PIPE_SERV_FLAGS;

    ps->rd_ml_fd  = -1;
    ps->wr_ml_fd  = -1;

    ps->version   = 0;

    ps->strp      = NULL;
    ps->len       = 0;

    ps->left      = 0;
    ps->dest      = NULL;

    mstr_ref(password);
    ps->cpass     = password;
    ps->rpass     = NULL;

    ps->token     = 0;

    ps->argv      = NULL;
    ps->argvsiz   = 0;
    ps->argc      = 0;
    ps->arg       = 0;

    ps->buf       = NULL;
    ps->siz       = 0;
    ps->rpos      = 0;
    ps->wpos      = 0;

    ps->rd_ml_fd = ml_fd_new(rdfd, serv);
    if (ps->rd_ml_fd < 0)
    {
        goto error;
    }
    if (wrfd == rdfd)
    {
        ps->wr_ml_fd = ps->rd_ml_fd;
    }
    else
    {
        ps->wr_ml_fd = ml_fd_new(wrfd, serv);
        if (ps->wr_ml_fd < 0)
        {
            goto error;
        }
    }

    /* The file descriptors must be in nonblocking mode. */
    if (nonblock(rdfd) || (wrfd != rdfd && nonblock(wrfd)))
    {
        goto error;
    }

    ml_fd_read_cb(ps->rd_ml_fd, &pipe_serv_read_cb);
    ml_fd_error_cb(ps->rd_ml_fd, &pipe_serv_error_cb);
    if (wrfd != rdfd)
    {
        ml_fd_error_cb(ps->wr_ml_fd, &pipe_serv_error_cb);
    }
    ml_fd_hangup_cb(ps->rd_ml_fd, &pipe_serv_hangup_cb);
    if (wrfd != rdfd)
    {
        ml_fd_hangup_cb(ps->wr_ml_fd, &pipe_serv_hangup_cb);
    }

    /* Set stop_func late. Now it is up to the caller to initialize the
     * data so that it can be safely destroyed by the stop function. */
    log_debug("pipe serv startup succeeded for %p", (void *)ps);
    ps->stop_func = stop_func;
    return serv;
error:
    log_debug("pipe serv startup failed for %p", (void *)ps);
    pipe_serv_stop(serv);
    return NULL;
}

void
pipe_serv_stop(serv_t * serv)
{
    pipe_serv_t * ps = serv_data(serv);

    log_debug("stopping pipe serv %p", (void *)ps);

    mstr_unref(ps->cpass);
    free(ps->rpass);
    free(ps->argv);
    free(ps->buf);

    if (ps->wr_ml_fd >= 0)
    {
        if (ps->wr_ml_fd == ps->rd_ml_fd)
        {
            ps->wr_ml_fd = -1;
        }
        else
        {
            ml_fd_destroy(ps->wr_ml_fd);
        }
    }
    if (ps->rd_ml_fd >= 0)
    {
        ml_fd_destroy(ps->rd_ml_fd);
    }

    if (ps->stop_func != NULL)
    {
        (*ps->stop_func)(serv);
    }

    serv_destroy(serv);

    return;
}

void * NONNULL PURE
pipe_serv_data(serv_t * serv)
{
    pipe_serv_t * ps = serv_data(serv);
    return ps->data;
}

int NONNULL PURE
pipe_serv_rdfd(serv_t * serv)
{
    pipe_serv_t * ps = serv_data(serv);
    return ps->rdfd;
}

int NONNULL PURE
pipe_serv_wrfd(serv_t * serv)
{
    pipe_serv_t * ps = serv_data(serv);
    return ps->wrfd;
}
