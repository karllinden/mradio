/*
 * This file is part of mradio.
 *
 * Copyright (C) 2014-2017 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#define MRADIO_LOG_DOMAIN MRADIO_LOG_DOMAIN_DAEMON

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <dirent.h>
#include <errno.h>
#include <signal.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include <gop.h>

#include <mradio/common.h>
#include <mradio/log.h>
#include <mradio/mstr.h>
#include <mradio/user.h>

#include <common/call.h>
#include <common/massert.h>
#include <common/prepend-envvar.h>
#include <common/print-version.h>
#include <common/run-shell.h>

#include "builtin-commands.h"
#include "bookmark.h"
#include "command.h"
#include "fetcher.h"
#include "main.h"
#include "module.h"
#include "ml.h"
#include "pidfile.h"
#include "pipe-serv.h"
#include "player.h"
#include "serv.h"

#define SIGNAL SIGUSR1

/* Set to non-zero if the daemon should act as a system daemon. */
static int          is_system = 0;

/*  Shell to spawn for reading configuration scripts. */
static char *       shell  = "/bin/sh";

/* Configuration script to read instead of the default. */
static char *       config = NULL;

/* Pipe that is used for the temporary startup script server. */
#define TO 0 /* to daemon */
#define FROM 1 /* from daemon */
#define RD 0
#define WR 1
static int          pipefd[2][2] = {{-1, -1}, {-1,-1}};
static serv_t *     pipeserv  = NULL;

static void
pipefd_close(void)
{
    for (unsigned i = 0; i < 2; ++i)
    {
        for (unsigned j = 0; j < 2; ++j)
        {
            if (pipefd[i][j] >= 0 && close(pipefd[i][j]))
            {
                log_error_errno("could not close pipe end");
            }
            pipefd[i][j] = -1;
        }
    }
    return;
}

static void
pipeserv_stop_func(serv_t * serv)
{
    pipefd_close();
    pipeserv = NULL;
    return;
}

/* Returns zero on success and non-zero otherwise. */
static int NONNULL
run_shell_in_dir(char * argv[])
{
    char * name;
    char * slash;
    char * copy;
    size_t len;
    int    ret;

    /* Always read the script from the containing directory. Duplicate
     * the string to avoid SIGSEGV. */
    name = argv[1];
    slash = strrchr(name, '/');
    if (slash != NULL)
    {
        len = (size_t)(slash - name);
        copy = malloc(len + 1);
        if (copy == NULL)
        {
            log_error_errno("could not allocate memory");
            return -1;
        }
        memcpy(copy, name, len);
        copy[len] = '\0';
        ret = chdir(copy);
        if (ret)
        {
            log_error_errno("could not change directory to %s", copy);
        }
        free(copy);
        if (ret)
        {
            return -1;
        }
    }

    return run_shell(argv);
}

static int NONNULL
script_filter(const struct dirent * d)
{
    if (d->d_type == DT_LNK || d->d_type == DT_REG)
    {
        return 1;
    }
    else
    {
        log_info("skipping %s", d->d_name);
        return 0;
    }
}

/* Returns 0 if the operation succeeded, a positive value if the
 * directory did not exist or if name is not a directory and a negative
 * value on error. */
static int NONNULL
read_directory_scripts(char * name, char * argv[])
{
    int              retval;
    struct dirent ** files = NULL;

    int              n;
    int              ret;

    n = scandir(name, &files, &script_filter, &alphasort);
    if (n < 0)
    {
        if (errno == ENOENT || errno == ENOTDIR)
        {
            retval = 1;
        }
        else
        {
            log_error_errno("could not scan %s", name);
            retval = -1;
        }
    }
    else
    {
        ret = chdir(name);
        if (ret < 0)
        {
            log_error_errno("could not change directory to %s", name);
            retval = -1;
        }
        else
        {
            retval = 0;
            for (int i = 0; i < n; ++i)
            {
                argv[1] = files[i]->d_name;
                if (run_shell(argv))
                {
                    retval = -1;
                }
            }
        }
    }

    free(files);
    return retval;
}

static int
read_startup_scripts(int argc, char * argv[])
{
    /* It is rather unnecessary to spawn the mradio client just to read
     * the startup scripts due to the following reasons:
     * (1) the client makes sure there is a server and optionally spawns
     *     one, which is useless here,
     * (2) the client parses options, which is not needed here,
     * (3) the client sets up environment variables for interactive
     *     environments as well,
     * (4) invoking the client as an intermediate here requires one
     *     extra fork()+exec() and wastes processor by setting up the
     *     environment on each invokation of mradio.
     * Therefore the environment is just setup and the shell is spawned
     * to read the scripts here, which is much more efficient. */
    int     exit_status = EXIT_FAILURE;
    int     new_argc    = argc + 1;
    char ** new_argv    = NULL;
    int     read_system = 1;
    char *  name        = NULL;
    int     ret;

    new_argv = malloc((unsigned)(new_argc + 1) * sizeof(char *));
    if (new_argv == NULL)
    {
        log_error_errno("could not allocate new argv");
        goto error;
    }

    new_argv[0] = shell;
    /* new_argv[1] = <config file to read>; */
    memcpy(new_argv + 2, argv + 1, (unsigned)argc * sizeof(char *));
    massert(argv[argc] == NULL);
    massert(new_argv[new_argc] == NULL);

    /* Hook up the communication pipes on standard I/O. */
    do {
        errno = 0;
    } while (dup2(pipefd[TO][WR], STDOUT_FILENO) && errno == EINTR);
    if (errno)
    {
        log_error_errno("dup2 failed");
        goto error;
    }
    do {
        errno = 0;
    } while (dup2(pipefd[FROM][RD], STDIN_FILENO) && errno == EINTR);
    if (errno)
    {
        log_error_errno("dup2 failed");
        goto error;
    }

    /* Make sure the utilities know that the server is waiting on
     * standard I/O. */
    if (setenv("MRADIO_STDIO", "1", 0))
    {
        log_error_errno("could not set MRADIO_STDIO");
        goto error;
    }

    /* If --config was given or MRADIO_CONFIG was set, then use that one
     * and no more. Otherwise read different files depending on whether
     * the daemon runs as a system user or a regular user.
     *
     * SYSTEM
     * ------
     * (1) Read /etc/mradio if it exists.
     * (2) If /etc/mradio.d/ is a directory, read all files in it in
     *     lexical order.
     *
     * USER
     * ----
     * (1) Read ${HOME}/.mradio/config if it exists.
     * (2) If ${HOME}/.mradio/config.d is a directory, read all files in
     *     it in lexical order.
     * (3) If none of the above existed read as under SYSTEM. */

    /* Respect MRADIO_CONFIG */
    if (config == NULL)
    {
        config = getenv("MRADIO_CONFIG");
    }

    if (config != NULL)
    {
        /* Read the given config script. */
        new_argv[1] = config;
        if (run_shell_in_dir(new_argv))
        {
            goto error;
        }
    }
    else
    {
        if (!is_system)
        {
            /* Read user files, if any, otherwise request reading system
             * files. */
            name = userfile("config");
            if (name == NULL)
            {
                goto error;
            }
            if (!access(name, R_OK))
            {
                new_argv[1] = name;
                if (run_shell_in_dir(new_argv))
                {
                    goto error;
                }
                read_system = 0;
            }
            mstr_unref(name);
            name = userfile("config.d");
            if (name == NULL)
            {
                goto error;
            }
            ret = read_directory_scripts(name, new_argv);
            if (ret < 0)
            {
                goto error;
            }
            else if (ret == 0)
            {
                read_system = 0;
            }
            mstr_unref(name);
        }
        if (read_system)
        {
            /* Read system config scripts. */
            name = SYSCONFDIR "/" PACKAGE;
            if (!access(name, R_OK))
            {
                new_argv[1] = name;
                if (run_shell_in_dir(new_argv))
                {
                    goto error;
                }
            }
            name = SYSCONFDIR "/" PACKAGE ".d";
            ret = read_directory_scripts(name, new_argv);
            if (ret < 0)
            {
                goto error;
            }
        }
    }

    exit_status = EXIT_SUCCESS;
error:
    free(new_argv);
    return exit_status;
}

/* Start a child process that will read startup scripts (in the correct
 * order). The major reason for first forking a child that in turn
 * fork()+exec()s instances of the mradio client for each file is that
 * doing multiple single forks can result in forking when pthread_create
 * has been called, in which case stuff gets unnecessarily complex.
 *
 * Make sure pipe() has been called prior to this call.
 *
 * Before the fork SIGUSR1 is blocked, so that it will be blocked in the
 * forked process as well. Then it is unblocked in the parent again, but
 * kept in the child. Note that this is necessary, since blocking in the
 * child process causes a race condition; the parent could send SIGUSR1
 * before the child has blocked it. */
static pid_t
start_child(int argc, char * argv[])
{
    sigset_t set;
    sigset_t old;
    int      sig = SIGNAL;
    pid_t    pid;
    int      status;

    CALL(sigemptyset, &set);
    CALL(sigaddset, &set, sig);
    PCALL(pthread_sigmask, SIG_BLOCK, &set, &old);

    pid = fork();
    if (pid < 0)
    {
        log_error_errno("could not fork");
        return pid;
    }
    else if (pid == 0)
    {
        /* Child process. */

        /* Close unnecessary pipe ends. */
        if (close(pipefd[TO][RD]) + close(pipefd[FROM][WR]))
        {
            log_error_errno("could not close pipe end");
        }
        pipefd[TO][RD] = -1;
        pipefd[FROM][WR] = -1;

        /* Wait for the daemon to signal that it is ready to handle the
         * clients' requests. */
        PCALL(sigwait, &set, &sig);

        /* Read startup scritpts. Finally. */
        status = read_startup_scripts(argc, argv);
        exit(status);
    }

    PCALL(pthread_sigmask, SIG_SETMASK, &old, NULL);

    return pid;
}

/* Called from the main loop code when the child is ready to be waited
 * for. */
int
wait_child(void)
{
    int    retval = 1;
    pid_t  child;
    int    wstatus;
    info_t info;

    info_init(&info);

    log_debug("wait_child triggered");

    child = waitpid(-1, &wstatus, WNOHANG);
    if (child == 0)
    {
        /* Spurious SIGCHLD. */
    }
    else if (child < 0)
    {
        if (errno == ECHILD)
        {
            /* Spurious SIGCHLD. */
        }
        else
        {
            log_error_errno("waitpid failed");
            goto error;
        }
    }
    else
    {
        if (WIFEXITED(wstatus))
        {
            if (WEXITSTATUS(wstatus))
            {
                goto error;
            }

            /* Child exited successfully. */
            /* Stop the temporary pipe server and start the regular
             * server. */
            if (info_getenv(&info))
            {
                goto error;
            }
            if (serv_start(&info) == NULL)
            {
                goto error;
            }
        }
        else
        {
            log_error("child killed by signal %d", WTERMSIG(wstatus));
            goto error;
        }
    }

    retval = 0;
error:
    info_deinit(&info);
    return retval;
}

/* FIXME: merge into main when possible */
static int
run_core(int argc, char * argv[])
{
    int    retval        = 1;
    bool   module_inited = false;
    bool   serv_inited   = false;
    pid_t  child;

    /* Start a temporary server on a pipe, that will serve the clients
     * that are spawned as part of reading the startup scripts. The
     * child is spawned as soon as possible in order to duplicate as
     * little stuff as possible and avoid unnecessary hard-to-debug
     * bugs. */
    for (unsigned i = 0; i < 2; ++i)
    {
        if (pipe(pipefd[i]))
        {
            goto error;
        }
    }
    child = start_child(argc, argv);
    if (child < 0)
    {
        goto error;
    }

    /* Close unnecessary pipe ends. */
    if (close(pipefd[TO][WR]) + close(pipefd[FROM][RD]))
    {
        log_error_errno("could not close pipe end");
    }
    pipefd[TO][WR] = -1;
    pipefd[FROM][RD] = -1;

    bookmark_init();
    command_init();

    if (module_init()) {
        goto error;
    }
    module_inited = true;
    if (serv_init())
    {
        goto error;
    }
    serv_inited = true;

    if (builtin_commands_register())
    {
        goto error;
    }

    /* Start a pipe server on the created pipe, that will serve initial
     * requests. Tell it to stop on hangup, because in that case the
     * child has stopped in which case an error shall be signaled
     * through the exit status of the client. */
    pipeserv = pipe_serv_start(NULL, NULL, pipefd[TO][RD],
                               pipefd[FROM][WR], 0, &pipeserv_stop_func,
                               PIPE_SERV_STOP_ON_HUP);
    if (pipeserv == NULL)
    {
        pipefd_close();
        goto error;
    }

    /* The daemon is now ready to handle requests from the startup
     * script reading clients. */
    CALL(kill, child, SIGNAL);

    /* Initialize fetcher now that configurations have been parsed and
     * before other threads are started. */
    if (fetcher_init())
    {
        goto error;
    }

    player_init();

    retval = ml_run();

error:
    if (pipeserv != NULL)
    {
        pipe_serv_stop(pipeserv);
    }
    if (serv_inited)
    {
        serv_stop_all();
        serv_deinit();
    }
    ml_free();
    player_deinit();
    fetcher_deinit();
    if (module_inited)
    {
        module_deinit();
    }
    command_deinit();
    bookmark_deinit();
    pidfile_unlink();
    log_close(); /* last for obvious reasons */
    return retval;
}

/* Ignore unknown options. */
static gop_return_t
aterror_gop(gop_t * gop)
{
    gop_error_t error = gop_get_error(gop);
    if (error == GOP_ERROR_UNKLOPT || error == GOP_ERROR_UNKSOPT)
    {
        return GOP_DO_CONTINUE_WO_HANDLER;
    }
    else
    {
        return GOP_DO_CONTINUE;
    }
}

int
main(int argc, char * argv[])
{
    int          exit_status = EXIT_FAILURE;

    const char * user        = NULL;
    uid_t        uid;

    char *       logfile;

    const gop_option_t options[] = {
        {"config", 'c', GOP_STRING, &config, NULL, "config file",
            "FILENAME"},
        {"shell", 's', GOP_STRING, &shell, NULL,
            "shell to spawn for config scripts [/bin/sh]", "SHELL"},
        {"user", 'u', GOP_STRING, &user, NULL,
            "user (not root) to run the daemon as", "USER"},
        {"version", 'v', GOP_NONE, NULL, &print_version,
            "print version and exit", NULL},
        GOP_TABLEEND
    };

    log_set_name(PROGRAM_NAME);

    gop_t * const gop = gop_new();
    if (gop == NULL) {
        goto error;
    }

    gop_set_program_name(gop, PROGRAM_NAME);
    gop_add_table(gop, "Program options:", options);
    gop_autohelp(gop);
    gop_extra_help(gop,
                   "Options not understood by this program will be "
                   "passed to the shell.");
    gop_aterror(gop, &aterror_gop);
    gop_parse(gop, &argc, argv);
    gop_destroy(gop);

    /* Do this early so that debug messages can be caught early. */
    if (log_getenv())
    {
        goto error;
    }
    logfile = getenv("MRADIO_LOGFILE");
    if (logfile != NULL && log_open(logfile))
    {
        return 1;
    }

    /* Respect MRADIO_USER variable. */
    if (user == NULL)
    {
        user = getenv("MRADIO_USER");
    }
    uid = getuid();
    if (uid == 0)
    {
        is_system = 1;
        if (user != NULL)
        {
            if (uid_from_user(user, &uid))
            {
                goto error;
            }
        }
        if (uid == 0)
        {
            log_fatal("running the daemon as root is not allowed");
            log_info("either give --user=USER on the command line or "
                     "set the MRADIO_USER environment variable to the "
                     "desired user");
            goto error;
        }
        if (setuid(uid))
        {
            log_fatal_errno("could not drop privileges");
            goto error;
        }
    }
    else if (user != NULL)
    {
        log_warn("ignoring user setting due to not running as root");
    }

    if (is_system)
    {
        if (setenv("MRADIO_SYSTEM", "1", 0))
        {
            log_error_errno("could not set MRADIO_SYSTEM");
            goto error;
        }
    }
    else
    {
        CALL(unsetenv, "MRADIO_SYSTEM");
    }

    if (setenv("LOCALSTATEDIR", LOCALSTATEDIR, 0))
    {
        log_error_errno("could not set LOCALSTATEDIR");
        goto error;
    }

    if (run_core(argc, argv)) {
        goto error;
    }

    exit_status = EXIT_SUCCESS;
error:
    return exit_status;
}
