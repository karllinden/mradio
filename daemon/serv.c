/*
 * This file is part of mradio.
 *
 * Copyright (C) 2014-2016 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

/* This must be set before any #includes. */
#define MRADIO_LOG_DOMAIN MRADIO_LOG_DOMAIN_SERV

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <mradio/info.h>
#include <mradio/log.h>
#include <mradio/mstr.h>
#include <mradio/sock.h>

#include <common/massert.h>

#include "pipe-serv.h"
#include "serv.h"
#include "sock-serv.h"

/* Common server stuff. */
struct serv_s
{
    char *      name; /* mstr */
    serv_type_t type;
};
typedef struct serv_s serv_t;

/* Anonymous server. */
struct serv_anon_s
{
    char *               name; /* NULL */ /* mstr */
    serv_type_t          type;
    struct serv_anon_s * prev;
    struct serv_anon_s * next;
    char                 data[];
};
typedef struct serv_anon_s serv_anon_t;

/* Named server. */
struct serv_name_s
{
    char *      name; /* non-NULL */ /* mstr */
    serv_type_t type;
    char        data[];
};
typedef struct serv_name_s serv_name_t;

/* Linked list of anonymous servers. */
static pthread_mutex_t serv_anon_mutex;
static serv_anon_t *   serv_anon_list = NULL;

/* Lexically sorted array of named servers. */
static pthread_mutex_t serv_name_mutex;
static serv_name_t **  serv_name_list      = NULL;
static size_t          serv_name_list_len  = 0;
static size_t          serv_name_list_size = 0;

int
serv_init(void)
{
    pthread_mutexattr_t attr;
    if (pthread_mutexattr_init(&attr))
    {
        log_fatal_errno("pthread_mutexattr_init");
        return 1;
    }
    pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_RECURSIVE);
    if (pthread_mutex_init(&serv_anon_mutex, &attr) ||
        pthread_mutex_init(&serv_name_mutex, &attr))
    {
        log_fatal_errno("pthread_mutex_init");
        return 1;
    }
    pthread_mutexattr_destroy(&attr);
    return 0;
}

void
serv_deinit(void)
{
    serv_stop_all();
    pthread_mutex_destroy(&serv_anon_mutex);
    pthread_mutex_destroy(&serv_name_mutex);
    return;
}

static serv_name_t * NONNULL
serv_name_lookup(const char * name)
{
    size_t        min;
    size_t        max;
    size_t        mid;
    serv_name_t * serv;
    int           cmp;

    pthread_mutex_lock(&serv_name_mutex);
    min = 0; /* included */
    max = serv_name_list_len; /* excluded */
    while (min < max)
    {
        massert(min <= max);
        mid = (min + max) / 2;
        serv = serv_name_list[mid];
        cmp = strcmp(name, serv->name);
        if (cmp == 0)
        {
            break;
        }
        else /* if (cmp != 0) */
        {
            serv = NULL;
            if (cmp > 0)
            {
                min = mid + 1; /* mid excluded, mid + 1 included */
            }
            else /* if (cmp < 0) */
            {
                max = mid; /* mid excluded */
            }
        }
    }
    pthread_mutex_unlock(&serv_name_mutex);

    return serv;
}

serv_t * NONNULL
serv_start(const info_t * info)
{
    serv_t *     serv = NULL;
    info_type_t  type;

    type = info_get_type(info);

    switch (type)
    {
        case INFO_TYPE_INET:
        case INFO_TYPE_UNIX:
            serv = sock_serv_start(info);
            break;
        default:
            log_error("unhandled info type %s", info_strtype(type));
            break;
    }

    return serv;
}

void NONNULL
serv_stop(serv_t * serv)
{
    switch (serv->type)
    {
        case SERV_PIPE:
            pipe_serv_stop(serv);
            break;
        case SERV_SOCK:
            sock_serv_stop(serv);
            break;
        default:
            log_fatal("invalid server type");
            abort();
    }
    return;
}

void NONNULL
serv_stop_name(const char * name)
{
    serv_name_t * serv = serv_name_lookup(name);
    if (serv == NULL)
    {
        log_error("could not find server %s", name);
        return;
    }
    serv_stop((serv_t *)serv);
    return;
}

static void
serv_stop_anon_all(void)
{
    pthread_mutex_lock(&serv_anon_mutex);
    while (serv_anon_list != NULL)
    {
        serv_stop((serv_t *)serv_anon_list);
    }
    pthread_mutex_unlock(&serv_anon_mutex);
    return;
}

static void
serv_stop_name_all(void)
{
    pthread_mutex_lock(&serv_name_mutex);
    while (serv_name_list_len > 0)
    {
        /* stop the last one to reduce memmove()'s */
        serv_stop((serv_t *)serv_name_list[serv_name_list_len - 1]);
    }
    free(serv_name_list);
    serv_name_list      = NULL;
    serv_name_list_len  = 0;
    serv_name_list_size = 0;
    pthread_mutex_unlock(&serv_name_mutex);
    return;
}

void NONNULL
serv_stop_all(void)
{
    serv_stop_name_all();
    serv_stop_anon_all();
    return;
}

static serv_t *
serv_new_anon(serv_type_t type,
              size_t data_size)
{
    serv_anon_t * serv;
    serv_anon_t * next;

    serv = malloc(sizeof(serv_anon_t) + data_size);
    if (serv == NULL)
    {
        log_error_errno("could not allocate memory");
        return NULL;
    }

    serv->name = NULL;
    serv->type = type;
    serv->prev = NULL;

    pthread_mutex_lock(&serv_anon_mutex);
    next = serv_anon_list;
    serv->next = next;
    if (next != NULL)
    {
        next->prev = serv;
    }
    serv_anon_list = serv;
    pthread_mutex_unlock(&serv_anon_mutex);

    return (serv_t *)serv;
}

static serv_t * NONNULL
serv_new_name(char * name, /* mstr */
              serv_type_t type,
              size_t data_size)
{
    serv_name_t *  serv;
    size_t         new_size;
    serv_name_t ** new_list;
    size_t         min;
    size_t         max;
    size_t         mid;
    int            cmp;

    serv = malloc(sizeof(serv_name_t) + data_size);
    if (serv == NULL)
    {
        log_error_errno("could not allocate memory");
        return NULL;
    }

    mstr_ref(name);
    serv->name = name;

    serv->type = type;

    pthread_mutex_lock(&serv_name_mutex);

    if (serv_name_list_size == serv_name_list_len)
    {
        new_size = serv_name_list_size + 1;
        new_list = realloc(serv_name_list,
                           new_size * sizeof(serv_name_t *));
        if (new_list == NULL)
        {
            log_error_errno("could not allocate memory");
            goto fail;
        }
        serv_name_list_size = new_size;
        serv_name_list = new_list;
    }
    massert(serv_name_list_len < serv_name_list_size);

    min = 0; /* included */
    max = serv_name_list_len; /* excluded */
    while (min < max)
    {
        massert(min <= max);
        mid = (min + max) / 2;
        cmp = strcmp(name, serv_name_list[mid]->name);
        if (cmp == 0)
        {
            log_error("a server named %s already exists", name);
            break;
        }
        else if (cmp > 0)
        {
            min = mid + 1; /* mid excluded, mid + 1 included */
        }
        else /* if (cmp < 0) */
        {
            max = mid; /* mid excluded */
        }
    }
    massert(min == max);

    memmove(serv_name_list + min + 1,
            serv_name_list + min,
            (serv_name_list_len - min) * sizeof(serv_name_t *));
    serv_name_list[min] = serv;

    if (0)
    {
    fail:
        mstr_unref(serv->name);
        free(serv);
        serv = NULL;
    }

    pthread_mutex_unlock(&serv_name_mutex);

    return (serv_t *)serv;
}

serv_t *
serv_new(char * name, /* mstr */
         serv_type_t type,
         size_t data_size)
{
    if (name == NULL)
    {
        return serv_new_anon(type, data_size);
    }
    else /* if (name != NULL) */
    {
        return serv_new_name(name, type, data_size);
    }
}

static void NONNULL
serv_destroy_anon(serv_anon_t * serv)
{
    serv_anon_t * prev;
    serv_anon_t * next;

    pthread_mutex_lock(&serv_anon_mutex);
    prev = serv->prev;
    next = serv->next;
    if (prev == NULL)
    {
        massert(serv_anon_list == serv);
        serv_anon_list = next;
    }
    else /* if (prev != NULL) */
    {
        prev->next = next;
    }
    if (next != NULL)
    {
        next->prev = prev;
    }
    pthread_mutex_unlock(&serv_anon_mutex);

    free(serv);
    return;
}

static void NONNULL
serv_destroy_name(serv_name_t * serv)
{
    size_t index;
    size_t min;
    size_t max;
    size_t mid;
    int    cmp;

    pthread_mutex_lock(&serv_name_mutex);
    index = serv_name_list_len;
    min = 0; /* included */
    max = serv_name_list_len; /* excluded */
    while (min < max)
    {
        massert(min <= max);
        mid = (min + max) / 2;
        cmp = strcmp(serv->name, serv_name_list[mid]->name);
        if (cmp == 0)
        {
            massert(serv == serv_name_list[mid]);
            index = mid;
        }
        else if (cmp > 0)
        {
            min = mid + 1; /* mid excluded, mid + 1 included */
        }
        else /* if (cmp < 0) */
        {
            max = mid; /* mid excluded */
        }
    }
    if (index >= serv_name_list_len)
    {
        log_error("(bug) could not find server %p with name %s",
                  (void *)serv, serv->name);
    }
    else
    {
        memmove(serv_name_list + index,
                serv_name_list + index + 1,
                (serv_name_list_len - index) * sizeof(serv_name_t *));
        serv_name_list_len--;
    }
    pthread_mutex_unlock(&serv_name_mutex);

    mstr_unref(serv->name);
    free(serv);
    return;
}

void NONNULL
serv_destroy(serv_t * serv)
{
    if (serv->name == NULL)
    {
        serv_destroy_anon((serv_anon_t *)serv);
    }
    else /* if (serv->name != NULL) */
    {
        serv_destroy_name((serv_name_t *)serv);
    }
    return;
}

void * PURE NONNULL
serv_data(serv_t * serv)
{
    if (serv->name == NULL)
    {
        return ((serv_anon_t *)serv)->data;
    }
    else /* if (serv->name != NULL) */
    {
        return ((serv_name_t *)serv)->data;
    }
}
