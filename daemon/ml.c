/*
 * This file is part of mradio.
 *
 * Copyright (C) 2014-2016 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

/* It is possible to compile this file without the signal handling code.
 * Just compile with -DML_NO_SIGNAL=1. */
/* #undef ML_NO_SIGNAL */

/* This must be set before any #includes. */
#define MRADIO_LOG_DOMAIN MRADIO_LOG_DOMAIN_ML

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <errno.h>
#include <poll.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#include <mradio/common.h>
#include <mradio/log.h>

#include <common/log-debug.h>
#include <common/massert.h>

#include "ml.h"

#if !ML_NO_SIGNAL
# include <signal.h>
# include <unistd.h>
# include <common/call.h>
# include "main.h"
#endif /* !ML_NO_SIGNAL */

struct ml_fd_s {
    void * data;
    ml_cb_t * error_cb;
    ml_cb_t * hangup_cb;
    ml_cb_t * read_cb;
    ml_cb_t * write_cb;
};
typedef struct ml_fd_s ml_fd_t;

struct ml_timeout_s {
    time_t       time;
    ml_cb_t *    cb;
    void *       data;
};
typedef struct ml_timeout_s ml_timeout_t;

/* This is the array of ml_fds. There might be unused ml_fds that have
 * an index < ml_fds_len. One property that must hold, however, is that
 * the ml_fd = ml_fds_len - 1 is always a used ml_fd. */
static ml_fd_t * ml_fds = NULL;
static int ml_fds_len = 0;
static int ml_fds_size = 0;

static struct pollfd * ml_pfds = NULL;
static int ml_pfds_len = 0;
static int ml_pfds_size = 0;

/* This is a heap based priority queue where the lowest ml_fd has the
 * highest priority. The two 1:s are here so that the array gets the
 * size 2 the first time ml_fd_queue_extend is called.
 *
 * To get an overview how the heap works here is a simple example, where
 * the elements denote the index in the array.
 *            1
 *       /          \
 *      2            3
      /   \        /   \
 *   4     5      6     7
 *  / \   / \    / \   / \
 * 8   9 10  11 12 13 14 15
 * It is clear if an element is appended to the end (index) of the
 * array, it is inserted as the right-most element in the bottom of the
 * tree. Another property is that the a parent node with index p has
 * a left child with index 2*p and a right child with index 2*p+1. */
static int * ml_fd_queue = NULL;
static int ml_fd_queue_len = 1;
static int ml_fd_queue_size = 1;

/* An array of timeouts. Used to convert integers to structures. Also it
 * is good to avoid memory fragmentation. There is no len variable since
 * free timeouts are kept in the ml_timeout_free array. */
static ml_timeout_t * ml_timeouts           = NULL;
static int            ml_timeouts_size      = 0;

/* A heap-based priority queue with the soonest expiry having the
 * highest priority. */
static int *          ml_timeout_queue      = NULL;
static int            ml_timeout_queue_len  = 1;
static int            ml_timeout_queue_size = 1;

/* Simply an array that lists all free timeouts. It is not sorted. */
static int *          ml_timeout_free       = NULL;
static int            ml_timeout_free_len   = 0;
static int            ml_timeout_free_size  = 0;

/* Variables used to tell main loop what to do. */
static int            ml_error;
static int            ml_quit;

/* A pipe that the signal handler will write triggered signals to and
 * which the main loop will read those triggered signals from. */
#if !ML_NO_SIGNAL
# define RD 0
# define WR 1
static int            ml_signal_pipe[2];
#endif /* !ML_NO_SIGNAL */

/* Extends the array pointed to by ptr. The size of the array shall be
 * saved at an int pointed to by sizep. The size will be updated on
 * success. The last argument, memsize, is the size of each array
 * member. */
static int NONNULL
ml_array_extend(void * ptr, int * sizep, size_t membsize)
{
    void ** arrayp = ptr;
    void *  new;

    ++*sizep;
    massert(*sizep > 0);
    new = realloc((void *)*arrayp, (unsigned)(*sizep) * membsize);
    if (new == NULL)
    {
        log_error_errno("could not allocate memory");
        --*sizep;
        return 1;
    }
    *arrayp = new;

    return 0;
}

#define ml_fds_extend() \
    ml_array_extend(&ml_fds, &ml_fds_size, sizeof(ml_fd_t))
#define ml_pfds_extend() \
    ml_array_extend(&ml_pfds, &ml_pfds_size, sizeof(struct pollfd))
#define ml_fd_queue_extend() \
    ml_array_extend(&ml_fd_queue, &ml_fd_queue_size, sizeof(int))
#define ml_timeouts_extend() \
    ml_array_extend(&ml_timeouts, &ml_timeouts_size, \
                    sizeof(ml_timeout_t))
#define ml_timeout_queue_extend() \
    ml_array_extend(&ml_timeout_queue, &ml_timeout_queue_size, \
                    sizeof(int))
#define ml_timeout_free_extend() \
    ml_array_extend(&ml_timeout_free, &ml_timeout_free_size, \
                    sizeof(int))

#if DEBUG
static inline void
ml_fd_queue_test_pair(const int parent, const int child)
{
    if (ml_fd_queue[parent] >= ml_fd_queue[child]) {
        log_fatal("ml_fd_queue_test failed parent=%d has ml_fd=%d, but "
                  "child=%d has ml_fd=%d",
                  parent, ml_fd_queue[parent],
                  child, ml_fd_queue[child]);
        abort();
    }
    return;
}

/* run-time self-test on the ml_fd queue */
static void
ml_fd_queue_test(void)
{
    /* p is the parent, l is the left child and r is the right child.
     * The heap is constructed so that l(p) = 2*p and r(p) = 2*p+1.
     * Of course p + 1 - p = 1 so p is stepped by one each iteration.
     * For l, l(p+1) - l(p) = 2*(p+1) - 2*p = 2, so l is stepped by two
     * each iteration. For r, r(p+1) - r(p) = 2*(p+1)+1 - (2*p+1) =
     * = 2*p + 2 + 1 - 2*p - 1 = 2, so r is also stepped by two each
     * iteration. */
    int p = 1;
    int l = 2;
    int r = 3;

    while (r < ml_fd_queue_len) {
        ml_fd_queue_test_pair(p, l);
        ml_fd_queue_test_pair(p, r);
        p++;
        l += 2;
        r += 2;
    }

    /* r(p) = 2*p + 1 >= ml_fd_queue_len implies
     * l(p+1) = 2*(p+1) = 2*p + 2 = r(p) + 1 >= ml_fd_queue_len,
     * thus only l(p) must be checked here; no loop needed. */
    if (l < ml_fd_queue_len) {
        ml_fd_queue_test_pair(p, l);
    }

    return;
}
#endif /* DEBUG */

static int
ml_fd_queue_enqueue(const int ml_fd)
{
    massert(ml_fds_len == ml_pfds_len);
    massert(ml_fd >= 0);
    massert(ml_fd < ml_fds_len);
    massert(ml_fd_get_fd(ml_fd) < 0);

    /* This is employed here so that the length of the ml_fds array is
     * reduced whenever it can be (or else the main loop will not stop).
     * Hopefully it is cheaper to employ this here rather than in the
     * actual main loop, since this code is only executed when an ml_fd
     * is destroyed.
     *
     * If this ml_fd is the last one the length can be reduced, but if
     * it is not ml_fds_len - 1 is a used ml_fd and the length can thus
     * not be reduced. Hence, a check whether this ml_fd is the last one
     * is sufficient.
     *
     * The ml_fd should in this case not be added to the priority queue
     * since it will be pulled eventually when the priority queue is
     * empty. */
    if (ml_fd + 1 == ml_fds_len) {
        /* Find the first unused ml_fd in the end of the array. */
        int first;
        for (first = ml_fd;
             first > 0 && ml_fd_get_fd(first - 1) < 0;
             first--)
        {
            /* empty */
        }
        ml_fds_len = first;
        ml_pfds_len = first;
        log_debug("ml_fds_len reduced to %d", first);
        return 0;
    }

    /* If the code reaches this point the ml_fd that was destroyed is
     * in the ml_fds array (in between used ml_fds). Thus, the ml_fd
     * must be added to the priority queue so that it can be reused
     * later on. */
    if (ml_fd_queue_len == ml_fd_queue_size &&
        ml_fd_queue_extend())
    {
        return 1;
    }
    massert(ml_fd_queue_size > ml_fd_queue_len);

    /* Insert ml_fd at the end of the heap and swap it up the heap. */
    int this = ml_fd_queue_len;
    int next = ml_fd_queue_len >> 1; /* ml_fd_len / 2 */
    while (next > 0 && ml_fd_queue[next] > ml_fd) {
        ml_fd_queue[this] = ml_fd_queue[next];
        this = next;
        next >>= 1; /* next /= 2; */
    }
    ml_fd_queue[this] = ml_fd;
    ml_fd_queue_len++;

#if DEBUG
    ml_fd_queue_test();
#endif /* DEBUG */

    return 0;
}

static int
ml_fd_queue_dequeue(void)
{
    massert(ml_fds_len == ml_pfds_len);
    massert(ml_fds_len <= ml_fds_size);
    massert(ml_pfds_len <= ml_pfds_size);

    int ml_fd;

    /* If there is an ml_fd in the queue it must be pulled first. */
    if (ml_fd_queue_len > 1) {
        /* (1) Pull out the highest priority element at the top of the
         *     heap.
         * (2) Save the last element in the heap array (it should be
         *     re-added somewhere in the heap).
         * (*) Note that there is a gap where either the last element
         *     should be inserted or on of the children should be moved
         *     up.
         * (3) Move children of the top node upwards until the gap
         *     should contain the last element (that is, above the gap
         *     the ml_fd is less, but below the gap ml_fd is more. */
        ml_fd = ml_fd_queue[1];

        /* Consider the following situation.
         * [initial state] 0 1  2  3, len=4
         * [destroy 2]     0 1 (2) 3, len=4 => 2 added to prio queue
         * [destroy 3]     0 1,       len=2
         * [destroy 1]     0,         len=1
         * Pulling the priority queue gives 2, which should not be used
         * at all. Instead 1 (ml_fds_len) should be used. If the element
         * pulled the queue is >= ml_fds_len then all elements in the
         * priority queue are >= ml_fds_len, which means the queue can
         * be cleared since there are no in-between elements. */
        if (ml_fd >= ml_fds_len) {
            ml_fd_queue_len = 1; /* clear priority queue */
            massert(ml_fds_len < ml_fds_size);
            massert(ml_pfds_len < ml_pfds_size);
            ml_fd = ml_fds_len;
            ml_fds_len++;
            ml_pfds_len++;
            return ml_fd;
        }

        const int last = ml_fd_queue[--ml_fd_queue_len];

        /* For a description look at the comment in ml_fd_queue_test. */
        int p = 1; /* this is the parent (the gap) */
        int l = 2;
        int r = 3;

        /* Run as long as there is a right child. */
        while (r < ml_fd_queue_len) {
            massert(ml_fd_queue[l] != ml_fd_queue[r]);

            int min; /* the minimum child, either l or r */
            if (ml_fd_queue[l] < ml_fd_queue[r]) {
                min = l;
            } else {
                min = r;
            }

            /* The gap should be filled with either the minimum child
             * (at min) or the last element. For the heap property to
             * hold the lowest one must be picked. If the minimum is
             * picked the new gap (parent) is where min was. */
            massert(ml_fd_queue[min] != last);
            if (ml_fd_queue[min] < last) {
                ml_fd_queue[p] = ml_fd_queue[min];
                p = min;
                l = p << 1; /* p*2 */
                r = l + 1;
            } else {
                /* This trick breaks this loop and disables the
                 * following if-clause. */
                r = ml_fd_queue_len;
                l = ml_fd_queue_len;
            }
        }

        /* It is known there is not a right child, which means
         * r >= ml_fd_queue_len. Now there might be a left child, but
         * the left child does not have any other children since if the
         * left child would have children l*2 < ml_fd_queue_len but
         * l*2 >= r >= ml_fd_queue_len, which is a contradiction. Thus
         * no loop is needed here. */
        if (l < ml_fd_queue_len) {
            massert(ml_fd_queue[l] != last);
            if (ml_fd_queue[l] < last) {
                ml_fd_queue[p] = ml_fd_queue[l];

                /* Since l = 2*p, the assignment p = l is the same as
                 * p <<= 1. */
                p <<= 1;
            }
        }

        massert(p <= ml_fd_queue_len);
        ml_fd_queue[p] = last;

#if DEBUG
        ml_fd_queue_test();
#endif /* DEBUG */
    } else {
        if (ml_fds_len == ml_fds_size && ml_fds_extend()) {
            return -1;
        }
        if (ml_pfds_len == ml_pfds_size && ml_pfds_extend()) {
            return -1;
        }
        massert(ml_fds_len == ml_pfds_len);
        massert(ml_fds_len < ml_fds_size);
        massert(ml_pfds_len < ml_pfds_size);

        ml_fd = ml_fds_len;
        ml_fds_len++;
        ml_pfds_len++;
    }

    return ml_fd;
}

int
ml_fd_new(const int fd, void * const data)
{
    const int ml_fd = ml_fd_queue_dequeue();
    if (ml_fd < 0) {
        return ml_fd;
    }

    ml_fd_t * const ml_fd_priv = ml_fds + ml_fd;
    struct pollfd * const pfd = ml_pfds + ml_fd;

    pfd->fd = fd;
    pfd->events = 0;
    pfd->revents = 0;

    ml_fd_priv->data = data;
    ml_fd_priv->error_cb = NULL;
    ml_fd_priv->hangup_cb = NULL;
    ml_fd_priv->read_cb = NULL;
    ml_fd_priv->write_cb = NULL;

    log_debug("created ml_fd %d for fd %d", ml_fd, fd);
    log_debug("length and size of ml_fds array is now %d and %d "
              "respectively",
              ml_fds_len, ml_pfds_len);

    massert(ml_fd < ml_fds_len);
    return ml_fd;
}

int NONNULL
ml_fd_destroy(int ml_fd)
{
    struct pollfd * pfd;

    log_debug("destroying ml_fd %d", ml_fd);

    massert(ml_fd >= 0);
    massert(ml_fd < ml_fds_len);
    pfd = ml_pfds + ml_fd;
    pfd->fd = -1;

    return ml_fd_queue_enqueue(ml_fd);
}

void * PURE
ml_fd_get_data(const int ml_fd)
{
    ml_fd_t * const ml_fd_priv = ml_fds + ml_fd;
    return ml_fd_priv->data;
}

int PURE
ml_fd_get_fd(const int ml_fd)
{
    struct pollfd * const pfd = ml_pfds + ml_fd;
    return pfd->fd;
}

void
ml_fd_error_cb(const int ml_fd, ml_cb_t * const cb)
{
    massert(ml_fd >= 0);
    massert(ml_fd < ml_fds_len);

    ml_fd_t * const ml_fd_priv = ml_fds + ml_fd;
    ml_fd_priv->error_cb = cb;

    return;
}

void
ml_fd_hangup_cb(const int ml_fd, ml_cb_t * const cb)
{
    massert(ml_fd >= 0);
    massert(ml_fd < ml_fds_len);

    ml_fd_t * const ml_fd_priv = ml_fds + ml_fd;
    ml_fd_priv->hangup_cb = cb;

    return;
}

void
ml_fd_read_cb(const int ml_fd, ml_cb_t * const cb)
{
    massert(ml_fd >= 0);
    massert(ml_fd < ml_fds_len);

    ml_fd_t * const ml_fd_priv = ml_fds + ml_fd;
    struct pollfd * const pfd = ml_pfds + ml_fd;

    ml_fd_priv->read_cb = cb;
    if (cb != NULL) {
        pfd->events |= POLLIN;
    } else {
        pfd->events &= ~POLLIN;
    }

    return;
}

void
ml_fd_write_cb(const int ml_fd, ml_cb_t * const cb)
{
    massert(ml_fd >= 0);
    massert(ml_fd < ml_fds_len);

    ml_fd_t * const ml_fd_priv = ml_fds + ml_fd;
    struct pollfd * const pfd = ml_pfds + ml_fd;

    ml_fd_priv->write_cb = cb;
    if (cb != NULL) {
        pfd->events |= POLLOUT;
    } else {
        pfd->events &= ~POLLOUT;
    }

    return;
}

static time_t
ml_timeout_queue_peek(void)
{
    if (ml_timeout_queue_len > 1) {
        const int timeout = ml_timeout_queue[1];
        return ml_timeouts[timeout].time;
    } else {
        return 0;
    }
}

static int
ml_timeout_queue_enqueue(const int timeout)
{
    massert(timeout >= 0);
    massert(timeout < ml_timeouts_size);

    ml_timeout_t * const ml_timeout = ml_timeouts + timeout;
    int this;
    int next;

    if (ml_timeout_queue_len == ml_timeout_queue_size) {
        if (ml_timeout_queue_extend()) {
            return 1;
        }
    }
    massert(ml_timeout_queue_len < ml_timeout_queue_size);

    /* Insert ml_fd at the end of the heap and swap it up the heap. */
    this = ml_timeout_queue_len;
    next = ml_timeout_queue_len >> 1; /* ml_queue_len / 2 */
    while (next > 0) {
        const int tmp = ml_timeout_queue[next];
        if (ml_timeout->time < ml_timeouts[tmp].time) {
            /* swap */
            ml_timeout_queue[this] = ml_timeout_queue[next];
            this = next;
            next >>= 1; /* next /= 2; */
        } else {
            break;
        }
    }
    ml_timeout_queue[this] = timeout;
    ml_timeout_queue_len++;

    return 0;
}

/* Returns the non-negative timeout on success or -1 on failure. */
static int
ml_timeout_queue_dequeue(void)
{
    int timeout = -1;

    /* If there is at least one timeout in the queue, pull it from the
     * queue. */
    if (ml_timeout_queue_len > 1) {
        const int last = ml_timeout_queue[--ml_timeout_queue_len];
        int this = 1;
        int next = 2;

        timeout = ml_timeout_queue[1];

        while (next < ml_timeout_queue_len) {
            /* Set next to the child with the soonest expiry time. */
            if (next + this < ml_timeout_queue_len) {
                const int t1 = ml_timeout_queue[next + this];
                const int t2 = ml_timeout_queue[next];
                if (ml_timeouts[t1].time < ml_timeouts[t2].time) {
                    next += this;
                }
            }

            /* If the soonest expiry time of the children is smaller
             * than the expiry time of the last element (which should
             * be inserted here otherwise), next must be swapped up to
             * this place. */
            if (ml_timeouts[next].time < ml_timeouts[last].time) {
                ml_timeout_queue[this] = ml_timeout_queue[next];
                this = next;
                next <<= 1; /* next *= 2; */
            } else {
                break;
            }
        }
        ml_timeout_queue[this] = last;
    }

    return timeout;
}

static inline int
ml_timeout_expire(const int timeout, const unsigned int expiry)
{
    massert(expiry != 0);

    ml_timeout_t * const ml_timeout = ml_timeouts + timeout;
    ml_timeout->time = time(NULL);
    ml_timeout->time += expiry;

    return ml_timeout_queue_enqueue(timeout);
}

/* The timeout must not be in the queue when this function is called or
 * strange things will happen.
 * Returns 0 on success and 1 otherwise. */
static int
ml_timeout_destroy(const int timeout)
{
    if (ml_timeout_free_len == ml_timeout_free_size &&
        ml_timeout_free_extend())
    {
        return 1;
    }
    massert(ml_timeout_free_len < ml_timeout_free_size);

    ml_timeout_free[ml_timeout_free_len] = timeout;
    ml_timeout_free_len++;

    return 0;
}

int
ml_timeout_new(unsigned int expiry,
               ml_cb_t * const cb,
               void * const data)
{
    int            timeout;
    ml_timeout_t * ml_timeout;

    if (ml_timeout_free_len > 0) {
        ml_timeout_free_len--;
        timeout = ml_timeout_free[ml_timeout_free_len];
    } else {
        if (ml_timeouts_extend()) {
            return -1;
        }
        timeout = ml_timeouts_size - 1;
    }
    massert(timeout >= 0);

    ml_timeout = ml_timeouts + timeout;
    ml_timeout->cb   = cb;
    ml_timeout->data = data;

    if (ml_timeout_expire(timeout, expiry)) {
        ml_timeout_destroy(timeout);
        return -1;
    }

    return timeout;
}

void * PURE
ml_timeout_get_data(const int timeout)
{
    return ml_timeouts[timeout].data;
}

#define ML_SHOULD_LEAVE \
    if (ml_pfds[ml_fd].fd < 0) { \
        log_debug("leaving ml_fd %d (fd %d)", ml_fd, \
                  ml_pfds[ml_fd].fd); \
        goto leave; \
    }

static void
ml_handle_pfds(int ready)
{
    massert(ready > 0);
    massert(ml_pfds_len > 0);
    massert(ready <= ml_pfds_len);

    for (int ml_fd = 0; ml_fd < ml_fds_len && ready > 0; ml_fd++) {
        massert(ml_fds_len == ml_pfds_len);

        /* Note that it is not safe to save the pointer to the array
         * members and use them them to access the struct pfd and
         * ml_fd_t throughout this function since the array may be
         * reallocated by a callback. Hence, constructs such as
         * ml_pfds[ml_fd].xxx are necessary. */

        /* There might be "holes" in the ml_fds array. This is
         * intentional. Just ignore deleted ml_fds that are not used.
         * Also avoid unnecessary operations if no event is waiting on
         * the file descriptor.*/
        if (ml_pfds[ml_fd].fd < 0 || ml_pfds[ml_fd].revents == 0) {
            log_debug("skipping ml_fd %d (fd %d)", ml_fd,
                      ml_pfds[ml_fd].fd);
            continue;
        }

        if (ml_pfds[ml_fd].revents & POLLNVAL) {
            ml_pfds[ml_fd].revents &= ~POLLNVAL;
            log_error("invalid request: fd %d not open",
                      ml_pfds[ml_fd].fd);
        }

        if (ml_pfds[ml_fd].revents & POLLERR) {
            ml_pfds[ml_fd].revents &= ~POLLERR;
            if (ml_fds[ml_fd].error_cb != NULL) {
                log_debug("error on ml_fd %d (fd %d)", ml_fd,
                          ml_pfds[ml_fd].fd);
                (*ml_fds[ml_fd].error_cb)(ml_fd);
                ML_SHOULD_LEAVE;
            } else {
                log_warn("unhandled error on file descriptor %d",
                         ml_pfds[ml_fd].fd);
            }
        }

        if (ml_pfds[ml_fd].revents & POLLHUP) {
            ml_pfds[ml_fd].revents &= ~POLLHUP;
            if (ml_fds[ml_fd].hangup_cb != NULL) {
                log_debug("hangup on ml_fd %d (fd %d)", ml_fd,
                          ml_pfds[ml_fd].fd);
                (*ml_fds[ml_fd].hangup_cb)(ml_fd);
                ML_SHOULD_LEAVE;
            } else {
                log_warn("unhandled hangup on file descriptor %d",
                         ml_pfds[ml_fd].fd);
            }
        }

        if (ml_pfds[ml_fd].revents & POLLIN) {
            ml_pfds[ml_fd].revents &= ~POLLIN;
            massert(ml_fds[ml_fd].read_cb != NULL);
            log_debug("invoking read callback on ml_fd %d (fd %d)",
                      ml_fd, ml_pfds[ml_fd].fd);
            (*ml_fds[ml_fd].read_cb)(ml_fd);
            ML_SHOULD_LEAVE;
        }

        if (ml_pfds[ml_fd].revents & POLLOUT) {
            ml_pfds[ml_fd].revents &= ~POLLOUT;
            massert(ml_fds[ml_fd].write_cb != NULL);
            log_debug("invoking write callback on ml_fd %d (fd %d)",
                      ml_fd, ml_pfds[ml_fd].fd);
            (*ml_fds[ml_fd].write_cb)(ml_fd);
            ML_SHOULD_LEAVE;
        }

        massert(ml_pfds[ml_fd].revents == 0);
    leave:
        ready--;
    }

    return;
}

#if !ML_NO_SIGNAL
static unsigned
ml_signal_read_cb(int ml_fd)
{
    size_t  siz = sizeof(int);
    int     sig;
    char *  buf = (char *)&sig;
    ssize_t ret;

    while (siz > 0)
    {
        ret = read(ml_signal_pipe[RD], buf, siz);
        if (ret < 0)
        {
            if (errno != EINTR)
            {
                log_error_errno("read from signal pipe failed");
                ml_error = 1;
                ml_quit  = 1;
                return 0;
            }
        }
        buf += ret;
        siz -= (size_t)ret;
    }

    if (sig == SIGCHLD)
    {
        if (wait_child())
        {
            ml_quit  = 1;
            ml_error = 1;
        }
    }
    else
    {
        ml_quit = 1;
    }

    return 0;
}

static void
ml_signal_handler(int sig)
{
    size_t  siz = sizeof(int);
    char *  buf = (char *)&sig;
    ssize_t ret;
    pid_t   pid;

    while (siz > 0)
    {
        ret = write(ml_signal_pipe[WR], buf, siz);
        if (ret < 0)
        {
            if (errno != EINTR)
            {
                /* There is no way to handle this error, other than
                 * stopping right here. However abort() may not be
                 * called, so send ourselves the KILL signal. */
                pid = getpid();
                kill(pid, SIGKILL);
                return;
            }
        }
        buf += ret;
        siz -= (size_t)ret;
    }

    return;
}
#endif /* !ML_NO_SIGNAL */

int
ml_run(void)
{
    int              retval = 1;

#if !ML_NO_SIGNAL
    struct sigaction act;
    int              sig_ml_fd = -1;
#endif /* !ML_NO_SIGNAL */

    time_t           now;
    time_t           next;
    int              t = -1;
    int              ready;

#if !ML_NO_SIGNAL
    if (pipe(ml_signal_pipe))
    {
        log_fatal_errno("could not create pipe");
        ml_signal_pipe[RD] = -1;
        ml_signal_pipe[WR] = -1;
        goto error;
    }

    sig_ml_fd = ml_fd_new(ml_signal_pipe[RD], NULL);
    if (sig_ml_fd < 0)
    {
        goto error;
    }
    ml_fd_read_cb(sig_ml_fd, &ml_signal_read_cb);

    CALL(sigemptyset, &act.sa_mask);
    act.sa_flags = 0;

    act.sa_handler = &ml_signal_handler;
    CALL(sigaction, SIGINT, &act, NULL);
    CALL(sigaction, SIGTERM, &act, NULL);
    CALL(sigaction, SIGCHLD, &act, NULL);

    act.sa_handler = SIG_IGN;
    CALL(sigaction, SIGPIPE, &act, NULL);
#endif /* !ML_NO_SIGNAL */

    ml_error = 0;
    ml_quit  = 0;
    while (!ml_quit && ml_pfds_len > 0)
    {
        /* trigger any timeouts that have expired */
        for (;;)
        {
            int            timeout;
            ml_timeout_t * ml_timeout;
            unsigned int   ret;

            now = time(NULL);
            next = ml_timeout_queue_peek();
            if (next == 0 || now < next)
            {
                break;
            }

            timeout = ml_timeout_queue_dequeue();
            ml_timeout = ml_timeouts + timeout;

            ret = (*ml_timeout->cb)(timeout);
            if (ret > 0)
            {
                /* The timeout should be re-added. */
                if (ml_timeout_expire(timeout, ret)) {
                    ml_timeout_destroy(timeout);
                }
            }
            else
            {
                /* The timeout should be destroyed. */
                ml_timeout_destroy(timeout);
            }
        }

        if (next > 0) {
            t = (int)(next - now) * 1000;
        }
        log_debug("calling poll()");
        ready = poll(ml_pfds, (nfds_t)ml_pfds_len, t);
        log_debug("poll() returned %d", ready);

        if (ready < 0)
        {
            if (errno == EINTR)
            {
                log_debug("poll() was interrupted");
            }
            else
            {
                log_fatal_errno("poll() failed");
                goto error;
            }
        }
        else if (ready > 0)
        {
            ml_handle_pfds(ready);
        }
    }

    if (!ml_error)
    {
        retval = 0;
    }
error:
#if !ML_NO_SIGNAL
    if (sig_ml_fd >= 0)
    {
        ml_fd_destroy(sig_ml_fd);
    }
    for (unsigned i = 0; i < 2; ++i)
    {
        if (ml_signal_pipe[i] >= 0 && close(ml_signal_pipe[i]))
        {
            log_error_errno("could not close pipe end");
        }
    }
#endif /* !ML_NO_SIGNAL */
    return retval;
}

void
ml_free(void)
{
    free(ml_fds);
    ml_fds                = NULL;
    ml_fds_len            = 0;
    ml_fds_size           = 0;

    free(ml_pfds);
    ml_pfds               = NULL;
    ml_pfds_len           = 0;
    ml_pfds_size          = 0;

    free(ml_fd_queue);
    ml_fd_queue           = NULL;
    ml_fd_queue_len       = 1;
    ml_fd_queue_size      = 1;

    free(ml_timeouts);
    ml_timeouts           = NULL;
    ml_timeouts_size      = 1;

    free(ml_timeout_queue);
    ml_timeout_queue      = NULL;
    ml_timeout_queue_len  = 0;
    ml_timeout_queue_size = 0;

    free(ml_timeout_free);
    ml_timeout_free       = NULL;
    ml_timeout_free_len   = 0;
    ml_timeout_free_size  = 0;

    return;
}
