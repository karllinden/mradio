/*
 * This file is part of mradio.
 *
 * Copyright (C) 2017 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#define MRADIO_LOG_DOMAIN MRADIO_LOG_DOMAIN_DAEMON /* FIXME */

# if HAVE_CONFIG_H
#  include <config.h>
# endif /* HAVE_CONFIG_H */

#include <stdlib.h>

#include <mradio/mstr.h>
#include <mradio/player.h>

#include "player.h"

/*
 * Currently only a single player is supported, but it should not be
 * hard to implement support for multiple players.
 * The hardest part in doing that will be the user interface.
 */
static player_t player;

void
player_init(void)
{
    atomic_store(&player.url, NULL);
    return;
}

void
player_deinit(void)
{
    char * url;
    url = (char *)atomic_exchange(&player.url, NULL);
    mstr_unref(url);
    return;
}

char *
player_url(const player_t * p)
{
    char * url;
    url = (char *)atomic_load(&p->url);
    mstr_ref(url);
    return url;
}

int NONNULL
player_play(char * url) /* mstr */
{
    /* FIXME */
    return 0;
}

int
player_stop(void)
{
    /* FIXME */
    return 0;
}
