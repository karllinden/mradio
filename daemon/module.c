/*
 * This file is part of mradio.
 *
 * Copyright (C) 2015-2017 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

/* FIXME: thread safety */

#define MRADIO_LOG_DOMAIN MRADIO_LOG_DOMAIN_DAEMON

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <string.h>

#if HAVE_LTDL
# include <dirent.h>
# include <stdlib.h>
# include <sys/types.h>
# include <ltdl.h>
#endif /* HAVE_LTDL */

#include <mradio/common.h>
#include <mradio/log.h>
#include <mradio/mstr.h>

#include <common/massert.h>

#include "avl.h"
#include "builtin.h"
#include "module.h"

#define module_ltdl_error(msg) \
    do { \
        const char * errstr = lt_dlerror(); \
        log_error("%s%s%s", msg, \
                  errstr != NULL ? ": " : "", \
                  errstr != NULL ? errstr : ""); \
    } while (0);

static avl_t modules;

static int NONNULL
module_cmp_func(const void * key,
                const void * data,
                const void * extra)
{
    const char *     name = key;
    const module_t * module = data;
    return strcmp(name, module->name);
}

static int NONNULLA(1)
module_unload_real(void * data, void * extra)
{
    module_t * module = data;
    log_info("unloading module %s", module->name);
    mstr_unref(module->name);

    if (module->deinit)
    {
        (*module->deinit)();
    }

#if HAVE_LTDL
    if (module->handle != NULL) {
        if (lt_dlclose(module->handle)) {
            module_ltdl_error("could not unload module");
        }
    }
#endif /* HAVE_LTDL */

    return 0;
}

int
module_init(void)
{
    avl_init(&modules, &module_cmp_func, &module_unload_real, NULL);

#if HAVE_LTDL
    if (lt_dlinit()) {
        module_ltdl_error("could not initialize libltdl");
        return 1;
    }

    if (lt_dlsetsearchpath(MRADIO_MODULE_PATH)) {
        module_ltdl_error("could not set search path");
        module_deinit();
        return 1;
    }
#endif /* HAVE_LTDL */

    /* FIXME: bind commands */

    return 0;
}

int
module_deinit(void)
{
    avl_deinit(&modules);

#if HAVE_LTDL
    if (lt_dlexit()) {
        module_ltdl_error("could not exit libltdl");
        return 1;
    }
#endif /* HAVE_LTDL */

    return 0;
}

/* name is a referenced mstr
 * module is the to be initialized module structure */
static int
module_load_func_real(char * name, module_t * module)
{
    module_init_fn_t * init;
    int                ret;

#if HAVE_LTDL
    size_t                namelen;
    char *                sym;
#endif /* HAVE_LTDL */

    log_info("loading module %s", name);

    /* Initialize the structure to something that module_unload_real can
     * handle. */
    module->name    = name;
    module->count   = 0;
    module->handles = NULL;
    module->info    = NULL;
    module->deinit  = NULL;
# if HAVE_LTDL
    module->handle  = NULL;
# endif /* HAVE_LTDL */

    module->info = builtin_load(name);
    if (module->info != NULL)
    {
        log_info("loading builtin %s", name);
    }
    else
    {
#if HAVE_LTDL
        namelen = strlen(name);

        log_info("loading dynamic %s", name);

        sym = malloc(namelen + 6);
        if (sym == NULL)
        {
            log_error_errno("could not allocate memory");
            goto error;
        }

        module->handle = lt_dlopenext(name);
        if (module->handle == NULL)
        {
            module_ltdl_error("could not load module");
            free(sym);
            goto error;
        }

        memcpy(sym, name, namelen);
        memcpy(sym + namelen, "_info", 6);
        module->info = lt_dlsym(module->handle, sym);
        free(sym);
        if (module->info == NULL) {
            module_ltdl_error("could not load info structure");
            goto error;
        }
#else /* !HAVE_LTDL */
        log_warn("could not load module %s: dynamic module support "
                 "disabled", name);
        goto error;
#endif /* !HAVE_LTDL */
    }

    init = module->info->init;
    if (init != NULL)
    {
        ret = (*init)();
        if (ret)
        {
            log_error("module initialization failed");
            goto error;
        }
    }
    module->deinit = module->info->deinit;

    return 0;
error:
    module_unload_real(module, NULL);
    return 1;
}

static int
module_load_func(void * key, void * data, void * extra)
{
    const char * name = key;
    char *       copy = mstr(name);
    if (copy == NULL)
    {
        log_error_errno("could not allocate memory");
        return 1;
    }
    return module_load_func_real(copy, data);
}

static int
module_load_func_mstr(void * key, void * data, void * extra)
{
    char * name = key;
    mstr_ref(name);
    return module_load_func_real(name, data);
}

static module_t * NONNULL
module_load_real(const void * name, avl_init_func_t * func)
{
    module_t * module;
    module = avl_insearch(&modules, name, sizeof(module_t), func, NULL);
    if (module == NULL)
    {
        return NULL;
    }
    return module;
}

int NONNULL
module_load_mstr(char * name)
{
    module_t * module;
    module = module_load_real(name, &module_load_func_mstr);
    if (!module)
    {
        return 1;
    }
    module->count++;
    return 0;
}

static void NONNULL
module_unload_common(module_t * module)
{
    if (!module->count && !module->handles)
    {
        avl_delete_data(&modules, module);
    }
    return;
}

void NONNULL
module_unload(const char * name)
{
    module_t * module;

    module = avl_search(&modules, name);
    if (!module)
    {
        log_warn("could not unload %s: module not found", name);
    }
    else if (module->count)
    {
        module->count--;
        module_unload_common(module);
    }

    return;
}

static int NONNULL
module_handle_load_real(const void * name,
                        avl_init_func_t * func,
                        module_handle_t * handle)
{
    module_t * module;
    massert(!module_handle_loaded(handle));
    module = module_load_real(name, func);
    if (!module)
    {
        return 1;
    }
    handle->module  = module;
    handle->prev    = NULL;
    handle->next    = module->handles;
    module->handles = handle;
    return 0;
}

int NONNULL
module_handle_load(const char * name, module_handle_t * handle)
{
    return module_handle_load_real(name, &module_load_func, handle);
}

int NONNULL
module_handle_load_mstr(char * name, module_handle_t * handle)
{
    return module_handle_load_real(name,
                                   &module_load_func_mstr,
                                   handle);
}

void NONNULL
module_handle_unload(module_handle_t * handle)
{
    massert(module_handle_loaded(handle));
    if (handle->prev)
    {
        massert(handle->prev->next == handle);
        handle->prev->next = handle->next;
    }
    else
    {
        massert(handle->module->handles == handle);
        handle->module->handles = handle->next;
    }
    if (handle->next)
    {
        massert(handle->next->prev == handle);
        handle->next->prev = handle->prev;
    }
    module_unload_common(handle->module);

    handle->module = NULL;
    handle->prev   = NULL;
    handle->next   = NULL;

    return;
}
