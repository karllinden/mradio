/*
 * This file is part of mradio.
 *
 * Copyright (C) 2014-2016 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#ifndef SERV_H
# define SERV_H 1

# if HAVE_CONFIG_H
#  include <config.h>
# endif /* HAVE_CONFIG_H */

# include <mradio/common.h>
# include <mradio/info.h>

enum serv_type_e
{
    SERV_PIPE,
    SERV_SOCK
};
typedef enum serv_type_e serv_type_t;
typedef struct serv_s serv_t;

int      serv_init     (void);
void     serv_deinit   (void);

/* Should be called from the daemon. */
serv_t * serv_start    (const info_t * info) NONNULL;
void     serv_stop     (serv_t * serv)       NONNULL;
void     serv_stop_name(const char * name)   NONNULL;
void     serv_stop_all (void)                NONNULL;

/* Should be called from server imlementations. */
serv_t * serv_new      (char * name, /* mstr */
                        serv_type_t type,
                        size_t data_size);
void     serv_destroy  (serv_t * serv)       NONNULL;
void *   serv_data     (serv_t * serv)       PURE NONNULL;

#endif /* !SERV_H */
