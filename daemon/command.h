/*
 * This file is part of mradio.
 *
 * Copyright (C) 2016-2017 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#ifndef COMMAND_H
# define COMMAND_H 1

# if HAVE_CONFIG_H
#  include <config.h>
# endif /* HAVE_CONFIG_H */

# include <stdint.h>

# include <mradio/common.h>
# include <mradio/return.h>

/* argv is an argument vector where the strings are mstrs */
typedef int32_t (command_func_t)(uint8_t argc,
                                 char ** argv,
                                 return_t ** retp);

/* The subcommands array must be sorted. */
struct command_s
{
    const char *             name;
    const char *             help;
    command_func_t *         func;
    const struct command_s * subcommands;
    size_t                   size;
};
typedef struct command_s command_t;

void    command_init          (void);
void    command_deinit        (void);

int     command_register      (const command_t * command) NONNULL;
int     command_register_array(const command_t * array,
                               unsigned size) NONNULL;

/* First argument, argv[0], is the command name. argv must be an array
 * of mstr. */
int32_t command_call          (uint8_t argc,
                               char ** argv,
                               return_t ** retp) NONNULL;

#endif /* !COMMAND_H */
