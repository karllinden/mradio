/*
 * This file is part of mradio.
 *
 * Copyright (C) 2014-2016 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#define MRADIO_LOG_DOMAIN MRADIO_LOG_DOMAIN_PID

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>

#include <mradio/common.h>
#include <mradio/create-parent.h>
#include <mradio/log.h>
#include <mradio/mstr.h>

#include "pidfile.h"

/* FIXME: thread safety */

_Static_assert(sizeof(pid_t) <= sizeof(long), "bad size");

static char * pidfile = NULL;

int NONNULL
pidfile_create(char * filename)
{
    int    retval  = 1;
    FILE * file    = NULL;
    pid_t  pid;
    int    ret;

    pidfile_unlink();

    if (create_parent(filename))
    {
        goto error;
    }
    file = fopen(filename, "wx");
    if (file == NULL)
    {
        log_error_errno("could not create pidfile %s", filename);
        goto error;
    }

    pid = getpid();
    ret = fprintf(file, "%ld\n", (long)pid);
    if (ret < 0) {
        log_error("could not write PID to pidfile %s", filename);
        goto error;
    }

    pidfile = filename;
    mstr_ref(filename);

    /* success */
    retval = 0;

error:
    if (file != NULL && fclose(file)) {
        log_error_errno("could not close pidfile %s", filename);
    }

    return retval;
}

void NONNULL
pidfile_unlink(void)
{
    if (pidfile != NULL)
    {
        if (unlink(pidfile) && errno != ENOENT)
        {
            log_error_errno("could not unlink pidfile %s", pidfile);
        }
        mstr_unref(pidfile);
        pidfile = NULL;
    }
    return;
}
