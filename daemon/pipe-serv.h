/*
 * This file is part of mradio.
 *
 * Copyright (C) 2016 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#ifndef PIPE_SERV_H
# define PIPE_SERV_H 1

# if HAVE_CONFIG_H
#  include <config.h>
# endif /* HAVE_CONFIG_H */

# include <stddef.h>

# include "serv.h"

/* Mask for externally settable flags. Other flags are to be used by the
 * pipe server code. */
# define PIPE_SERV_FLAGS              0x00FF

/* Should the pipe server be closed if authentication fails? */
# define PIPE_SERV_STOP_ON_AUTH_FAIL  0x0001

/* Should the pipe server stop if the client is deprecated? If this is
 * not set the state will be reset when a deprecated client connects. */
# define PIPE_SERV_STOP_ON_DEPRECATED 0x0002

/* Should the pipe server stop if the client hangs up? */
# define PIPE_SERV_STOP_ON_HUP        0x0004

/* Should the pipe server stop whenever something funny happens. */
# define PIPE_SERV_STOP_ALL           0x0007

/* Has the server received a PROTO_BEGIN? */
# define PIPE_SERV_BEGUN              0x0100

/* Has the client version been checked against the server version? */
# define PIPE_SERV_VERSION_IS_CHECKED 0x0200

/* Has the connection been authenticated? */
# define PIPE_SERV_AUTH               0x0400

/* The server should stop as soon as everything has been written. */
# define PIPE_SERV_STOP_AFTER_FLUSH   0x0800

/* The pipe server works as follows:
 *
 * A caller invokes pipe_serv_start giving it a read end and a write end
 * of a pipe like connection.
 * The caller can supply a data_size which will be allocated inside the
 * pipe server structure.
 * The allocated and uninitialized data can then be accessed by
 * pipe_serv_data().
 * The caller should also supply a stop function which will be called
 * when the pipe server stops.
 * There is a flags argument where the caller can tweak how the pipe
 * server behaves.
 *
 * After initialization the server waits for a client to send
 * PROTO_BEGIN (uint8_t). When that it received the server writes 32
 * bits of its protocol version and waits for 32 bits of protocol
 * version from the client when those are fetched the server responds
 * with one of
 *  1. PROTO_DEPRECATED if the clients protocol version is too old.
 *  2. PROTO_PASSWORD if the client needs to authenticate using a
 *     password.
 *  3. PROTO_AUTH if regular communication may begin.
 * In case of 2 the server waits for the client to send a string
 * containing the password. When the string is received it is validated
 * and if the password is correct the server sends PROTO_AUTH to the
 * client and communication may begin. If the authentication failed then
 * PROTO_PASSWORD is resent and the process is repeated unless
 * PIPE_SERV_STOP_ON_AUTH_FAIL is set in which case the server stops.
 *
 * When authenticated the server sends PROTO_AUTH to the client to
 * message that communication can proceed as usual.
 *
 * When the client wants to stop the communication it sends PROTO_END
 * and hangs up. */

/* Since pipe servers can ba applicable in a variety of places each
 * different server can be equipped with a stop function which will be
 * called upon (pipe_)serv_stop before destroying the pipe server.
 * The given function may not destroy the pipe server.
 * Note that the pipe server code close()s neither rdfd nor wrfd so if
 * this shall be done it must be done by the given stop function. */
typedef void (pipe_serv_stop_func_t)(serv_t * pipeserv);

serv_t * pipe_serv_start(char * name, /* mstr */
                         char * password, /* mstr */
                         int rdfd,
                         int wrfd,
                         size_t data_size,
                         pipe_serv_stop_func_t * stop_func,
                         int flags);
void     pipe_serv_stop (serv_t * serv) NONNULL;
void *   pipe_serv_data (serv_t * serv) NONNULL PURE;
int      pipe_serv_rdfd (serv_t * serv) NONNULL PURE;
int      pipe_serv_wrfd (serv_t * serv) NONNULL PURE;

#endif /* !PIPE_SERV_H */
