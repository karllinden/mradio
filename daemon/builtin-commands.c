/*
 * This file is part of mradio.
 *
 * Copyright (C) 2014-2017 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

/* FIXME: write a help program called "?" */

#define MRADIO_LOG_DOMAIN MRADIO_LOG_DOMAIN_DAEMON

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <errno.h>
#include <limits.h>
#include <stdint.h>
#include <string.h>

#include <gop.h>

#include <mradio/common.h>
#include <mradio/log.h>
#include <mradio/mstr.h>
#include <mradio/return.h>

#include "builtin-commands.h"
#include "bookmark.h"
#include "command.h"
#include "fetcher.h"
#include "module.h"
#include "pidfile.h"
#include "player.h"

typedef int32_t (yesno_func_t)(return_t ** retp);

static const char help_fetcher[] =
"Usage: fetcher FETCHER\n"
"\n"
"Select which fetcher should be used. Only one fetcher can be used\n"
"and a restart is necessary to select another fetcher.";

static const char help_load[] =
"Usage: load MODULE [MODULE...]\n"
"\n"
"Load module.\n";

static const char help_logdomain[] =
"Usage: logdomain [yes|no]\n"
"\n"
"If no argument is given or argument is yes, the log domain will be\n"
"printed along with log messages. If argument is no log domain will\n"
"be omitted.\n";

static const char help_logfile[] =
"Usage: logfile [FILENAME]\n"
"\n"
"If FILENAME is given, sends log messages to that file. Otherwise\n"
"log messages will not be sent to any log file.\n";

static const char help_loglevel[] =
"Usage: loglevel [[DOM]:LEV]...\n"
"\n"
"For each argument sets the log level of the domain DOM to LEV,\n"
"which must be one of \"fatal\", \"error\", \"warn\", \"info\" and \n"
"\"debug\". The log levels of all domains are set to LEV\n";

static const char help_pause[] = "FIXME";

static const char help_pidfile[] =
"Usage: pidfile [FILENAME]\n"
"\n"
"If FILENAME is given, create a pidfile with that name. Otherwise\n"
"any current pidfile is unlinked.\n";

static const char help_play[] = "FIXME";

static const char help_resume[] = "FIXME";

static const char help_stop[] = "FIXME";

static const char help_syslog[] =
"Usage: syslog [yes|no]\n"
"\n"
"If no argument is given or argument is yes, log messages will be\n"
"sent to syslog as well. If argument is no, messages will not be sent\n"
"to syslog.\n";

static const char help_toggle[] = "FIXME";

static const char help_unload[] =
"Usage: unload MODULE [MODULE...]\n"
"\n"
"Unload module.\n";

/* FIXME: document the subcommands somehow and make this list dynamic */
static const char help_bookmark[] =
"Usage: bookmark [subcommand] [ARGUMENTS...]\n"
"\n"
"Available subcommands are:\n"
"    add\n"
"    del\n"
"    file\n"
"    load\n"
"    save\n"
"    save-on-change\n"
"    save-on-exit\n"
"    save-periodic\n"
"    url\n";

static const char help_bookmark_add[] =
"Usage: bookmark add PATH URL\n"
"\n"
"Adds the station at URL and saves it to PATH\n";

static const char help_bookmark_del[] =
"Usage: bookmark del [PATH...]\n"
"\n"
"Deletes the station or category at each PATH.\n";

static const char help_bookmark_file[] =
"Usage: bookmark file FILE\n"
"\n"
"Sets FILE to the file that bookmarks should be loaded from and saved\n"
"to. This command has no effect on the currently loaded bookmarks and\n"
"neither loads not saves the file. Refer bookmark load and bookmark\n"
"save.\n";

static const char help_bookmark_list[] =
"Usage: bookmark list [PATH] [OPTION...]\n"
"\n"
"Lists all bookmarks that are rooted at PATH. If no PATH is given all\n"
"bookmarks are listed.\n"
"\n"
"Options:\n"
"  -u, --url    include url of each bookmark.\n";

static const char help_bookmark_load[] =
"Usage: bookmark load [FILE]\n"
"\n"
"Loads bookmarks from the bookmark file. If FILE is given bookmark\n"
"file is called implicitly prior to loading.\n";

static const char help_bookmark_save[] =
"Usage: bookmark save [FILE]\n"
"\n"
"Saves bookmarks to the bookmark file. If FILE is given bookmark file\n"
"is called implicitly prior to saving.\n";

static const char help_bookmark_save_on_change[] =
"Usage: bookmark save-on-change [yes|no]\n"
"\n"
"Specify whether or not bookmarks should be saved whenever changed.\n"
"If the argument is omitted yes is assumed.\n";

static const char help_bookmark_save_on_exit[] =
"Usage: bookmark save-on-exit [yes|no]\n"
"\n"
"Specify whether or not bookmarks should be saved when the daemon\n"
"exits. If the argument is omitted yes is assumed.\n";

static const char help_bookmark_save_periodic[] =
"Usage: bookmark save-periodic INTERVAL\n"
"\n"
"Saves bookmarks periodically each INTERVAL seconds. If INTERVAL is\n"
"periodic saving is disabled.\n";

static const char help_bookmark_url[] =
"Usage: bookmark url PATH\n"
"\n"
"Returns the url for the station at PATH.\n";

static int NONNULL
arg_to_uint(const char * arg, unsigned * dest)
{
    unsigned long ul;
    char *        endptr;

    if (*arg)
    {
        errno = 0;
        ul = strtoul(arg, &endptr, 0);
        if (ul >= UINT_MAX)
        {
            errno = ERANGE;
        }
        if (errno)
        {
            log_error_errno("could not convert argument");
            return 1;
        }
        else if (*endptr != '\0')
        {
            log_error("could not convert argument: "
                      "argument not fully converted");
            return 1;
        }
        *dest = (unsigned)ul;
        return 0;
    }
    else
    {
        log_error("could not convert argument: argument is empty");
        return 1;
    }
}

/*
 * Logs a GOP error and requests GOP to return control to the command.
 */
static gop_return_t
aterror_gop(gop_t * gop)
{
    gop_error_t error = gop_get_error(gop);
    if (gop_get_errno_set(gop))
    {
        log_error_errno("%s", gop_strerror(error));
    }
    else
    {
        log_error("%s", gop_strerror(error));
    }
    return GOP_DO_RETURN;
}

/* Parses the option table using GOP. */
static int
parse_gop_table(const gop_option_t * table,
                uint8_t * argcp,
                char ** argv)
{
    int     retval = 1;
    int     ret;
    int     argc = (int)*argcp;
    gop_t * gop;

    gop = gop_new();
    if (!gop)
    {
        log_error("could not create GOP context");
        goto error;
    }

    /* By default GOP exits on error, so make sure it does not. */
    gop_aterror(gop, aterror_gop);

    /*
     * No need to set an exit handler since gop will not try to exit
     * unless a callback requests exit, which shall not happen.
     */

    if (gop_add_table(gop, NULL, table))
    {
        goto error;
    }

    ret = gop_parse(gop, &argc, argv);
    if (ret < 0)
    {
        goto error;
    }
    *argcp = (uint8_t)argc;

    retval = 0;
error:
    gop_destroy(gop);
    return retval;
}

static int32_t NONNULLA(2,4,5)
cmd_yesno(uint8_t argc,
          char ** argv,
          return_t ** retp,
          yesno_func_t * yesfunc,
          yesno_func_t * nofunc)
{
    if (argc == 1)
    {
        return (*yesfunc)(retp);
    }
    else if (argc == 2)
    {
        if (strcmp(argv[1], "no") == 0)
        {
            return (*nofunc)(retp);
        }
        else if (strcmp(argv[1], "yes") == 0)
        {
            return (*yesfunc)(retp);
        }
        else
        {
            log_error("invalid argument \"%s\"", argv[1]);
            return 1;
        }
    }
    else
    {
        log_error("invalid number of arguments");
        return 1;
    }
}

static int32_t
cmd_fetcher(uint8_t argc, char ** argv, return_t ** retp)
{
    int32_t retval = 1;
    if (argc == 2)
    {
        retval = fetcher_set(argv[1]);
    }
    else
    {
        log_error("invalid number of arguments");
    }
    return retval;
}

static int32_t
cmd_load(uint8_t argc, char ** argv, return_t ** retp)
{
    int32_t    retval = 0;
    for (uint8_t i = 1; i < argc; ++i)
    {
        retval += module_load_mstr(argv[i]);
    }
    return retval;
}

static int32_t
cmd_logdomain_yes(return_t ** retp)
{
    log_set_domain(1);
    return 0;
}

static int32_t
cmd_logdomain_no(return_t ** retp)
{
    log_set_domain(0);
    return 0;
}

static int32_t
cmd_logdomain(uint8_t argc, char ** argv, return_t ** retp)
{
    return cmd_yesno(argc,
                     argv,
                     retp,
                     cmd_logdomain_yes,
                     cmd_logdomain_no);
}

static int32_t
cmd_logfile(uint8_t argc, char ** argv, return_t ** retp)
{
    if (argc == 1)
    {
        log_close();
        return 0;
    }
    else if (argc == 2)
    {
        return log_open(argv[1]);
    }
    else
    {
        log_error("invalid number of arguments");
        return 1;
    }
}

static int32_t
cmd_loglevel(uint8_t argc, char ** argv, return_t ** retp)
{
    for (uint8_t i = 1; i < argc; ++i)
    {
        if (log_parse_level(argv[i]))
        {
            return 1;
        }
    }
    return 0;
}

static int32_t
cmd_pause(uint8_t argc, char ** argv, return_t ** retp)
{
    /* FIXME */
    return 0;
}

static int32_t
cmd_pidfile(uint8_t argc, char ** argv, return_t ** retp)
{
    if (argc == 1)
    {
        pidfile_unlink();
        return 0;
    }
    else if (argc == 2)
    {
        return pidfile_create(argv[1]);
    }
    else
    {
        log_error("invalid number of arguments");
        return 1;
    }
}

static int32_t cmd_resume(uint8_t argc, char ** argv, return_t ** retp);
static int32_t
cmd_play(uint8_t argc, char ** argv, return_t ** retp)
{
    char * url;

    if (argc == 1)
    {
        /* If there is no argument, try to resume. */
        return cmd_resume(argc, argv, retp);
    }
    else if (argc > 2)
    {
        log_error("too many arguments");
        return 1;
    }
    else
    {
        url = bookmark_url(argv[1]);
        if (!url)
        {
            return 1;
        }
        return player_play(url);
    }
}

static int32_t
cmd_resume(uint8_t argc, char ** argv, return_t ** retp)
{
    /* FIXME */
    return 0;
}

static int32_t
cmd_stop(uint8_t argc, char ** argv, return_t ** retp)
{
    /* FIXME */
    return 0;
}

static int32_t
cmd_syslog(uint8_t argc, char ** argv, return_t ** retp)
{
    if (argc == 1)
    {
        log_syslog(1);
        return 0;
    }
    else if (argc == 2)
    {
        if (strcmp(argv[1], "no") == 0)
        {
            log_syslog(0);
            return 0;
        }
        else if (strcmp(argv[1], "yes") == 0)
        {
            log_syslog(1);
            return 0;
        }
        else
        {
            log_error("invalid argument \"%s\"", argv[1]);
            return 1;
        }
    }
    else
    {
        log_error("invalid number of arguments");
        return 1;
    }
}

static int32_t
cmd_toggle(uint8_t argc, char ** argv, return_t ** retp)
{
    /* FIXME */
    return 0;
}

static int32_t
cmd_unload(uint8_t argc, char ** argv, return_t ** retp)
{
    for (uint8_t i = 1; i < argc; ++i)
    {
        module_unload(argv[i]);
    }
    return 0;
}

static int32_t
subcmd_bookmark_add(uint8_t argc, char ** argv, return_t ** retp)
{
    if (argc != 3)
    {
        log_error("invalid number of arguments");
        return 1;
    }

    return bookmark_add(argv[1], argv[2]);
}

static int32_t
subcmd_bookmark_del(uint8_t argc, char ** argv, return_t ** retp)
{
    int retval = 0;
    for (uint8_t i = 1; i < argc; ++i)
    {
        retval += bookmark_del(argv[i]);
    }
    return retval;
}

static int32_t
subcmd_bookmark_file(uint8_t argc, char ** argv, return_t ** retp)
{
    if (argc != 2)
    {
        log_error("invalid number of arguments");
        return 1;
    }
    bookmark_file(argv[1]);
    return 0;
}

/* FIXME: GOP will leak memory when arguments are removed from the
 *        array. */
static int32_t
subcmd_bookmark_list(uint8_t argc, char ** argv, return_t ** retp)
{
    char *  path = NULL;
    int     url  = 0;

    const gop_option_t options[] = {
        {"url", 'u', GOP_NONE, &url, NULL, NULL, NULL},
        GOP_TABLEEND
    };

    if (parse_gop_table(options, &argc, argv))
    {
        return 1;
    }

    if (argc <= 2)
    {
        path = argv[1];
    }
    else
    {
        log_error("too many arguments");
        return 1;
    }

    *retp = bookmark_list(path, url);
    return *retp ? 0 : 1;
}

static int32_t
subcmd_bookmark_load(uint8_t argc, char ** argv, return_t ** retp)
{
    if (argc == 2)
    {
        bookmark_file(argv[1]);
    }
    else if (argc != 1)
    {
        log_error("invalid number of arguments");
        return 1;
    }
    return bookmark_load();
}

static int32_t
subcmd_bookmark_save(uint8_t argc, char ** argv, return_t ** retp)
{
    if (argc == 2)
    {
        bookmark_file(argv[1]);
    }
    else if (argc != 1)
    {
        log_error("invalid number of arguments");
        return 1;
    }
    return bookmark_save();
}

static int32_t
subcmd_bookmark_save_on_change_yes(return_t ** retp)
{
    bookmark_save_on_change(1);
    return 0;
}

static int32_t
subcmd_bookmark_save_on_change_no(return_t ** retp)
{
    bookmark_save_on_change(0);
    return 0;
}

static int32_t
subcmd_bookmark_save_on_change(uint8_t argc,
                               char ** argv,
                               return_t ** retp)
{
    return cmd_yesno(argc,
                     argv,
                     retp,
                     subcmd_bookmark_save_on_change_yes,
                     subcmd_bookmark_save_on_change_no);
}

static int32_t
subcmd_bookmark_save_on_exit_yes(return_t ** retp)
{
    bookmark_save_on_exit(1);
    return 0;
}

static int32_t
subcmd_bookmark_save_on_exit_no(return_t ** retp)
{
    bookmark_save_on_exit(0);
    return 0;
}

static int32_t
subcmd_bookmark_save_on_exit(uint8_t argc,
                             char ** argv,
                             return_t ** retp)
{
    return cmd_yesno(argc,
                     argv,
                     retp,
                     subcmd_bookmark_save_on_exit_yes,
                     subcmd_bookmark_save_on_exit_no);
}

static int32_t
subcmd_bookmark_save_periodic(uint8_t argc,
                              char ** argv,
                              return_t ** retp)
{
    unsigned period;

    if (argc != 2)
    {
        log_error("invalid number of arguments");
        return 1;
    }

    if (arg_to_uint(argv[1], &period))
    {
        return 1;
    }

    return bookmark_save_periodic(period);
}

static int32_t
subcmd_bookmark_url(uint8_t argc, char ** argv, return_t ** retp)
{
    int        retval = 0;
    char *     url    = NULL;
    return_t * ret    = NULL;

    if (argc != 2)
    {
        log_error("invalid number of arguments");
        return 1;
    }

    ret = return_new(1);
    if (!ret)
    {
        return 1;
    }
    url = bookmark_url(argv[1]);
    if (!url)
    {
        return_unref(ret);
        return 1;
    }
    return_set(ret, 0, RETURN_TYPE_STRING, url);
    mstr_unref(url);

    *retp = ret;
    return retval;
}

#define COMMAND(name) {#name, help_ ## name, &cmd_ ## name, NULL, 0}
#define SUBCOMMAND_HYPHEN(category, name, identifier) \
    {name, \
        help_ ## category ## _ ## identifier, \
        &subcmd_ ## category ## _ ## identifier, \
        NULL, 0}
#define SUBCOMMAND(c, n) SUBCOMMAND_HYPHEN(c, #n, n)

static const command_t bookmark_subcommands[] =
{
    SUBCOMMAND(bookmark, add),
    SUBCOMMAND(bookmark, del),
    SUBCOMMAND(bookmark, file),
    SUBCOMMAND(bookmark, list),
    SUBCOMMAND(bookmark, load),
    SUBCOMMAND(bookmark, save),
    SUBCOMMAND_HYPHEN(bookmark, "save-on-change", save_on_change),
    SUBCOMMAND_HYPHEN(bookmark, "save-on-exit", save_on_exit),
    SUBCOMMAND_HYPHEN(bookmark, "save-periodic", save_periodic),
    SUBCOMMAND(bookmark, url),
};

static const command_t commands[] =
{
    {
        "bookmark",
        help_bookmark,
        NULL,
        bookmark_subcommands,
        ARRAY_SIZE(bookmark_subcommands)
    },
    COMMAND(fetcher),
    COMMAND(load),
    COMMAND(logdomain),
    COMMAND(logfile),
    COMMAND(loglevel),
    COMMAND(pause),
    COMMAND(pidfile),
    COMMAND(play),
    COMMAND(resume),
    COMMAND(stop),
    COMMAND(syslog),
    COMMAND(toggle),
    COMMAND(unload),
};

int
builtin_commands_register(void)
{
    return command_register_array(commands, ARRAY_SIZE(commands));
}
