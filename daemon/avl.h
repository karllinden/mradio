/*
 * This file is part of mradio.
 *
 * Copyright (C) 2016-2017 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#ifndef AVL_H
# define AVL_H 1

# if HAVE_CONFIG_H
#  include <config.h>
# endif /* HAVE_CONFIG_H */

# include <mradio/common.h>

/* FIXME: Implement (3) and (4) */
/* In this file there are four implementations.
 * (1) The basic and most general implementation in which the key must
 *     be stored in the data block, which is allocated on insertion.
 *     This is the actual implementation. All other implementations are
 *     just wrappers.
 * (2) A similar implementation where each node has a pointer to a data
 *     structure. This implementation has _dp (data pointer) in all its
 *     routines. Also this implentation requires the caller to store the
 *     key inside the data block.
 * (3) An implementation similar to (1), but where the key has explicit
 *     storage. This means the caller does not need to keep the key
 *     inside its own data block. This implementation has the keyword
 *     _kd (key and data).
 * (4) An implementation similar to (2), but where again the key has
 *     explicit storage. This implentation has the keyword _kdp. */

typedef struct avl_node_s avl_node_t;

/* Compares a key, as given to one of the functions, to an initialized
 * data block. */
typedef int (avl_cmp_func_t)(const void * key, const void * data,
                             const void * extra);
typedef int (avl_init_func_t)(void * key, void * data, void * extra);
typedef int (avl_func_t)(void * data, void * extra);

typedef avl_cmp_func_t avl_dp_cmp_func_t;
typedef avl_func_t avl_dp_func_t;

struct avl_s
{
    avl_node_t *     root;
    avl_cmp_func_t * cmp_func;
    avl_func_t *     destroy_func;
    const void *     extra;
};
typedef struct avl_s avl_t;

struct avl_dp_s
{
    avl_t               avl;
    avl_dp_cmp_func_t * cmp_func;
    avl_dp_func_t *     destroy_func;
    const void *        extra;
};
typedef struct avl_dp_s avl_dp_t;

/* Initializes a new AVL tree. The destroy_func is a function that will
 * be called when data associated with a key is destroyed either as a
 * result of avl_delete or avl_deinit. extra will be passed to a such
 * destroy_func. It may be NULL, in which case no deinitialization of
 * the data is made. avl_init always succeeds. */
void   avl_init          (avl_t * avl,
                          avl_cmp_func_t * cmp_func,
                          avl_func_t * destroy_func,
                          const void * extra) NONNULLA(1,2);

/* Deinitializes the AVL tree and invokes destroy_func on all data left
 * in the tree. The sum of the return values is returned or 0 if there
 * is no destroy_func. */
int    avl_deinit        (avl_t * avl) NONNULL;

/* Inserts a data entry into the tree and returns the data field
 * associated with the key. */
void * avl_insert        (avl_t * avl,
                          const void * key,
                          size_t data_size) NONNULL;

/* Searches for a key in the AVL tree and returns the corresponding data
 * field if the key was found, or NULL otherwise. */
void * avl_search        (avl_t * avl,
                          const void * key) NONNULL PURE;

/* A mixture of the insert and search operation. Searches for key in the
 * tree. If the key exists the data associated with it is returned. If
 * the key did not exist it is created and the associated data is
 * initialized with init_func. init_func may be NULL in which case the
 * data will not be initialized. A non-zero return from the init
 * function indicates initialization failure and aborts the insertion.
 * If an error occured NULL is returned. */
void * avl_insearch      (avl_t * avl,
                          const void * key,
                          size_t data_size,
                          avl_init_func_t * init_func,
                          const void * extra) NONNULLA(1,2);

/* Deletes a key from the tree. If the key did not exist the returned
 * value is negative. Otherwise the return value of the destroy function
 * is returned (if any). Lastly, if there was no destroy function 0 is
 * returned. */
int    avl_delete        (avl_t * avl,
                          const void * key) NONNULL;

/* Deletes a the key corresponding to data from AVL. Works like
 * avl_delete, except data is passed rather than the key. */
int    avl_delete_data   (avl_t * avl,
                          const void * data) NONNULL;

/* Recurses the avl tree in-order and calls the supplied function FUNC
 * for every node. To FUNC the associated data and optional pointer
 * EXTRA is passed. If FUNC returns non-zero the recursion stops and
 * the return value equals the returned one from FUNC. Otherwise 0 is
 * returned. */
int    avl_recurse       (const avl_t * avl,
                          avl_func_t * func,
                          const void * extra) NONNULLA(1,2);

void   avl_dp_init       (avl_dp_t * avldp,
                          avl_dp_cmp_func_t * cmp_func,
                          avl_dp_func_t * destroy_func,
                          const void * extra) NONNULLA(1,2);
int    avl_dp_deinit     (avl_dp_t * avldp) NONNULL;
int    avl_dp_insert     (avl_dp_t * avldp,
                          const void * key,
                          const void * data) NONNULL;
void * avl_dp_search     (avl_dp_t * avldp,
                          const void * key) NONNULL PURE;
int    avl_dp_delete     (avl_dp_t * avldp,
                          const void * key) NONNULL;
int    avl_dp_recurse    (avl_dp_t * avldp,
                          avl_dp_func_t * func,
                          const void * extra) NONNULLA(1,2);

#endif /* !AVL_H */
