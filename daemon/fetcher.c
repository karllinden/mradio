/*
 * This file is part of mradio.
 *
 * Copyright (C) 2017 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#define MRADIO_LOG_DOMAIN MRADIO_LOG_DOMAIN_DAEMON /* FIXME */

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <assert.h>
#include <stddef.h>

#include <mradio/common.h>
#include <mradio/log.h>
#include <mradio/mstr.h>

#include "fetcher.h"
#include "module.h"

/* Array of known fetchers in default order. */
static const char * fetcher_known[] = {
    "curl",
    NULL
};

static char *          fetcher_selected = NULL; /* mstr */
static module_handle_t fetcher_handle   = MODULE_HANDLE_INITIALIZER;

static void
fetcher_selected_null(void)
{
    mstr_unref(fetcher_selected);
    fetcher_selected = NULL;
    return;
}

int NONNULL
fetcher_set(char * name) /* mstr */
{
    if (module_handle_loaded(&fetcher_handle))
    {
        log_error("a fetcher is already loaded");
        return 1;
    }
    else if (fetcher_selected)
    {
        log_error("only one fetcher can be selected");
        return 1;
    }
    else
    {
        mstr_ref(name);
        fetcher_selected = name;
        return 0;
    }
}

int
fetcher_init(void)
{
    int ret;

    /* This function shall not be called more than once. */
    assert(!fetcher_module);

    if (fetcher_selected)
    {
        ret = module_handle_load_mstr(fetcher_selected,
                                      &fetcher_handle);
        fetcher_selected_null();
        if (ret)
        {
            return 1;
        }
    }
    else
    {
        /* Go through the list of known fetchers and try to load each
         * one of them. */
        ret = 1;
        for (unsigned i = 0;
             i < ARRAY_SIZE(fetcher_known) && ret;
             ++i)
        {
            ret = module_handle_load(fetcher_known[i],
                                     &fetcher_handle);
        }
        if (ret)
        {
            log_error("could not load a fetcher");
            return 1;
        }
    }
    return 0;
}

void
fetcher_deinit(void)
{
    if (module_handle_loaded(&fetcher_handle))
    {
        module_handle_unload(&fetcher_handle);
    }
    fetcher_selected_null();
    return;
}
