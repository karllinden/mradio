/*
 * This file is part of mradio.
 *
 * Copyright (C) 2016-2017 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#ifndef BOOKMARK_H
# define BOOKMARK_H 1

# if HAVE_CONFIG_H
#  include <config.h>
# endif /* HAVE_CONFIG_H */

# include <mradio/common.h>
# include <mradio/return.h>

void       bookmark_init          (void);
void       bookmark_deinit        (void);

void       bookmark_file          (char * filename) NONNULL; /* mstr */
int        bookmark_load          (void);
int        bookmark_save          (void);

void       bookmark_save_on_change(int save_on_change);
void       bookmark_save_on_exit  (int save_on_exit);

# if !BOOKMARK_NO_ML
int        bookmark_save_periodic (unsigned int periodic);
# endif /* !BOOKMARK_NO_ML */

/* Bookmarks a station.
 * Arguments:
 *  path - ordinary string giving the path where the station should be
 *         added
 *  url  - an mstr giving the url
 * Returns zero on success or non-zero otherwise. */
int        bookmark_add           (const char * path,
                                   char * url)            NONNULL;
int        bookmark_del           (const char * path)     NONNULL;

/* returns mstr */
char *     bookmark_url           (const char * path)     NONNULL;

return_t * bookmark_list          (const char * path,
                                   int url);

#endif /* !BOOKMARK_H */
