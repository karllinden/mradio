/*
 * This file is part of mradio.
 *
 * Copyright (C) 2016 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#define MRADIO_LOG_DOMAIN MRADIO_LOG_DOMAIN_AVL

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <stddef.h>
#include <stdlib.h>
#include <string.h>

#include <mradio/common.h>
#include <mradio/log.h>

#include <common/massert.h>

#include "avl.h"

#define LEFT  0
#define RIGHT 1

_Static_assert (LEFT == !RIGHT, "left is not not-right");
_Static_assert (RIGHT == !LEFT, "right is not not-left");

struct avl_node_s
{
    int          factor;
    avl_node_t * parent;
    avl_node_t * child[2];
    char data[];
};

struct avl_dp_data_s
{
    void * data;
};
typedef struct avl_dp_data_s avl_dp_data_t;

struct avl_dp_recurse_data_s
{
    avl_dp_func_t * func;
    const void *    extra;
};
typedef struct avl_dp_recurse_data_s avl_dp_recurse_data_t;

static avl_node_t * NONNULL
avl_node_new(size_t data_size)
{
    avl_node_t * node;
    node = malloc(sizeof(avl_node_t) + data_size);
    if (node == NULL)
    {
        log_error_errno("could not allocate memory");
        return NULL;
    }

    node->factor       = 0;
    node->parent       = NULL;
    node->child[LEFT]  = NULL;
    node->child[RIGHT] = NULL;

    return node;
}

static int
avl_node_destroy(avl_node_t * node,
                 avl_func_t * destroy_func,
                 const void * extra)
{
    int          ret = 0;
    avl_node_t * next;
    if (node != NULL)
    {
        node->parent = NULL;
    }
    while (node != NULL)
    {
        if (node->child[LEFT] != NULL)
        {
            next = node->child[LEFT];
            node->child[LEFT] = NULL;
        }
        else if (node->child[RIGHT] != NULL)
        {
            next = node->child[RIGHT];
            node->child[RIGHT] = NULL;
        }
        else
        {
            next = node->parent;
            if (destroy_func != NULL)
            {
                ret += (*destroy_func)(node->data, (void *)extra);
            }
            free(node);
        }
        node = next;
    }
    return ret;
}

void NONNULLA(1)
avl_init(avl_t * avl,
         avl_cmp_func_t * cmp_func,
         avl_func_t * destroy_func,
         const void * extra)
{
    avl->root         = NULL;
    avl->cmp_func     = cmp_func;
    avl->destroy_func = destroy_func;
    avl->extra        = extra;
    return;
}

int NONNULL
avl_deinit(avl_t * avl)
{
    return avl_node_destroy(avl->root, avl->destroy_func, avl->extra);
}

/* Finds the place where the key should be. Returns the place where
 * the node should be using the nodepp argument (i.e. *nodepp will be
 * set to a pointer to the node place. Also the parent node is returned
 * (using the parentp argument). The return value indicates the
 * in which direction (LEFT/RIGHT) the node is from the parent.
 * parent may be NULL in which case *nodep = &avl->root and the
 * returned direction is undefined. */
static unsigned NONNULLA(1,2,3)
avl_place(avl_t * avl,
          const void * key,
          avl_node_t *** nodepp,
          avl_node_t **  parentp)
{
    avl_node_t *  parent = NULL;
    avl_node_t ** nodep  = &avl->root;
    avl_node_t *  node;
    unsigned      dir    = LEFT;
    int           cmp;

    while (*nodep != NULL)
    {
        node = *nodep;
        cmp = (*avl->cmp_func)(key, node->data, avl->extra);
        if (cmp < 0)
        {
            dir = LEFT;
        }
        else if (cmp > 0)
        {
            dir = RIGHT;
        }
        else /* if (cmp == 0) */
        {
            break;
        }
        parent = node;
        nodep  = parent->child + dir;
    }

    if (parentp != NULL)
    {
        *parentp = parent;
    }
    *nodepp  = nodep;
    return dir;
}

static inline int
dir_to_bal(unsigned dir) /* dir == LEFT || dir == RIGHT */
{
    if (dir == LEFT)
    {
        return -1;
    }
    else /* if (dir == RIGHT) */
    {
        massert(dir == RIGHT);
        return 1;
    }
}

static inline unsigned
bal_to_dir(int bal) /* bal == -1 || bal == 1 */
{
    if (bal == -1)
    {
        return LEFT;
    }
    else /* if (bal == 1) */
    {
        massert(bal == 1);
        return RIGHT;
    }
}

static unsigned NONNULL
avl_dir(const avl_node_t * parent,
        const avl_node_t * child)
{
    massert(child->parent == parent);
    if (child == parent->child[LEFT])
    {
        return LEFT;
    }
    else
    {
        massert(child == parent->child[RIGHT]);
        return RIGHT;
    }
}

static avl_node_t **
avl_ptr(avl_t * avl,
        avl_node_t * node)
{
    avl_node_t * parent = node->parent;
    unsigned     dir;
    if (parent != NULL)
    {
        dir = avl_dir(parent, node);
        return &parent->child[dir];
    }
    else
    {
        return &avl->root;
    }
}

/*
 * Rotates tree rooted at node depending on direction (LEFT/RIGHT), but
 * does not update factors.
 *
 * LEFT
 * ----
 *     N            C
 *    / \          / \
 *   C  T2  ==>>  T1  N
 *  / \              / \
 * T1  G            G   T2
 *
 * RIGHT
 * -----
 *     N              C
 *    / \            / \
 *   T1  C  ==>>    N  T2
 *      / \        / \
 *     G  T2      T1  G
 */
static void NONNULL
avl_rotate(avl_t * avl,
           avl_node_t * node,
           unsigned dir)
{
    avl_node_t ** nodep = avl_ptr(avl, node);
    avl_node_t *  child = node->child[dir];
    avl_node_t *  grand = child->child[!dir]; /* grandchild */

    /* link child */
    *nodep = child;
    child->parent = node->parent;

    /* link node */
    child->child[!dir] = node;
    node->parent = child;

    /* link grandchild */
    node->child[dir] = grand;
    if (grand != NULL)
    {
        grand->parent = node;
    }

    return;
}

/* Balances an unbalanced (factor == +-2) node. This can be used both
 * after an insertion and after a deletion.
 *
 * In case of insertion the return value may be ignored. The height of
 * the tree before insertion is always the same as the height after
 * insertion.
 *
 * In case of deletion the return value is zero if the tree was reduced
 * in height or non-zero otherwise. */
static int NONNULL
avl_balance(avl_t * avl,
            avl_node_t * node,
            unsigned dir)
{
    int          retval = 0;
    int          p;
    int          bal;
    avl_node_t * child = node->child[dir];
    avl_node_t * grand = child->child[!dir];

    /* assert that this node is too unbalanced */
    massert(node->factor == dir_to_bal(dir) * 2);

    /* Sign of this product will be used after the assertion. */
    p = node->factor * child->factor;

    /* In the following the tree will be rotated and factors will be
     * updated. Since the rotating routine does not use the factors,
     * the order of (1) rotation and (2) factor updating is irrelevant.
     * Therefore the rotation at node in direction dir (which is done
     * lastly in all cases) can be moved outside the if-else block. */

    if (p > 0)
    {
        /* Both N (node) and C (child) were unbalanced in the same
         * direction. Thinking about it, this means the situation is
         * like the following:
         * (Only the right case is shown, since the right one is just
         * mirroring this case.)
         *      N[2]{x+3}
         *      / \
         *  T1{x}  C[1]{x+2}
         *        / \
         *      G{x} T2{x+1}
         * To fix this avl_rotate is invoked with the same direction as
         * here to produce something like this:
         *          C[0]{x+2}
         *          / \
         *       N[0] T2{x+1}
         *      / \
         *  T1{x}  G{x}
         * Here the balances can be seen from the image.
         *
         * INSERTION
         * ---------
         * Now note that the newly inserted node must have been in the
         * T2 tree, since otherwise the tree would have been in an
         * unbalanced state before the new node was inserted. Therefore
         * the previous height (before insertion) of the tree rooted at
         * N was x+2, and now the tree rooted at same position (but now
         * C) is also x+2. Thus, there has been no change in height and
         * the caller, avl_balance_insertion, may return.
         *
         * DELETION
         * --------
         * Now the tree reduced in height so 0 (the default) must be
         * returned. */
        node->factor  = 0;
        child->factor = 0;
    }
    else if (p < 0)
    {
        /* N and C are unbalanced in opposite directions. This means
         * the situation is something like the following:
         *        N[2]{x+3}
         *        / \
         *    T1{x}  C[-1]{x+2}
         *             / \
         *     G[a]{x+1}  T4{x}
         *    /     \
         *  T2{y}   T3{z}
         * (For sure the height x >= 0, so G != NULL.)
         * If the sub-tree rooted at C is rotated in direction !dir one
         * gets the following:
         *      N[2]
         *      / \
         *  T1{x}  G
         *        / \
         *   T2{y}   C
         *          / \
         *     T3{z}   T4{x}
         * Followed by a rotation in direction dir at N one gets
         *            G[0]{x+2}
         *          /     \
         *      N[b]       C[c]
         *     /   \       /   \
         *  T1{x} T2{y}  T3{z} T4{x}
         * Note that the balance of G, 0, can be computed without
         * knowledge of a. Since the height of the tree rooted at G
         * in the initial stage was x+1 the trees T2 and T3 cannot be
         * higher than x. Thus, the trees rooted at N and C in the last
         * picture are both of height x+1 respectively.
         *
         * Depending on the value of the balance factor a, one gets
         * different balances b and c. Since the tree rooted at G
         * is valid there are three possibilities for a.
         *  a == -1:
         *    In this case one has y=x and z=x-1 so b=0 and c=1.
         *  a == 0:
         *    Here x=y=z, so b=c=0.
         *  a == 1:
         *    Now y=x-1 and z=x, which means b=-1 and c=0.
         * Note that since the dir=RIGHT (=> bal=1) case has been
         * discussed one must compensate with a factor of
         * dir_to_bal(dir) in the code (since the left case is the same
         * but with reflected numbers).
         *
         * INSERTION
         * ---------
         * The inserted node must have been inserted to the G tree in
         * the first picture. Because the tree was balanced before the
         * G tree cannot have been of height x+1 before insertion.
         * Therefore only one of y and z are x, and that is due to the
         * insertion. The other one is x-1. Thus the factor a == 0 will
         * never occur. Do note that the height increase due to the
         * inserted node has been compensated so the caller may return
         * after this call.
         *
         * DELETION
         * --------
         * Note that the tree reduced in height. This means 0 (the
         * default) shall be returned. In the deletion case, however,
         * a == 0 is possible. */
        massert(child->child[!dir] != NULL);
        avl_rotate(avl, child, !dir);
        bal = dir_to_bal(dir);
        if (grand->factor == -bal)
        {
            node->factor  = 0;
            child->factor = bal;
        }
        else if (grand->factor == bal)
        {
            node->factor  = -bal;
            child->factor = 0;
        }
        else /* if (grand->factor == 0) */
        {
            massert(grand->factor == 0);
            node->factor  = 0;
            child->factor = 0;
        }
        grand->factor = 0;
    }
    else /* if (p == 0) */
    {
        /* INSERTION
         * ---------
         * For the product to be zero child->factor must have been zero,
         * since node->factor is known to be non-zero, but if
         * child->factor was zero then the avl_balance_insertion loop
         * must have stopped there since the tree rooted at child did
         * not change height, which would mean this code will never be
         * reached on insertion. */

        /* DELETION
         * --------
         * On deletion, however, this code may be reached. In that case
         * the situation is like the following:
         *      N[2]{x+3}
         *      / \
         *  T1{x}  C[0]{x+2}
         *         /   \
         *    G{x+1}   T2{x+1}
         * To fix this avl_rotate is invoked with the same direction as
         * dir to produce something like this:
         *          C[-1]{x+3}
         *          /       \
         *       N[1]{x+2}  T2{x+1}
         *      /   \
         *  T1{x}   G{x+1}
         * Here the balances can be seen from the image.
         *
         * Since the tree was not reduced, 1 must be returned. */
        retval = 1;
        bal = dir_to_bal(dir);
        node->factor  = bal;
        child->factor = -bal;
    }
    avl_rotate(avl, node, dir);

    /* In any of the insertion cases, the height of the current sub-tree
     * (the one that was rooted at N, but is now rotated) has not
     * changed as a result of the insertion. Therefore, the caller of
     * this function, avl_balance_insertion, can break its loop. */

    return retval;
}

static void NONNULL
avl_balance_insertion(avl_t * avl,
                      avl_node_t * node)
{
    avl_node_t * parent = node->parent;
    unsigned     dir;

    while (parent != NULL)
    {
        /* Each time this loop is entered the tree rooted at node has
         * grown by one node. First find which direction the tree has
         * grown. */
        dir = avl_dir(parent, node);

        massert(parent->factor == -1 ||
                parent->factor == 0 ||
                parent->factor == 1);

        /* add the balance contributed by the grown tree to the factor
         * of the parent */
        parent->factor += dir_to_bal(dir);

        if (parent->factor == -2)
        {
            avl_balance(avl, parent, LEFT);
            break;
        }
        else if (parent->factor == 2)
        {
            avl_balance(avl, parent, RIGHT);
            break;
        }
        else if (parent->factor == 0)
        {
            /* In this case the addition weighted up for a slight
             * unbalancing, so the tree has not grown. Thus, the balance
             * of the ancestor tree is correct and hence balanced
             * (because it was balanced before). It is safe to return
             * from here, which is done by breaking. */
            break;
        }
        else {
            massert(parent->factor == -1 || parent->factor == 1);
        }

        /* The cases factor == -1 and factor == 1 have not been
         * handleded. In case of -1 the inserted node was in the left
         * tree. Similarly in case of factor == 1 the inserted node was
         * in the right tree. In both these cases the previous factor
         * was 0, so the height of the tree rooted at parent must
         * therefore have grown by one, so the loop shall continue. */
        node = parent;
        parent = node->parent;
    }

    return;
}

static void * NONNULLA(1,2,4)
avl_node_insert(avl_t * avl,
                avl_node_t ** nodep,
                avl_node_t * parent,
                const void * key,
                size_t data_size)
{
    avl_node_t * node = avl_node_new(data_size);
    if (node == NULL)
    {
        return NULL;
    }

    /* link new node */
    *nodep = node;
    node->parent = parent;

    /* balance */
    avl_balance_insertion(avl, node);

    return node->data;
}

void * NONNULL
avl_insert(avl_t * avl,
           const void * key,
           size_t data_size)
{
    avl_node_t *  parent;
    avl_node_t ** nodep;

    avl_place(avl, key, &nodep, &parent);
    if (*nodep != NULL)
    {
        log_error("could not insert avl node: key already exists");
        return NULL;
    }
    else
    {
        return avl_node_insert(avl, nodep, parent, key, data_size);
    }
}

void * NONNULL PURE
avl_search(avl_t * avl,
           const void * key)
{
    avl_node_t ** nodep;
    avl_place(avl, key, &nodep, NULL);
    if (*nodep != NULL)
    {
        return (*nodep)->data;
    }
    else
    {
        return NULL;
    }
}

void * NONNULLA(1,2)
avl_insearch(avl_t * avl,
             const void * key,
             size_t data_size,
             avl_init_func_t * init_func,
             const void * extra)
{
    avl_node_t *  parent;
    avl_node_t ** nodep;
    void *        data;
    avl_place(avl, key, &nodep, &parent);
    if (*nodep != NULL)
    {
        return (*nodep)->data;
    }
    else
    {
        data = avl_node_insert(avl, nodep, parent, key, data_size);
        if (data == NULL)
        {
            return NULL;
        }
        if (init_func != NULL && (*init_func)((void *)key, data,
                                              (void *)extra))
        {
            avl_delete(avl, key);
            return NULL;
        }
        return data;
    }
}

/* Call this when the DIR sub-tree of node has been reduced by one in
 * height. */
static void NONNULL
avl_balance_reduction(avl_t * avl,
                      avl_node_t * node,
                      unsigned dir)
{
    unsigned     nextdir;
    avl_node_t * next;
    while (node != NULL)
    {
        /* Acquire this data before rotation, because after rotation
         * this node may have been moved. */
        next = node->parent;
        if (next != NULL)
        {
            nextdir = avl_dir(next, node);
        }

        /* The sub-tree rooted at node's DIR child has been reduced by
         * one in height. This needs adjustment of this nodes factor. */
        node->factor += dir_to_bal(!dir);
        if (node->factor == 0)
        {
            /* The previous balance must have been +-1, which is now
             * compensated with the removal. This means the tree rooted
             * at node has decreased in height, so the loop must
             * continue. */
            /* empty */
        }
        else if (node->factor == -1 || node->factor == 1)
        {
            /* In this case the previous factor was 0, i.e. both
             * sub-trees were equally high. Therefore the removal
             * has not affected the height of the tree rooted at node,
             * because the other branch did not change height. Thus, the
             * tree is again balanced and the loop can stop. */
            return;
        }
        else /* if (node->factor == -2 || node->factor == 2) */
        {
            massert(node->factor == -2 || node->factor == 2);
            if (avl_balance(avl, node, !dir))
            {
                return;
            }
        }

        /* The tree rooted at node has been reduced by one in height so
         * the loop shall continue. */
        node = next;
        dir  = nextdir;
    }
    return;
}

static void NONNULL
avl_replace(avl_t * avl,
            avl_node_t * node,
            avl_node_t * repl)
{
    avl_node_t * parent = node->parent;
    avl_node_t * left   = node->child[LEFT];
    avl_node_t * right  = node->child[RIGHT];
    unsigned     dir;

    repl->parent = parent;
    if (parent != NULL)
    {
        dir = avl_dir(parent, node);
        parent->child[dir] = repl;
    }
    else
    {
        massert(avl->root == node);
        avl->root = repl;
    }

    repl->child[LEFT] = left;
    if (left != NULL)
    {
        left->parent = repl;
    }

    repl->child[RIGHT] = right;
    if (right != NULL)
    {
        right->parent = repl;
    }

    repl->factor = node->factor;

    return;
}

static int
avl_node_delete(avl_t * avl,
                avl_node_t * node)
{
    avl_node_t * parent =  node->parent;

    unsigned     dir;
    unsigned     pick;
    avl_node_t * child;
    avl_node_t * repl;
    avl_node_t * start;
    avl_node_t * tmp;

    /* Make a reasonable choice of which sub-tree to pick the
     * replacement from. */
    if (node->factor == 0)
    {
        /* No sub-tree is heigher than the other so which sub-tree
         * to pick cannot be known, but since this does not really
         * matter just pick the left one. */
        pick = LEFT;
    }
    else
    {
        /* Pick a replacement node from the heighest sub-tree, since
         * this will likely be a good choice. */
        pick = bal_to_dir(node->factor);
    }

    /* Pick the desired child and find a replacement. */
    child = node->child[pick];
    if (child != NULL)
    {
        /* Since the node has children the node must be replaced. A
         * good replacement is the !pick-most node in the
         * pick-subtree of the node. */
        repl = child;
        while (repl->child[!pick] != NULL)
        {
            repl = repl->child[!pick];
        }

        /* Determine where to start rebalancing, and in which
         * direction. */
        if (repl == child)
        {
            /* Since node will be replaced by child balancing should
             * start at repl and since the factor will be copied
             * from node to child, the pick-subtree has been reduced
             * by one. */
            start = repl;
            dir = pick;
        }
        else
        {
            /* Now the balancing must start at the parent of the
             * replacement node and the direction must be !pick. */
            start = repl->parent;
            dir = !pick;
        }

        /* Unlink replacement node. */
        parent = repl->parent;
        massert(repl == parent->child[dir]);
        tmp = repl->child[pick];
        parent->child[dir] = tmp;
        if (tmp != NULL)
        {
            tmp->parent = parent;
        }
        massert(repl->child[!pick] == NULL);
        repl->child[pick] = NULL;
        repl->parent = NULL;

        avl_replace(avl, node, repl);
        avl_balance_reduction(avl, start, dir);
    }
    else
    {
        /* The node had no children, i.e. it is a leaf node. This
         * case is simple, because now the node to delete can simply
         * be unlinked and then the tree can be reduction
         * rebalanced. If there was no parent this node must have
         * been the root, and since the node has no children this is
         * the last node of the tree. */
        if (parent != NULL)
        {
            dir = avl_dir(parent, node);
            parent->child[dir] = NULL;
            avl_balance_reduction(avl, parent, dir);
        }
        else
        {
            massert(avl->root == node);
            avl->root = NULL;
        }
    }

    node->parent       = NULL;
    node->child[LEFT]  = NULL;
    node->child[RIGHT] = NULL;
    return avl_node_destroy(node, avl->destroy_func, avl->extra);
}

int NONNULL
avl_delete(avl_t * avl,
           const void * key)
{
    avl_node_t ** nodep;
    avl_place(avl, key, &nodep, NULL);
    if (*nodep == NULL)
    {
        return -1;
    }
    else
    {
        return avl_node_delete(avl, *nodep);
    }
}

static avl_node_t * NONNULL PURE
avl_node(const void * data)
{
    avl_node_t * node;
    node = (avl_node_t *)((char *)data - offsetof(avl_node_t, data));
    return node;
}

int NONNULL
avl_delete_data(avl_t * avl,
                const void * data)
{
    avl_node_t * node = avl_node(data);
    return avl_node_delete(avl, node);
}

static int NONNULLA(2)
avl_node_recurse(const avl_node_t * node,
                 avl_func_t * func,
                 const void * extra)
{
    int ret;
    if (node != NULL)
    {
        ret = avl_node_recurse(node->child[LEFT], func, extra);
        if (ret)
        {
            return ret;
        }
        ret = (*func)((void *)node->data, (void *)extra);
        if (ret)
        {
            return ret;
        }
        return avl_node_recurse(node->child[RIGHT], func, extra);
    }
    else
    {
        return 0;
    }
}

/* FIXME: Can this be implemented without recursion? */
int NONNULLA(1,2)
avl_recurse(const avl_t * avl,
            avl_func_t * func,
            const void * extra)
{
    return avl_node_recurse(avl->root, func, extra);
}


static int
avl_dp_cmp_func(const void * key,
                const void * data,
                const void * extra)
{
    const avl_dp_data_t * dpdata = data;
    const avl_dp_t *      avldp  = extra;
    return (*avldp->cmp_func)(key, dpdata->data, avldp->extra);
}

static int
avl_dp_destroy_func(void * data, void * extra)
{
    const avl_dp_data_t * dpdata = data;
    const avl_dp_t *      avldp  = extra;
    return (*avldp->destroy_func)(dpdata->data, (void *)avldp->extra);
}

void NONNULLA(1,2)
avl_dp_init(avl_dp_t * avldp,
            avl_dp_cmp_func_t * cmp_func,
            avl_dp_func_t * destroy_func,
            const void * extra)
{
    avl_func_t * dfunc = NULL;
    if (destroy_func != NULL)
    {
        dfunc = &avl_dp_destroy_func;
    }
    avl_init(&avldp->avl, &avl_dp_cmp_func, dfunc, avldp);
    avldp->cmp_func     = cmp_func;
    avldp->destroy_func = destroy_func;
    avldp->extra        = extra;
    return;
}

int NONNULL
avl_dp_deinit(avl_dp_t * avldp)
{
    return avl_deinit(&avldp->avl);
}

int NONNULL
avl_dp_insert(avl_dp_t * avldp,
              const void * key,
              const void * data)
{
    avl_dp_data_t * dpdata = avl_insert(&avldp->avl, key,
                                        sizeof(avl_dp_data_t));
    if (dpdata != NULL)
    {
        dpdata->data = (void *)data;
        return 0;
    }
    else
    {
        return 1;
    }
}

void * NONNULL PURE
avl_dp_search(avl_dp_t * avldp,
              const void * key)
{
    const avl_dp_data_t * dpdata = avl_search(&avldp->avl, key);
    if (dpdata != NULL)
    {
        return dpdata->data;
    }
    else
    {
        return NULL;
    }
}

int NONNULL
avl_dp_delete(avl_dp_t * avldp,
              const void * key)
{
    return avl_delete(&avldp->avl, key);
}

static int NONNULL
avl_dp_recurse_func(void * data, void * extra)
{
    const avl_dp_data_t *         dpdata = data;
    const avl_dp_recurse_data_t * rdata  = extra;
    return (*rdata->func)(dpdata->data, (void *)rdata->extra);
}

int NONNULLA(1,2)
avl_dp_recurse(avl_dp_t * avldp,
               avl_dp_func_t * func,
               const void * extra)
{
    avl_dp_recurse_data_t rdata;
    rdata.func  = func;
    rdata.extra = extra;
    return avl_recurse(&avldp->avl, &avl_dp_recurse_func, &rdata);
}
