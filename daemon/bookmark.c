/*
 * This file is part of mradio.
 *
 * Copyright (C) 2016-2017 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

/* It is possible to compile this file without the ml dependency. Just
 * compile with -DBOOKMARK_NO_ML=1 */
/* #undef BOOKMARK_NO_ML */

/* This must be set before any #includes. */
#define MRADIO_LOG_DOMAIN MRADIO_LOG_DOMAIN_BKMARK

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <ctype.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <mradio/common.h>
#include <mradio/log.h>
#include <mradio/mstr.h>
#include <mradio/read-file.h>
#include <mradio/return.h>

#include <common/log-debug.h>

#include "avl.h"
#include "bookmark.h"

#if !BOOKMARK_NO_ML
# include "ml.h"
#endif /* !BOOKMARK_NO_ML */

struct bookmark_s
{
    char * name;    /* mstr */
    int    is_station; /* 1 */
    char * url;     /* mstr */
};
typedef struct bookmark_s bookmark_t;

struct category_s
{
    char *   name;       /* mstr */
    int      is_station; /* 0 */
    unsigned size;
    avl_t    nodes;
};
typedef struct category_s category_t;

struct header_s
{
    char * name;
    int    is_station;
};
typedef struct header_s header_t;

struct bookmark_list_recurse_s
{
    return_t * ret;
    unsigned   index;
    int        url;
};
typedef struct bookmark_list_recurse_s bookmark_list_recurse_t;

/* Maximum number of parts in a path. */
#define PARTS_MAX 8

enum part_type_e
{
    PART_NONE = 0,
    PART_CATEGORY,

    /*
     * This part type represents the last part of a path, that does not
     * have a trailing slash.
     * Every path has at most one leaf, but need not have a leaf (in
     * which case it ends in a slash).
     * For the commands add and url the leaf must be a station, but for
     * del it need not be.
     * For the command list it must be a category.
     */
    PART_LEAF
};
typedef enum part_type_e part_type_t;
struct part_s
{
    const char * begin;
    part_type_t  type;
    unsigned     len;
};
typedef struct part_s part_t;

struct bookmark_save_s
{
    FILE *   file;
    unsigned indent;
    unsigned count;
};
typedef struct bookmark_save_s bookmark_save_t;

static char *       bookmark_filename = NULL; /* mstr */

static category_t   bookmarks;
static int          bookmarks_saved = 1;

static int          bookmark_should_save_on_change = 0;
static int          bookmark_should_save_on_exit   = 0;

#if !BOOKMARK_NO_ML
static unsigned int bookmark_periodic         = 0;
static int          bookmark_periodic_timeout = -1;
#endif /* !BOOKMARK_NO_ML */

static int
header_cmp_func(const void * key,
                const void * ptr,
                const void * extra)
{
    const char *     name = key;
    const header_t * head = ptr;
    return strcmp(name, head->name);
}

static void
bookmark_destroy(bookmark_t * b)
{
    mstr_unref(b->name);
    mstr_unref(b->url);
}

static void
category_destroy(category_t * c)
{
    mstr_unref(c->name);
    avl_deinit(&c->nodes);
    return;
}

static int
data_destroy(void * ptr, void * extra)
{
    header_t * head = ptr;
    if (head->is_station)
    {
        bookmark_destroy(ptr);
    }
    else
    {
        category_destroy(ptr);
    }
    return 0;
}

static void
category_init(category_t * c, char * name)
{
    c->name       = mstr_ref(name);
    c->is_station = 0;
    c->size       = 0;
    avl_init(&c->nodes, &header_cmp_func, &data_destroy, NULL);
    return;
}

void
bookmark_init(void)
{
    category_init(&bookmarks, NULL);
    return;
}

void
bookmark_deinit(void)
{
    if (bookmark_should_save_on_exit)
    {
        bookmark_save();
    }
    mstr_unref(bookmark_filename);
    bookmark_filename = NULL;
    category_destroy(&bookmarks);
    return;
}

static int
bookmark_changed(void)
{
    bookmarks_saved = 0;
    if (bookmark_should_save_on_change)
    {
        return bookmark_save();
    }
    else
    {
        return 0;
    }
}

static int NONNULL
category_insert_bookmark(category_t * c,
                         const char * key,
                         char * url)
{
    int          retval = 1;
    bookmark_t * b;
    char *       name   = NULL; /* mstr */

    name = mstr(key);
    if (name == NULL)
    {
        goto error;
    }

    b = avl_insert(&c->nodes, key, sizeof(bookmark_t));
    if (b != NULL)
    {
        retval = 0;
        b->name = name;
        b->is_station = 1;
        b->url = mstr_ref(url);
        c->size++;
    }
    else
    {
    error:
        mstr_unref(name);
    }
    return retval;
}

static int NONNULL
category_load_bookmark(category_t * category,
                       const char * name,
                       char * data)
{
    int retval = 1;

    /* keys to extract from data */
    char * url  = NULL;
    char * murl; /* mstr */

    /* temporary variables. */
    char * key;
    char * newline;
    char * value;
    char * valueend;

    while (isspace(*data))
    {
        data++;
    }
    while (*data != '\0') {
        key = data;
        newline = strchrnul(key, '\n');

        /* prepare data for next run in the loop */
        if (*newline == '\0')
        {
            data = newline;
        }
        else
        {
            *newline = '\0';
            data = newline + 1;
        }
        while (isspace(*data))
        {
            data++;
        }

        value = strpbrk(key, " \t");
        if (value == NULL)
        {
            log_error("%s: key %s in station %s lacks a value",
                      bookmark_filename, key, name);
        }
        *value = '\0';
        do {
            value++;
        } while (isspace(*value));
        if (*value == '\0')
        {
            log_error("%s: key %s in station %s lacks a value",
                      bookmark_filename, key, name);
            goto error;
        }

        valueend = newline;
        do {
            valueend--;
        } while (isspace(*valueend));
        valueend++;
        *valueend = '\0';

        if (strcmp(key, "url") == 0)
        {
            url = value;
        }
        else
        {
            log_error("%s: unknown key %s in station %s",
                      bookmark_filename, key, name);
            goto error;
        }
    }

    if (url == NULL)
    {
        log_error("%s: station %s lacks a url",
                  bookmark_filename, name);
        goto error;
    }

    murl = mstr(url);
    if (!murl)
    {
        goto error;
    }
    retval = category_insert_bookmark(category, name, murl);
    mstr_unref(murl);
error:
    return retval;
}

static int
data_init_category(void * key, void * ptr, void * extra)
{
    char * name = mstr(key);
    if (!name)
    {
        return 1;
    }
    category_init(ptr, name);
    mstr_unref(name);
    return 0;
}

static int category_load_block(category_t * category,
                               char * buffer) NONNULL;
static int NONNULL
category_load_category(category_t * category,
                       const char * name,
                       char *       buffer)
{
    int          ret;
    category_t * new;
    char *       copy; /* mstr */

    copy = mstr(name);
    if (!copy)
    {
        return 1;
    }

    new = avl_insert(&category->nodes, name, sizeof(category_t));
    if (new)
    {
        category_init(new, copy);
        ret = category_load_block(new, buffer);
        if (new->size)
        {
            category->size++;
        }
        else
        {
            log_warn("removing empty category \"%s\"", name);
            avl_delete(&category->nodes, name);
        }
    }
    mstr_unref(copy);

    return (new == NULL) || ret;
}

static int NONNULL
category_load_block(category_t * category, char * buffer)
{
    int               retval = 1;
    char *            type;
    char *            name;
    char *            nameend;
    char *            data;
    char *            nested;
    char *            dataend;

    /* skip leading spaces */
    while (isspace(*buffer))
    {
        buffer++;
    }

    while (*buffer != '\0')
    {
        /* first extract information */
        type = buffer;

        name = strpbrk(type, " \t\n");
        if (name == NULL)
        {
            log_error("%s: invalid syntax", bookmark_filename);
            goto error;
        }
        *name = '\0';
        do {
            name++;
        } while (isspace(*name));

        nameend = strchr(name, '{');
        if (nameend == NULL)
        {
            log_error("%s: %s missing opening bracket",
                      bookmark_filename, type);
            goto error;
        }
        else if (name == nameend)
        {
            log_error("%s: %s has no name", bookmark_filename, type);
            goto error;
        }

        data = nameend + 1;

        /* since isspace(*name) = 0 the loop is guaranteed to not go
         * out of bounds */
        do {
            nameend--;
        } while (isspace(*nameend));
        nameend++;
        *nameend = '\0';

        /* find the end of this data section by matching the correct
         * closing bracket */
        nested  = strchr(data, '{');
        dataend = strchr(data, '}');
        while (nested != NULL && dataend != NULL && nested < dataend)
        {
            nested = strchr(nested + 1, '{');
            dataend = strchr(dataend + 1, '}');
        }
        if (dataend == NULL)
        {
            log_error("%s: unterminated %s", bookmark_filename, type);
            goto error;
        }
        *dataend = '\0';

        /* update buffer for next run */
        buffer = dataend;
        do {
            buffer++;
        } while (isspace(*buffer));

        /* handle the extracted information */
        if (strcmp(type, "category") == 0)
        {
            /* create a new category and add stuff to it */
            if (category_load_category(category, name, data))
            {
                goto error;
            }
        }
        else if (strcmp(type, "station") == 0)
        {
            /* extract information (url) about the station from the data
             * and create the station */
            if (category_load_bookmark(category, name, data))
            {
                goto error;
            }
        }
        else
        {
            log_error("%s: unknown type %s", bookmark_filename, type);
            goto error;
        }
    }

    retval = 0;
error:
    return retval;
}

static inline void NONNULL
findent(FILE * file, unsigned int indent)
{
    while (indent--) {
        fputc(' ', file);
    }
    return;
}

void NONNULL
bookmark_file(char * filename)
{
    mstr_unref(bookmark_filename);
    bookmark_filename = mstr_ref(filename);
    return;
}

int
bookmark_load(void)
{
    FILE * file;
    char * buffer;
    int    retval;

    if (bookmark_filename == NULL)
    {
        return 0;
    }

    log_debug("trying to load bookmarks from %s", bookmark_filename);

    /* destroy all existing bookmarks */
    category_destroy(&bookmarks);
    category_init(&bookmarks, NULL);
    bookmarks_saved = 1;

    /* read the bookmark file */
    file = fopen(bookmark_filename, "r");
    if (file == NULL)
    {
        if (errno == ENOENT)
        {
            log_debug("no bookmark file found");
            return 0;
        }
        else
        {
            log_error_errno("could not open bookmark file %s for "
                            "reading",
                            bookmark_filename);
            return 1;
        }
    }
    buffer = read_file(file);
    if (fclose(file))
    {
        log_error_errno("could not close bookmark file %s",
                        bookmark_filename);
    }
    if (buffer == NULL)
    {
        return 1;
    }

    retval = category_load_block(&bookmarks, buffer);
    free(buffer);
    return retval;
}

static void NONNULL
bookmark_save_func(bookmark_save_t * save,
                   bookmark_t * b)
{
    findent(save->file, save->indent);
    fprintf(save->file, "station %s {\n", b->name);
    findent(save->file, save->indent + 1);
    fprintf(save->file, "url %s\n", b->url);
    findent(save->file, save->indent);
    fputs("}\n", save->file);
    return;
}

static int data_save_func(void * ptr, void * extra) NONNULL;
static void NONNULL
category_save_func(bookmark_save_t * save,
                   category_t * c)
{
    findent(save->file, save->indent);
    fprintf(save->file, "category %s {\n", c->name);
    save->indent++;
    avl_recurse(&c->nodes, &data_save_func, save);
    save->indent--;
    findent(save->file, save->indent);
    fputs("}\n", save->file);
    return;
}

static int NONNULL
data_save_func(void * ptr, void * extra)
{
    header_t *        head = ptr;
    bookmark_save_t * save = extra;
    save->count++;
    if (head->is_station)
    {
        bookmark_save_func(save, ptr);
    } else {
        category_save_func(save, ptr);
    }
    return 0;
}

int
bookmark_save(void)
{
    int             retval = 1;
    FILE *          file;
    bookmark_save_t save;
    int             ret;

    if (bookmark_filename != NULL && !bookmarks_saved)
    {
        log_debug("saving bookmarks to %s", bookmark_filename);

        file = fopen(bookmark_filename, "w");
        if (file == NULL)
        {
            log_error_errno("could not open bookmark file %s for "
                            "writing",
                            bookmark_filename);
            goto error;
        }

        save.file   = file;
        save.indent = 0;
        save.count  = 0;

        ret = avl_recurse(&bookmarks.nodes, &data_save_func, &save);
        if (fclose(file))
        {
            log_error_errno("could not close bookmark file %s",
                            bookmark_filename);
        }
        if (ret)
        {
            goto error;
        }

        if (!save.count)
        {
            /* There were no bookmarks to save. Since empty files are
             * disallowed the file must be removed. */
            if (unlink(bookmark_filename))
            {
                log_error_errno("could not remove bookmark file %s",
                                bookmark_filename);
                goto error;
            }
        }
    }

    bookmarks_saved = 1;
    retval = 0;
error:
    return retval;
}

void
bookmark_save_on_change(int save_on_change)
{
    bookmark_should_save_on_change = save_on_change;
    return;
}

void
bookmark_save_on_exit(int save_on_exit)
{
    bookmark_should_save_on_exit = save_on_exit;
    return;
}

#if !BOOKMARK_NO_ML
static unsigned int
bookmark_save_periodic_cb(int timeout)
{
    if (bookmark_periodic)
    {
        /* Only save bookmarks if periodic saving is enabled. */
        log_debug("periodic saving triggered");
        bookmark_save();
    }
    else
    {
        bookmark_periodic_timeout = -1;
    }
    return bookmark_periodic;
}

int
bookmark_save_periodic(unsigned int periodic)
{
    bookmark_periodic = periodic;

    /* If a timeout is already created or no periodic saving is
     * requested, then nothing should be done. */
    if (bookmark_periodic_timeout >= 0 || periodic == 0)
    {
        return 0;
    }

    bookmark_periodic_timeout =
        ml_timeout_new(periodic,
                       &bookmark_save_periodic_cb,
                       NULL);
    if (bookmark_periodic_timeout < 0)
    {
        return 1;
    }

    return 0;
}
#endif /* !BOOKMARK_NO_ML */

static int NONNULL
bookmark_path_to_parts(const char * path,
                       part_t parts[PARTS_MAX])
{
    unsigned     i = 0; /* current part */
    const char * slash;

    memset(parts, 0, PARTS_MAX * sizeof(part_t));

    while (*path == '/')
    {
        path++;
    }

    while ((slash = strchr(path, '/')) != NULL) {
        parts[i].type = PART_CATEGORY;
        parts[i].begin = path;
        parts[i].len = (unsigned)(slash - path);

        /* jump to the next part of the path and skip slashes */
        path = slash + 1;
        while (*path == '/') {
            path++;
        }

        ++i;
        if (i == PARTS_MAX)
        {
            log_error("to many parts in path");
            return 1;
        }
    }

    if (*path != '\0')
    {
        parts[i].type = PART_LEAF;
        parts[i].begin = path;
        parts[i].len = (unsigned)strlen(path);
    }

    return 0;
}

/* Compute the maximum category name length and allocate a buffer large
 * enough to hold the longest one of them. */
static char *
keycopy_alloc(part_t * parts)
{
    size_t   keymax = 0;
    char *   keycopy;
    for (unsigned i = 0; parts[i].type == PART_CATEGORY; ++i)
    {
        if (parts[i].len > keymax)
        {
            keymax = parts[i].len;
        }
    }
    keycopy = malloc(keymax + 1);
    if (!keycopy)
    {
        log_error_errno("could not allocate memory");
    }
    return keycopy;
}

static void
keycopy_fill(char * keycopy, part_t part)
{
    memcpy(keycopy, part.begin, part.len);
    keycopy[part.len] = '\0';
    return;
}

/* Cleans out empty categories. */
static void NONNULL
categories_clean(category_t ** categories, unsigned i)
{
    char * key; /* mstr */
    while (i > 0 && !categories[i]->size)
    {
        key = mstr_ref(categories[i]->name);
        i--;
        if (avl_delete(&categories[i]->nodes, key))
        {
            log_warn("could not delete empty category \"%s\"", key);
        }
        else
        {
            categories[i]->size--;
        }
        mstr_unref(key);
    }
    return;
}

int NONNULL
bookmark_add(const char * path, char * url) /* url is mstr */
{
    part_t       parts[PARTS_MAX];
    category_t * categories[PARTS_MAX];
    unsigned     i;
    char *       keycopy = NULL;

    if (bookmark_path_to_parts(path, parts))
    {
        return 1;
    }

    keycopy = keycopy_alloc(parts);
    if (!keycopy)
    {
        return 1;
    }
    categories[0] = &bookmarks;
    for (i = 0; parts[i].type == PART_CATEGORY; ++i)
    {
        keycopy_fill(keycopy, parts[i]);

        categories[i+1] = avl_insearch(&categories[i]->nodes,
                                       keycopy,
                                       sizeof(category_t),
                                       &data_init_category,
                                       NULL);
        if (!categories[i+1])
        {
            goto error;
        }
        else if (categories[i+1]->is_station)
        {
            log_error("cannot add entry: \"%s\" is a station",
                      keycopy);
            goto error;
        }

        /* If the found category is empty, then it must have been
         * created, since empty categories are disallowed. */
        if (!categories[i+1]->size)
        {
            categories[i]->size++;
        }
    }

    if (parts[i].type != PART_LEAF)
    {
        log_error("cannot add entry: path ended in a category");
        goto error;
    }

    if (category_insert_bookmark(categories[i], parts[i].begin, url))
    {
        goto error;
    }

    free(keycopy);
    return bookmark_changed();
error:
    free(keycopy);
    categories_clean(categories, i);
    return 1;
}

int NONNULL
bookmark_del(const char * path)
{
    part_t            parts[PARTS_MAX];
    category_t *      categories[PARTS_MAX];
    unsigned          i;
    char *            keycopy = NULL;
    const char *      name; /* final name of the bookmark to delete */

    if (bookmark_path_to_parts(path, parts))
    {
        return 1;
    }

    keycopy = keycopy_alloc(parts);
    if (!keycopy)
    {
        return 1;
    }
    categories[0] = &bookmarks;
    for (i = 0; parts[i].type == PART_CATEGORY; ++i)
    {
        keycopy_fill(keycopy, parts[i]);

        categories[i+1] = avl_search(&categories[i]->nodes, keycopy);
        if (!categories[i+1])
        {
            log_error("could not find category \"%s\"", keycopy);
            goto error;
        }
        else if (categories[i+1]->is_station)
        {
            log_error("\"%s\" is a station", keycopy);
            goto error;
        }
    }

    if (parts[i].type == PART_LEAF)
    {
        /* There is a leaf that shall be removed. */
        name = parts[i].begin;
    }
    else
    {
        if (i > 0)
        {
            /*
             * The last category shall be removed.
             * The name of this category is held in the keycopy and the
             * category it shall be removed from is the parent.
             */
            name = keycopy;
            i--;
        }
        else
        {
            /*
             * The caller wants to remove the root.
             * Set name to NULL to signal this.
             */
            name = NULL;
        }
    }
    if (name != NULL)
    {
        /* If name is non-NULL, then there is a bookmark rooted at
         * categories[i] that shall be removed. */
        if (avl_delete(&categories[i]->nodes, parts[i].begin))
        {
            log_error("could not find station at \"%s\"", path);
            goto error;
        }
    }
    else
    {
        /* If name is NULL the caller wants to wipe all bookmarks. */
        category_destroy(&bookmarks);
        category_init(&bookmarks, NULL);
    }
    free(keycopy);
    categories[i]->size--;

    categories_clean(categories, i);
    return bookmark_changed();
error:
    free(keycopy);
    return 1;
}

static header_t * NONNULL
get_header_at_path(const char * path)
{
    part_t       parts[PARTS_MAX];
    category_t * category;
    unsigned     i;
    char *       keycopy;
    header_t   * header = NULL;

    if (bookmark_path_to_parts(path, parts))
    {
        return NULL;
    }
    keycopy = keycopy_alloc(parts);
    if (!keycopy)
    {
        return NULL;
    }

    category = &bookmarks;
    for (i = 0; parts[i].type == PART_CATEGORY; ++i)
    {
        keycopy_fill(keycopy, parts[i]);

        category = avl_search(&category->nodes, keycopy);
        if (!category)
        {
            log_error("could not find category \"%s\"", keycopy);
            goto error;
        }
        else if (category->is_station)
        {
            log_error("\"%s\" is a station", keycopy);
            goto error;
        }
    }

    if (parts[i].type == PART_LEAF)
    {
        header = avl_search(&category->nodes, parts[i].begin);
        if (!header)
        {
            log_error("could not find bookmark \"%s\"", parts[i].begin);
        }
    }
    else
    {
        header = (header_t *)category;
    }

error:
    free(keycopy);
    return header;
}

char * NONNULL
bookmark_url(const char * path)
{
    header_t *   header;
    bookmark_t * bmark;

    header = get_header_at_path(path);
    if (header == NULL)
    {
        return NULL;
    }
    else if (!header->is_station)
    {
        log_error("\"%s\" is not a station", path);
        return NULL;
    }

    bmark = (bookmark_t *)header;
    return mstr_ref(bmark->url);
}

static return_t * bookmark_list_category(const category_t * category,
                                         int url) NONNULL;
static int
bookmark_list_recurse(void * d, void * e)
{
    header_t *                header = d;
    bookmark_list_recurse_t * data   = e;
    bookmark_t *              bookmark;
    category_t *              category;
    return_t *                ret;

    if (header->is_station)
    {
        bookmark = (bookmark_t *)header;
        return_set(data->ret,
                   data->index,
                   RETURN_TYPE_STRING,
                   bookmark->name);
        data->index++;
        if (data->url)
        {
            return_set(data->ret,
                       data->index,
                       RETURN_TYPE_STRING,
                       bookmark->url);
            data->index++;
        }
    }
    else
    {
        category = (category_t *)header;
        if (data->url)
        {
            return_set(data->ret,
                       data->index,
                       RETURN_TYPE_STRING,
                       category->name);
            data->index++;
        }
        ret = bookmark_list_category(category, data->url);
        if (!ret)
        {
            return 1;
        }
        return_set(data->ret, data->index, RETURN_TYPE_ARRAY, ret);
        return_unref(ret);
        data->index++;
    }

    return 0;
}

static return_t * NONNULL
bookmark_list_category(const category_t * category, int url)
{
    bookmark_list_recurse_t data;
    char *                  name;
    unsigned                size;

    if (url)
    {
        size = category->size * 2;
    }
    else
    {
        size = category->size + 1;
    }

    data.ret = return_new(size);
    if (!data.ret)
    {
        return NULL;
    }
    data.index = 0;
    data.url   = url;

    /* When not sending URLs each category begins with its name. */
    if (!url)
    {
        if (category->name)
        {
            name = mstr_ref(category->name);
        }
        else
        {
            name = mstr("root");
            if (!name)
            {
                return_unref(data.ret);
                return NULL;
            }
        }
        return_set(data.ret, 0, RETURN_TYPE_STRING, name);
        mstr_unref(name);
        data.index++;
    }

    if (avl_recurse(&category->nodes,
                    bookmark_list_recurse,
                    &data))
    {
        return_unref(data.ret);
        return NULL;
    }

    return data.ret;
}

/*
 * This command returns differently shaped data depending on the value
 * of url.
 * If url is zero the return has the following structure.
 *   (root-name) | (array1) | (name1) | (array2) | (name2) | ...
 *                    |                    |
 *                    |                    \-> (array2-name) | ...
 *                    \-> (array1-name) | (child1) | (child2) | ...
 * Otherwise it has the following structure.
 *   (array1-name) | (array1) | (name1) | (url1) | ...
 *                      |
 *                      \-> (child1) | (child-url1) | ...
 * The reason for the differences is that in the first case the name of
 * the category is the first element of the its category array, making
 * it length n+1 where n is the number of children of that category.
 * In the second case each category has 2n elements.
 * Both n+1 and 2n are easily computed.
 * Furthermore both these data model have good efficiency, compared to
 * alternative data models.
 */
return_t *
bookmark_list(const char * path, int url)
{
    const category_t * category;

    if (path)
    {
        category = (category_t *)get_header_at_path(path);
        if (!category)
        {
            return NULL;
        }
        else if (category->is_station)
        {
            log_error("\"%s\" is not a category", path);
            return NULL;
        }
    }
    else
    {
        category = &bookmarks;
    }

    return bookmark_list_category(category, url);
}
