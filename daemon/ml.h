/*
 * This file is part of mradio.
 *
 * Copyright (C) 2014-2016 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#ifndef ML_H
# define ML_H 1

# if HAVE_CONFIG_H
#  include <config.h>
# endif /* HAVE_CONFIG_H */

# include <mradio/common.h>

/* This is the function type that is used for all callbacks from the
 * main loop, both tied to ml_fds, in which case the int argument is the
 * ml_fd, and ml_timeouts, in which case the int argument is the
 * timeout. The return value is ignored for ml_fds, but for ml_timeouts
 * the return value is the number of seconds to wait before the next
 * trigger of the timeout or 0 if the timeout should be removed. */
typedef unsigned int (ml_cb_t)(const int);

/* This function creates a new main loop file descriptor from the file
 * descriptor fd. On success it returns a non-negative main loop file
 * descriptor. On error a negative integer is returned. */
int    ml_fd_new          (const int fd, void * const data);
int    ml_fd_destroy      (const int ml_fd);

void * ml_fd_get_data     (const int ml_fd) PURE;
int    ml_fd_get_fd       (const int ml_fd) PURE;

void   ml_fd_error_cb     (const int ml_fd, ml_cb_t * const cb);
void   ml_fd_hangup_cb    (const int ml_fd, ml_cb_t * const cb);
void   ml_fd_read_cb      (const int ml_fd, ml_cb_t * const cb);
void   ml_fd_write_cb     (const int ml_fd, ml_cb_t * const cb);

/* This function creates a new main loop timeout. The expiry argument
 * may not be zero. The timeout will be triggered after EXPIRY seconds.
 * If periodic re-triggering is wanted the callback should then return
 * a positive next expiry. */
int    ml_timeout_new     (unsigned int expiry,
                           ml_cb_t * const cb,
                           void * const data);
void * ml_timeout_get_data(const int timeout);

/* Note. There is no such thing as ml_timeout_destroy since such a
 * function would have to traverse the entire heap to find the timeout
 * and remove it, in effect making it an O(n) operation which is not
 * acceptable. Instead timeouts that should be detached, should return
 * 0 from the callback so that the timeout is not re-added. */

int    ml_run             (void);
void   ml_free            (void);

#endif /* !ML_H */
