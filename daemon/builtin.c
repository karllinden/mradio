/*
 * This file is part of mradio.
 *
 * Copyright (C) 2015-2016 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#define MRADIO_LOG_DOMAIN MRADIO_LOG_DOMAIN_DAEMON

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <stdlib.h>
#include <string.h>

#include <mradio/common.h>
#include <mradio/module.h>

#include "builtin.h"
#include "builtins.h"

static int NONNULL
builtin_compare(const void * const m1, const void * const m2)
{
    const builtin_t * const b1 = m1;
    const builtin_t * const b2 = m2;
    return strcmp(b1->name, b2->name);
}

const module_info_t * NONNULL
builtin_load(const char * const name)
{
    builtin_t key;
    builtin_t * builtin;
    key.name = name;
    builtin = bsearch(&key, builtins, builtins_size, sizeof(builtin_t),
                      &builtin_compare);
    if (builtin != NULL) {
        return builtin->info;
    } else {
        return NULL;
    }
}

int NONNULL
builtin_foreach(builtin_func_t * func)
{
    int ret;
    for (size_t i = 0; i < builtins_size; ++i) {
        ret = (*func)(builtins[i].name, builtins[i].info);
        if (ret) {
            return ret;
        }
    }
    return 0;
}
