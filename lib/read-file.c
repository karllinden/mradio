/*
 * This file is part of mradio.
 *
 * Copyright (C) 2014-2016 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#define MRADIO_LOG_DOMAIN MRADIO_LOG_DOMAIN_RF

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <stdio.h>
#include <stdlib.h>

#include <mradio/common.h>
#include <mradio/log.h>
#include <mradio/read-file.h>

char * NONNULL
read_file(FILE * const file)
{
    char * buffer = NULL;

    if (fseek(file, 0L, SEEK_END)) {
        log_fatal_errno("fseek failed");
        goto error;
    }

    const size_t size = (size_t)ftell(file);
    if (size == (size_t)(-1)) {
        log_fatal_errno("ftell failed");
        goto error;
    }

    if (fseek(file, 0L, SEEK_SET)) {
        log_fatal_errno("fseek failed");
        goto error;
    }

    buffer = malloc(size + 1);
    if (buffer == NULL) {
        log_fatal_errno("could not allocate memory");
        goto error;
    }

    const size_t r = fread(buffer, sizeof(char), size, file);
    if (r != size && ferror(file)) {
        log_fatal("error reading configuration file");
        goto error;
    }

    buffer[size] = '\0';

    return buffer;
error:
    free(buffer);
    return NULL;
}
