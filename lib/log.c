/*
 * This file is part of mradio.
 *
 * Copyright (C) 2014-2017 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#define MRADIO_LOG_DOMAIN MRADIO_LOG_DOMAIN_LOG

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <assert.h>
#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <syslog.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>

#include <mradio/common.h>
#include <mradio/create-parent.h>
#include <mradio/log.h>

#include <common/call.h>
#include <common/massert.h>

/* flags to log_print_helper */
#define LOG_DATE 0x1
#define LOG_NAME 0x2

#define LOG_MODE (S_IRUSR | S_IWUSR)

struct log_redirect_s {
    pthread_t    thread;
    log_func_t * func;
    void *       data;
};
typedef struct log_redirect_s log_redirect_t;

struct log_dom_s
{
    const char * name;
    log_level_t  level;
};
typedef struct log_dom_s log_dom_t;

static log_dom_t log_domains[LOG_DOMAIN_ALL] =
{
    {"avl",    LOG_LEVEL_INFO},
    {"bkmark", LOG_LEVEL_INFO},
    {"client", LOG_LEVEL_INFO},
    {"common", LOG_LEVEL_INFO},
    {"daemon", LOG_LEVEL_INFO},
    {"info",   LOG_LEVEL_INFO},
    {"log",    LOG_LEVEL_INFO},
    {"ml",     LOG_LEVEL_INFO},
    {"mstr",   LOG_LEVEL_INFO},
    {"nblock", LOG_LEVEL_INFO},
    {"pid",    LOG_LEVEL_INFO},
    {"return", LOG_LEVEL_INFO},
    {"rf",     LOG_LEVEL_INFO},
    {"rng",    LOG_LEVEL_INFO},
    {"run",    LOG_LEVEL_INFO},
    {"serv",   LOG_LEVEL_INFO},
    {"sock",   LOG_LEVEL_INFO},
    {"test",   LOG_LEVEL_INFO},
    {"uid",    LOG_LEVEL_INFO},
};
#define LOG_DOMAIN_LEN_MAX 6

static const char *     log_name      = NULL;
static int              log_domain    = 0;
static int              log_quiet     = 0;

static char *           log_filename = NULL;
static FILE *           log_file = NULL;
static dev_t            log_dev;
static ino_t            log_ino;

static bool             log_do_syslog = false;

static pthread_once_t   log_once = PTHREAD_ONCE_INIT;
static pthread_mutex_t  log_mutex;

static log_redirect_t * log_redirect_list = NULL;
static size_t           log_redirect_len  = 0;
static size_t           log_redirect_size = 0;

static void
log_mutex_destroy(void)
{
    pthread_mutex_destroy(&log_mutex);
    return;
}

static void
log_mutex_init(void)
{
    /* Do not call log_* functions here, since it will cause great
     * trouble when log_lock is called. Thus perror should be used
     * instead. Abort on severe errors and let non-fatal errors be,
     * but do notice about them. */

    pthread_mutexattr_t attr;
    if (pthread_mutexattr_init(&attr)) {
        perror("pthread_mutexattr_init");
        abort();
    }
    pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_RECURSIVE);
    if (pthread_mutex_init(&log_mutex, &attr)) {
        perror("pthread_mutex_init");
        abort();
    }
    pthread_mutexattr_destroy(&attr);
    if (atexit(&log_mutex_destroy)) {
        perror("atexit");
    }
    return;
}

static inline void
log_lock(void)
{
    pthread_once(&log_once, &log_mutex_init);
    pthread_mutex_lock(&log_mutex);
    return;
}

static inline void
log_unlock(void)
{
    pthread_mutex_unlock(&log_mutex);
    return;
}

/* not thread-safe */
static void
log_redirect_free(void)
{
    free(log_redirect_list);
    log_redirect_list = NULL;
    log_redirect_len  = 0;
    log_redirect_size = 0;
    return;
}

/* the lock must be held when this function is called */
static int NONNULL
log_redirect_add(const pthread_t thread,
                 log_func_t * const func,
                 void * data)
{
    if (log_redirect_len == log_redirect_size) {
        size_t new_size = log_redirect_size + 1;
        log_redirect_t * new;
        new = realloc(log_redirect_list,
                      new_size * sizeof(log_redirect_t));
        if (new == NULL) {
            log_error_errno("could not allocate memory");
            return 1;
        }

        /* this is the first call to this function, so add the atexit
         * function that will do all clean up */
        if (log_redirect_list == NULL) {
            if (atexit(&log_redirect_free)) {
                log_error_errno("could not register atexit function");
            }
        }

        log_redirect_list = new;
        log_redirect_size = new_size;
    }

    log_redirect_t * redir = log_redirect_list + log_redirect_len;
    redir->thread = thread;
    redir->func   = func;
    redir->data   = data;

    log_redirect_len++;

    return 0;
}

/* the lock must be held when this function is called */
static void
log_redirect_del(const pthread_t thread)
{
    for (size_t i = 0; i < log_redirect_len; ++i) {
        if (pthread_equal(thread, log_redirect_list[i].thread)) {
            log_redirect_len--;
            if (i != log_redirect_len) {
                memcpy(log_redirect_list + i,
                       log_redirect_list + log_redirect_len,
                       sizeof(log_redirect_t));
            }
            return;
        }
    }
    return;
}

/* the lock must be held when this function is called */
static void
log_redirect_get(const pthread_t thread,
                 log_func_t ** funcp,
                 void ** datap)
{
    for (size_t i = 0; i < log_redirect_len; ++i) {
        if (pthread_equal(thread, log_redirect_list[i].thread)) {
            *funcp = log_redirect_list[i].func;
            *datap = log_redirect_list[i].data;
            return;
        }
    }
    *funcp = NULL;
    *datap = NULL;
    return;
}

int
log_redirect(log_func_t * func, void * data)
{
    const pthread_t thread = pthread_self();
    int retval;
    log_lock();
    if (func != NULL) {
        retval = log_redirect_add(thread, func, data);
    } else {
        massert(data == NULL);
        log_redirect_del(thread);
        retval = 0;
    }
    log_unlock();
    return retval;
}

void
log_syslog(int do_syslog)
{
    log_lock();
    massert(log_name != NULL);
    if (do_syslog && !log_do_syslog) {
        log_do_syslog = true;
        openlog(log_name, LOG_PID, LOG_DAEMON);
    } else if (!do_syslog && log_do_syslog) {
        log_do_syslog = false;
        closelog();
    }
    log_unlock();
    return;
}

void
log_set_level(log_domain_t domain, log_level_t level)
{
    massert(domain <= LOG_DOMAIN_ALL);
    massert(level != LOG_LEVEL_NONE);
    log_lock();
    if (domain == LOG_DOMAIN_ALL)
    {
        for (unsigned int i = 0; i < LOG_DOMAIN_ALL; ++i)
        {
            log_domains[i].level = level;
        }
    }
    else
    {
        log_domains[domain].level = level;
    }
    log_unlock();
    return;
}

void
log_set_name(const char * name)
{
    log_lock();
    log_name = name;
    log_unlock();
    return;
}

void
log_set_domain(int domain)
{
    log_lock();
    log_domain = domain;
    log_unlock();
    return;
}

void
log_set_quiet(int quiet)
{
    log_lock();
    log_quiet = quiet;
    log_unlock();
    return;
}

log_level_t NONNULL PURE
log_level_from_string(const char * string)
{
    if (strcmp(string, "fatal") == 0) {
        return LOG_LEVEL_FATAL;
    } else if (strcmp(string, "error") == 0) {
        return LOG_LEVEL_ERROR;
    } else if (strcmp(string, "warn") == 0) {
        return LOG_LEVEL_WARN;
    } else if (strcmp(string, "info") == 0) {
        return LOG_LEVEL_INFO;
    } else if (strcmp(string, "debug") == 0) {
        return LOG_LEVEL_DEBUG;
    } else {
        return LOG_LEVEL_NONE;
    }
}

static const char * CONST RETURNS_NONNULL
log_level_to_string(log_level_t level)
{
    switch (level) {
        case LOG_LEVEL_FATAL:
            return "fatal";
        case LOG_LEVEL_ERROR:
            return "error";
        case LOG_LEVEL_WARN:
            return "warn ";
        case LOG_LEVEL_INFO:
            return "info ";
        case LOG_LEVEL_DEBUG:
            return "debug";
        default:
            return "unknown";
    }
}

static int NONNULL PURE
log_dom_compar(const void * key, const void * ptr)
{
    const log_dom_t * domain = ptr;
    return strcmp(key, domain->name);
}

log_domain_t NONNULL PURE
log_domain_from_string(const char * string)
{
    const log_dom_t * domain;
    domain = bsearch(string, log_domains, LOG_DOMAIN_ALL,
                     sizeof(log_dom_t), &log_dom_compar);
    if (domain == NULL)
    {
        log_warn("unknown log domain %s", string);
        return LOG_DOMAIN_ALL;
    }
    else
    {
        return (log_domain_t)(domain - log_domains);
    }
}

static const char * CONST
log_domain_to_string(log_domain_t domain)
{
    massert(domain < LOG_DOMAIN_ALL);
    return log_domains[domain].name;
}

static inline int
log_should_log(log_domain_t domain, log_level_t level)
{
    int ret;

    /* Ordinary assertions here to avoid the infinite recursion:
     *  log_should_log -> massert -> log_fatal -> log_print_real ->
     *                 -> log_should_log.
     */
    assert(domain < LOG_DOMAIN_ALL);
    assert(level != LOG_LEVEL_NONE);

    log_lock();
    ret = (level <= log_domains[domain].level);
    log_unlock();

    return ret;
}

/* the lock must be held when this function is called */
static void
log_file_close(void)
{
    if (log_filename != NULL) {
        free(log_filename);
        log_filename = NULL;
    }

    if (log_file != NULL) {
        if (fclose(log_file)) {
            log_error_errno("could not close logfile %s", log_filename);
        }
        log_file = NULL;
    }

    return;
}

/* the lock must be held when this function is called */
static int NONNULL
log_file_open(void)
{
    massert(log_file == NULL);

    int fd = -1;
    int retval = 0;

    if (create_parent(log_filename)) {
        goto error;
    }

    fd = open(log_filename, O_APPEND | O_CREAT | O_WRONLY, LOG_MODE);
    if (fd == -1) {
        log_fatal_errno("could not open logfile %s for writing",
                        log_filename);
        goto error;
    }

    struct stat buf;
    if (fstat(fd, &buf)) {
        log_fatal_errno("could not stat logfile descriptor");
        goto error;
    }
    log_dev = buf.st_dev;
    log_ino = buf.st_ino;

    log_file = fdopen(fd, "a");
    if (log_file == NULL) {
        log_fatal_errno("could not open logfile %s for writing",
                          log_filename);
        goto error;
    }

    if (false) {
    error:
        COLD;
        retval = 1;
        if (fd >= 0 && close(fd)) {
            log_error_errno("could not close logfile %s", log_filename);
        }
    }

    return retval;
}

/* the lock must be held when this function is called */
static void
log_file_check(void)
{
    struct stat buf;
    if (stat(log_filename, &buf)) {
        /* error */
        if (errno == ENOENT) {
            goto reopen;
        } else {
            log_error_errno("could not stat logfile");
            return;
        }
    } else {
        /* success */
        if (buf.st_dev != log_dev || buf.st_ino != log_ino) {
            goto reopen;
        }
    }

    if (false) {
    reopen:
        log_close();
        log_file_open();
    }

    return;
}

int NONNULL
log_open(const char * logfile)
{
    int retval = 1;

    log_lock();
    log_file_close();

    log_filename = strdup(logfile);
    if (log_filename == NULL) {
        log_fatal_errno("could not allocate memory");
        goto error;
    }

    retval = log_file_open();
error:
    log_unlock();
    return retval;
}

void
log_close(void)
{
    log_lock();

    log_file_close();
    if (log_do_syslog) {
        closelog();
        log_do_syslog = false;
    }

    log_unlock();
    return;
}

int NONNULL
log_parse_level(const char * string)
{
    log_domain_t domain;
    log_level_t level;

    char * copy;
    char * levstr;
    char * domstr;
    char * next;

    copy = strdup(string);
    if (copy == NULL) {
        log_fatal_errno("could not allocate memory");
        return 1;
    }

    next = copy;
    while (next != NULL) {
        levstr = next;
        next = strpbrk(levstr, ", ");
        if (next != NULL) {
            *next = '\0';
            next++;
        }
        domstr = levstr;
        levstr = strchr(levstr, ':');
        if (levstr == NULL) {
            levstr = domstr;
            domstr = NULL;
        } else {
            *levstr = '\0';
            levstr++;
        }

        if (domstr != NULL) {
            domain = log_domain_from_string(domstr);
            if (domain == LOG_DOMAIN_ALL) {
                return 1;
            }
        } else {
            domain = LOG_DOMAIN_ALL;
        }
        level = log_level_from_string(levstr);
        if (level == LOG_LEVEL_NONE) {
            log_fatal("%s is not a valid log level", levstr);
            return 1;
        }
        log_set_level(domain, level);
    }

    free(copy);

    return 0;
}

static void NONNULLA(1,6)
log_print_helper(FILE * file,
                 int flags,
                 log_domain_t domain,
                 log_level_t level,
                 const char * err,
                 const char * fmt,
                 va_list ap)
{
    const char * levstr = log_level_to_string(level);

    if (flags & LOG_DATE) {
        /* Jan 01 00:00:00 */
        time_t t;
        const struct tm * tm;
        char datetime[16];
        size_t ret UNUSED;

        t = time(NULL);
        tm = localtime(&t);
        massert(tm != NULL);
        ret = strftime(datetime, 16, "%b %d %H:%M:%S", tm);
        massert(ret == 15);
        fputs(datetime, file);
        fputc(' ', file);
    }

    if ((flags & LOG_NAME) && log_name != NULL) {
        fputs(log_name, file);
        fputs(": ", file);
    }

    fputs(levstr, file);
    fputs(": ", file);

    if (log_domain) {
        massert(domain != LOG_DOMAIN_ALL);
        const char * domstr = log_domain_to_string(domain);
        fputc('(', file);
        fputs(domstr, file);
        fputs(") ", file);
        for (size_t i = strlen(domstr); i < LOG_DOMAIN_LEN_MAX; ++i) {
            fputc(' ', file);
        }
    }

    vfprintf(file, fmt, ap);

    if (err != NULL) {
        fprintf(file, ": %c%s\n", tolower(err[0]), err+1);
    } else {
        fputc('\n', file);
    }

    return;
}

void NONNULL PRINTF(4,5)
log_print_real(int has_errno,
               log_domain_t domain,
               log_level_t level,
               const char * fmt, ...)
{
    pthread_t    thread;
    log_func_t * func;
    void *       data;
    va_list      ap;

    log_lock();

    /* redirect logging if requested */
    thread = pthread_self();
    log_redirect_get(thread, &func, &data);
    if (func != NULL) {
        log_redirect_del(thread); /* avoid infinite recursion */
        va_start(ap, fmt);
        (*func)(has_errno, domain, level, fmt, ap, data);
        va_end(ap);
        log_redirect_add(thread, func, data); /* reset to prev state */
    }

    if (!log_should_log(domain, level)) {
        goto end;
    }

    const char * err = NULL;
    if (has_errno) {
        err = strerror(errno);
    }

    /* Use lockf here if possible to avoid scrambling log messages
     * from different processes (server, client). */
    if (!log_quiet) {
        va_start(ap, fmt);
#if HAVE_LOCKF
        CALL(lockf, STDERR_FILENO, F_LOCK, 0);
#endif /* HAVE_LOCKF */
        log_print_helper(stderr, LOG_NAME, domain, level, err, fmt, ap);
#if HAVE_LOCKF
        CALL(lockf, STDERR_FILENO, F_ULOCK, 0);
#endif /* HAVE_LOCKF */
        va_end(ap);
    }

    if (log_file != NULL) {
        log_file_check();

        va_start(ap, fmt);
        log_print_helper(log_file, LOG_DATE, domain, level, err, fmt,
                         ap);
        va_end(ap);

        /* this is silent since calling log_error_errno could cause an
         * infinite loop */
        fflush(log_file);
    }

    if (log_do_syslog) {
        int priority;
        switch (level) {
            case LOG_LEVEL_FATAL:
                priority = LOG_CRIT;
                break;
            case LOG_LEVEL_ERROR:
                priority = LOG_ERR;
                break;
            case LOG_LEVEL_WARN:
                priority = LOG_WARNING;
                break;
            case LOG_LEVEL_INFO:
                priority = LOG_INFO;
                break;
            case LOG_LEVEL_DEBUG:
                priority = LOG_DEBUG;
                break;
            case LOG_LEVEL_NONE:
                goto end;
        }

        va_start(ap, fmt);
        if (has_errno) {
            const size_t fmt_len = strlen(fmt);
            char new_fmt[fmt_len + 6];
            memcpy(new_fmt, fmt, fmt_len);
            memcpy(new_fmt + fmt_len, ": %m\n", 6);

            vsyslog(priority, new_fmt, ap);
        } else {
            vsyslog(priority, fmt, ap);
        }
        va_end(ap);
    }

end:
    log_unlock();
    return;
}

int
log_getenv(void)
{
    char * loglevel;

    loglevel = getenv("MRADIO_LOGLEVEL");
    if (loglevel != NULL && log_parse_level(loglevel))
    {
        return 1;
    }

    if (getenv("MRADIO_LOGDOMAIN"))
    {
        log_set_domain(1);
    }

    if (getenv("MRADIO_QUIET"))
    {
        log_set_quiet(1);
    }

    return 0;
}
