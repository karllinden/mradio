/*
 * This file is part of mradio.
 *
 * Copyright (C) 2016 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#define MRADIO_LOG_DOMAIN MRADIO_LOG_DOMAIN_INFO

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <ctype.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>

#include <mradio/common.h>
#include <mradio/info.h>
#include <mradio/log.h>
#include <mradio/mstr.h>
#include <mradio/user.h>

#include <common/massert.h>

#define INFO_TYPE_DEFAULT INFO_TYPE_UNIX

static const char * info_types[INFO_TYPE_LEN] = {
    "inet",
    "unix",
};

static const char * info_inet_props[] = {
    "addr",
    "port",
    NULL
};

static const char * info_unix_props[] = {
    "unix",
    NULL
};

static const char ** info_type_properties[INFO_TYPE_LEN] = {
    info_inet_props,
    info_unix_props
};

struct info_prop_off_s {
    const char * prop;
    size_t       off; /* offset */
};
typedef struct info_prop_off_s info_prop_off_t;
const info_prop_off_t info_prop_offs[] = {
    {"addr", offsetof(info_t, addr)},
    {"port", offsetof(info_t, port)},
    {"unix", offsetof(info_t, unix)},
    {NULL, 0},
};

#if DEBUG
static void NONNULL
info_debug_access(const info_t * info,
                  const char * prop)
{
    massert(info->type < INFO_TYPE_LEN);

    /* Determine which type prop corresponds to. */
    info_type_t type;
    for (type = 0; type < INFO_TYPE_LEN; ++type)
    {
        for (const char ** propp = info_properties(type);
             *propp != NULL;
             propp++)
        {
            if (strcmp(prop, *propp) == 0)
            {
                goto found;
            }
        }
    }

    /* Not type found for this property. => Invalid property. */
    log_error("invalid property %s", prop);
    abort();

found:
    if (type != info->type)
    {
        log_error("type mismatch: %s if of type %s, but type %s is "
                  "selected", prop, info_strtype(type),
                  info_strtype(info->type));
        abort();
    }
    return;
}
#else
# define info_debug_access(info, prop) /* empty */
#endif /* DEBUG */

const char * RETURNS_NONNULL
info_strtype(info_type_t type)
{
    if (type < INFO_TYPE_LEN)
    {
        return info_types[type];
    }
    else if (type == INFO_TYPE_NONE)
    {
        return "none";
    }
    else
    {
        return "invalid";
    }
}

const char ** NONNULL
info_properties(info_type_t type)
{
    if (type < INFO_TYPE_LEN)
    {
        return info_type_properties[type];
    }
    else
    {
        return NULL;
    }
}

char * NONNULL
info_default(const char * prop)
{
    uid_t  uid;

    if (strcmp(prop, "addr") == 0)
    {
        return mstr("127.0.0.1");
    }
    else if (strcmp(prop, "port") == 0)
    {
        return mstr("14752");
    }
    else if (strcmp(prop, "unix") == 0)
    {
        uid = getuid();
        if (uid == 0)
        {
            return mstr("/var/lib/mradio/socket");
        }
        else
        {
            return userfile("socket");
        }
    }
    else
    {
        log_error("invalid property %s", prop);
        return NULL;
    }
}

static char ** NONNULL RETURNS_NONNULL
info_prop_pointer(const info_t * info,
                  const char * prop)
{
    info_debug_access(info, prop);
    for (const info_prop_off_t * off = info_prop_offs;
         off->prop != NULL;
         ++off)
    {
        if (strcmp(prop, off->prop) == 0)
        {
            return (char **)((char *)info + off->off);
        }
    }
    log_fatal("invalid property %s to info_prop_pointer", prop);
    abort();
}

char * NONNULL
info_get_prop(const info_t * info,
              const char * prop)
{
    char ** valuep = info_prop_pointer(info, prop);
    mstr_ref(*valuep);
    return *valuep;
}

static void NONNULLA(1,2)
info_set_prop(info_t * info,
              const char * prop,
              char * value)
{
    char ** valuep = info_prop_pointer(info, prop);
    *valuep = value;
    return;
}

static void NONNULLA(1,2)
info_reset_prop(info_t * info,
                const char * prop,
                char * value)
{
    char ** valuep = info_prop_pointer(info, prop);
    mstr_unref(*valuep);
    *valuep = value;
    return;
}

/* Copying wrapper around info_prop_reset. */
static int NONNULLA(1,2)
info_reset(info_t * info,
           const char * prop,
           const char * value)
{
    char * copy = NULL;
    if (value != NULL)
    {
        copy = mstr(value);
        if (copy == NULL)
        {
            return 1;
        }
    }
    info_reset_prop(info, prop, copy);
    return 0;
}

/* Clears type specific data.
 * info must be initialized */
static void NONNULL
info_clear_type(info_t * info)
{
    if (info->type != INFO_TYPE_NONE)
    {
        for (const char ** propp = info_properties(info->type);
             *propp != NULL;
             ++propp)
        {
            /* use reset here to free any allocated data */
            info_reset_prop(info, *propp, NULL);
        }
        info->type = INFO_TYPE_NONE;
    }
    return;
}

static void NONNULL
info_set_type(info_t * info,
              info_type_t type)
{
    massert(type < INFO_TYPE_LEN);
    if (type != info->type)
    {
        info_clear_type(info);
        info->type = type;
        for (const char ** propp = info_properties(type);
             *propp != NULL;
             propp++)
        {
            /* Type is cleared so only use set here, no reset since
             * it frees old strings. This could result in freeing
             * unitialized pointers. */
            info_set_prop(info, *propp, NULL);
        }
    }
    return;
}

void NONNULL
info_init(info_t * info)
{
    info->type     = INFO_TYPE_NONE;
    info->name     = NULL;
    info->password = NULL;
    info_set_type(info, INFO_TYPE_DEFAULT);
    return;
}

void NONNULL
info_deinit(info_t * info)
{
    info_clear_type(info);
    mstr_unref(info->name);
    mstr_unref(info->password);
    return;
}

info_type_t NONNULL
info_get_type(const info_t * info)
{
    return info->type;
}

static int NONNULLA(1)
info_set_memb(char ** membp,
              const char * value)
{
    mstr_unref(*membp);
    *membp = NULL;
    if (value != NULL)
    {
        *membp = mstr(value);
        if (*membp == NULL)
        {
            return 1;
        }
    }
    return 0;
}

int NONNULLA(1)
info_set_name(info_t * info,
              const char * name)
{
    return info_set_memb(&info->name, name);
}

char * NONNULL
info_get_name(const info_t * info)
{
    mstr_ref(info->name);
    return info->name;
}

int NONNULLA(1)
info_set_password(info_t * info,
                  const char * password)
{
    return info_set_memb(&info->password, password);
}

char * NONNULL
info_get_password(const info_t * info)
{
    mstr_ref(info->password);
    return info->password;
}

static info_type_t NONNULL PURE
info_type_from_string(const char * str)
{
    for (info_type_t t = 0; t < INFO_TYPE_LEN; t++) {
        if (strcmp(str, info_types[t]) == 0) {
            return t;
        }
    }
    return INFO_TYPE_NONE;
}

static char * NONNULL
info_envname(const char * key)
{
    size_t len;
    char * name;

    len = strlen(key);
    name = malloc(len + 16); /* 16 = strlen("MRADIOD_SERVER_") + 1 */
    if (name == NULL)
    {
        log_error_errno("could not allocate memory");
        return NULL;
    }

    memcpy(name, "MRADIOD_SERVER_", 15);
    for (size_t i = 0; i <= len; ++i)
    {
        name[i + 15] = (char)toupper(key[i]);
    }

    return name;
}

static info_type_t
info_getenv_type(void)
{
    info_type_t  type = INFO_TYPE_NONE;
    const char * strtype;
    char *       name;
    const char * value;

    strtype = getenv("MRADIOD_SERVER_TYPE");
    if (strtype != NULL)
    {
        type = info_type_from_string(strtype);
        if (type == INFO_TYPE_NONE)
        {
            log_warn("invalid value %s for MRADIOD_SERVER_TYPE",
                     strtype);
        }
    }

    /* Try to detect type by investigating if there are environment
     * variables set. */
    if (type == INFO_TYPE_NONE)
    {
        for (info_type_t t = 0; t < INFO_TYPE_LEN; ++t)
        {
            for (const char ** propp = info_properties(t);
                 *propp != NULL;
                 ++propp)
            {
                name = info_envname(*propp);
                if (name == NULL)
                {
                    return INFO_TYPE_NONE;
                }
                value = getenv(name);
                free(name);
                if (value != NULL)
                {
                    return t;
                }
            }
        }
        return INFO_TYPE_NONE;
    }

    return type;
}

/* Loads info from environment. */
int NONNULL
info_getenv(info_t * info)
{
    info_type_t  type;
    char *       varname;
    const char * value;
    const char * name;
    const char * password;

    type = info_getenv_type();
    if (type == INFO_TYPE_NONE)
    {
        return 0;
    }
    info_set_type(info, type);

    for (const char ** propp = info_properties(type);
         *propp != NULL;
         ++propp)
    {
        varname = info_envname(*propp);
        if (varname == NULL)
        {
            return 1;
        }
        value = getenv(varname);
        free(varname);

        if (value != NULL && info_reset(info, *propp, value))
        {
            return 1;
        }
    }

    name = getenv("MRADIOD_SERVER_NAME");
    if (name != NULL && info_set_name(info, name))
    {
        return 1;
    }

    password = getenv("MRADIOD_SERVER_PASSWORD");
    if (password != NULL && info_set_password(info, password))
    {
        return 1;
    }

    return 0;
}

/* Exports info to environment. */
int NONNULL
info_setenv(info_t * info)
{
    char * varname;
    char * value;
    char * name;
    char * password;

    if (info->type == INFO_TYPE_NONE)
    {
        return 0;
    }

    if (setenv("MRADIOD_SERVER_TYPE", info_strtype(info->type), 1))
    {
        log_error_errno("could not set MRADIO_SERVER_TYPE");
        return 1;
    }

    for (const char ** propp = info_properties(info->type);
         *propp != NULL;
         ++propp)
    {
        varname = info_envname(*propp);
        if (varname == NULL)
        {
            return 1;
        }
        value = info_get_prop(info, *propp);
        if (value != NULL && setenv(varname, value, 1))
        {
            log_error_errno("could not set %s", varname);
            return 1;
        }
        mstr_unref(value);
        free(varname);
    }

    name = info_get_name(info);
    if (name != NULL)
    {
        if (setenv("MRADIOD_SERVER_NAME", name, 1))
        {
            log_error_errno("could not set MRADIOD_SERVER_NAME");
            return 1;
        }
    }
    mstr_unref(name);

    password = info_get_password(info);
    if (password != NULL)
    {
        if (setenv("MRADIOD_SERVER_PASSWORD", password, 1))
        {
            log_error_errno("could not set MRADIO_SERVER_PASSWORD");
            return 1;
        }
    }
    mstr_unref(password);

    return 0;
}

int NONNULLA(1)
info_inet(info_t * info,
          const char * addr,
          const char * port)
{
    info_set_type(info, INFO_TYPE_INET);
    return info_reset(info, "addr", addr) ||
           info_reset(info, "port", port);
}

int NONNULLA(1)
info_unix(info_t * info,
          const char * unix)
{
    info_set_type(info, INFO_TYPE_UNIX);
    return info_reset(info, "unix", unix);
}
