/*
 * This file is part of mradio.
 *
 * Copyright (C) 2016-2017 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#define MRADIO_LOG_DOMAIN MRADIO_LOG_DOMAIN_CLIENT

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <stdlib.h>

#include <mradio/common.h>
#include <mradio/info.h>
#include <mradio/log.h>
#include <mradio/return.h>

#include "client.h"
#include "pipe-client.h"
#include "sock-client.h"

struct client_s
{
    client_type_t type;
    char data[];
};

client_t * NONNULL
client_open(const info_t * info)
{
    client_t *  client = NULL;
    info_type_t type;

    type = info_get_type(info);
    switch (type)
    {
        case INFO_TYPE_INET:
        case INFO_TYPE_UNIX:
            client = sock_client_open(info);
            break;
        default:
            log_error("unhandled info type %s", info_strtype(type));
            break;
    }

    return client;
}

void NONNULL
client_close(client_t * client)
{
    switch (client->type)
    {
        case CLIENT_PIPE:
            pipe_client_close(client);
            break;
        default:
            log_fatal("invalid client type");
            abort();
    }
    return;
}

int32_t NONNULL
client_command(client_t * client,
               uint8_t argc,
               const char ** argv,
               return_t ** retp)
{
    switch (client->type)
    {
        case CLIENT_PIPE:
            return pipe_client_command(client, argc, argv, retp);
        default:
            log_fatal("invalid client type");
            abort();
    }
}

client_t *
client_new(client_type_t type,
           size_t data_size)
{
    client_t * client = malloc(sizeof(client_t) + data_size);
    if (client == NULL)
    {
        log_error_errno("could not allocate memory");
        return NULL;
    }
    client->type = type;
    return client;
}

void NONNULL
client_destroy(client_t * client)
{
    free(client);
    return;
}

void * NONNULL
client_data(client_t * client)
{
    return client->data;
}
