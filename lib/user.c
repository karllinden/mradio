/*
 * This file is part of mradio.
 *
 * Copyright (C) 2014-2016 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#define MRADIO_LOG_DOMAIN MRADIO_LOG_DOMAIN_USER

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <errno.h>
#include <pwd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>

#include <mradio/common.h>
#include <mradio/log.h>
#include <mradio/mstr.h>
#include <mradio/user.h>

#define USERDIR          "/." PACKAGE "/"

int NONNULL
uid_from_user(const char * const user, uid_t * const uidp)
{
    errno = 0;
    struct passwd * const p = getpwnam(user);
    if (p == NULL) {
        if (errno == 0) {
            log_fatal("could not find user \"%s\"", user);
        } else {
            log_fatal_errno("could not get information about user "
                            "\"%s\"", user);
        }
        return 1;
    }
    *uidp = p->pw_uid;
    return 0;
}

char * NONNULL
userfile(const char * const file)
{
    char *       ret;
    char *       str;
    const char * home;
    size_t       home_len;
    size_t       dir_len;
    size_t       file_len;

    home = getenv("HOME");
    if (home == NULL) {
        home = ".";
    }

    home_len = strlen(home);
    dir_len  = strlen(USERDIR);
    file_len = strlen(file);

    str = malloc(home_len + dir_len + file_len + 1);
    if (str == NULL) {
        log_fatal_errno("could not allocate memory");
        return NULL;
    }
    memcpy(str, home, home_len);
    memcpy(str + home_len, USERDIR, dir_len);
    memcpy(str + home_len + dir_len, file, file_len + 1);

    ret = mstr(str);
    free(str);
    return ret;
}
