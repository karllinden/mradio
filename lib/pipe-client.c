/*
 * This file is part of mradio.
 *
 * Copyright (C) 2016-2017 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#define MRADIO_LOG_DOMAIN MRADIO_LOG_DOMAIN_CLIENT

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <assert.h>
#include <errno.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>

#include <mradio/common.h>
#include <mradio/log.h>
#include <mradio/mstr.h>
#include <mradio/sock.h>

#include <common/log-debug.h>
#include <common/proto.h>

#include "client.h"
#include "pipe-client.h"
#include "sigpipe.h"

struct pipe_client_s
{
    int                        rdfd;
    int                        wrfd;
    pipe_client_close_func_t * close_func;

    uint32_t                   version;
};
typedef struct pipe_client_s pipe_client_t;

static int NONNULL
pipe_client_read(pipe_client_t * pc,
                 void * buf,
                 size_t siz)
{
    char * cbuf = buf;
    ssize_t ret;

    assert(siz <= SSIZE_MAX);

    log_debug("reading %lu bytes", siz);

    do {
        ret = read(pc->rdfd, cbuf, siz);
        if (ret < 0)
        {
            if (errno == EINTR)
            {
                continue;
            }
            else
            {
                log_error_errno("could not read data");
                return 1;
            }
        }
        else if (ret == 0)
        {
            log_error("could not read data: server hungup");
            return 1;
        }
        else
        {
            assert((size_t)ret <= siz);
            siz -= (size_t)ret;
            cbuf += ret;
        }
    } while (siz > 0);

    log_debug("done reading");

    return 0;
}

static char * NONNULL
pipe_client_read_string(pipe_client_t * pc)
{
    uint16_t len;
    char *   str;

    if (pipe_client_read(pc, &len, sizeof(uint16_t)))
    {
        return NULL;
    }

    str = malloc((size_t)len + 1);
    if (str == NULL)
    {
        log_error_errno("could not allocate memory");
        return NULL;
    }

    if (pipe_client_read(pc, str, len))
    {
        free(str);
        return NULL;
    }
    str[len] = '\0';

    return str;
}

static int NONNULL
pipe_client_read_log(pipe_client_t * pc)
{
    int          retval = 1;
    char *       msg    = NULL;
    log_domain_t domain;
    log_level_t  level;

    if (pipe_client_read(pc, &domain, sizeof(log_domain_t)) ||
        pipe_client_read(pc, &level, sizeof(log_level_t)))
    {
        goto error;
    }

    msg = pipe_client_read_string(pc);
    if (msg == NULL)
    {
        goto error;
    }

    log_print_real(0, domain, level, msg);

    retval = 0;
error:
    free(msg);
    return retval;
}

/* SIGPIPE */
static int NONNULL
pipe_client_write(pipe_client_t * pc,
                  const void * buf,
                  size_t siz)
{
    const char * cbuf = buf;
    ssize_t      ret;

    log_debug("writing %lu bytes", siz);

    do {
        ret = write(pc->wrfd, cbuf, siz);
        if (ret < 0)
        {
            if (errno == EINTR)
            {
                log_debug("EINTR");
                continue;
            }
            else
            {
                log_error_errno("could not write data");
                return 1;
            }
        }
        else
        {
            assert((size_t)ret <= siz);
            siz -= (size_t)ret;
            cbuf += ret;
        }
    } while (siz > 0);

    log_debug("done writing");

    return 0;
}

/* SIGPIPE */
static int NONNULL
pipe_client_write_string(pipe_client_t * pc,
                         const char * str)
{
    uint8_t len = (uint8_t)strlen(str);
    return pipe_client_write(pc, &len, sizeof(uint8_t)) ||
           pipe_client_write(pc, str, len);
}

client_t *
pipe_client_open(int rdfd,
                 int wrfd,
                 const char * password,
                 pipe_client_close_func_t * close_func)
{
    sigpipe_state_t spstate;
    client_t *      client;
    pipe_client_t * pc;
    uint8_t         begin   = PROTO_BEGIN;
    uint32_t        version = PROTO_VERSION;
    uint8_t         res;

    client = client_new(CLIENT_PIPE, sizeof(pipe_client_t));
    if (client == NULL)
    {
        return NULL;
    }

    sigpipe_block(&spstate);

    pc = client_data(client);
    pc->rdfd = rdfd;
    pc->wrfd = wrfd;
    pc->close_func = NULL;

    log_debug("writing begin to server");
    if (pipe_client_write(pc, &begin, sizeof(uint8_t)))
    {
        goto error;
    }

    log_debug("reading server version");
    if (pipe_client_read(pc, &pc->version, sizeof(uint32_t)))
    {
        goto error;
    }
    log_debug("server version is %u", pc->version);

    log_debug("writing client version %u", version);
    if (pipe_client_write(pc, &version, sizeof(uint32_t)))
    {
        goto error;
    }

    if (pc->version > PROTO_VERSION)
    {
        pc->version = PROTO_VERSION;
    }

    if (pc->version < PROTO_LEAST_VERSION)
    {
        log_error("deprecated server version");
        goto error;
    }

    log_debug("reading server response");
    if (pipe_client_read(pc, &res, sizeof(uint8_t)))
    {
        goto error;
    }
    if (res == PROTO_DEPRECATED)
    {
        log_error("deprecated client version");
        goto error;
    }
    else if (res == PROTO_PASSWORD)
    {
        if (password == NULL)
        {
            log_error("password authentication required, but no "
                      "password given");
            goto error;
        }
        if (pipe_client_write_string(pc, password) ||
            pipe_client_read(pc, &res, sizeof(uint8_t)))
        {
            goto error;
        }
        if (res == PROTO_PASSWORD)
        {
            log_error("invalid password");
        }
        else if (res != PROTO_AUTH)
        {
            log_error("unknown response from server");
            goto error;
        }
    }
    else if (res == PROTO_AUTH)
    {
        if (password != NULL)
        {
            log_warn("no password authentication required, but a "
                     "password was given");
        }
    }
    else
    {
        log_error("unknown response from server");
        goto error;
    }

    /* Set close_func as late as possible, so that the close function is
     * never called when an error occurs in this function. If it would
     * be called the behaviour on error would be inconsistent, since an
     * error in client_new does not trigger the function whereas an
     * error later on which invokes pipe_client_close would. */
    pc->close_func = close_func;

    if (0)
    {
    error:
        pipe_client_close(client);
        client = NULL;
    }

    sigpipe_unblock(&spstate);

    return client;
}

void NONNULL
pipe_client_close(client_t * client)
{
    pipe_client_t * pc;
    sigpipe_state_t state;
    uint8_t         end = PROTO_END;

    pc = client_data(client);

    sigpipe_block(&state);
    log_debug("writing end to server");
    pipe_client_write(pc, &end, sizeof(uint8_t));
    sigpipe_unblock(&state);

    if (pc->close_func != NULL)
    {
        (*pc->close_func)(client);
    }
    client_destroy(client);

    return;
}

int
pipe_client_rdfd(client_t * client)
{
    pipe_client_t * pc = client_data(client);
    return pc->rdfd;
}

int
pipe_client_wrfd(client_t * client)
{
    pipe_client_t * pc = client_data(client);
    return pc->wrfd;
}

static return_t * NONNULL
pipe_client_read_return(pipe_client_t * pc)
{
    uint32_t   len;
    return_t * ret = NULL;
    return_t * arr = NULL; /* nested array */
    char *     str = NULL;
    char *     m   = NULL; /* mstr */
    int8_t     res;

    if (pipe_client_read(pc, &len, sizeof(uint32_t)))
    {
        goto error;
    }
    ret = return_new(len);
    if (!ret)
    {
        goto error;
    }

    for (uint32_t i = 0; i < len; ++i)
    {
        if (pipe_client_read(pc, &res, sizeof(uint8_t)))
        {
            goto error;
        }
        switch (res)
        {
            case PROTO_STATUS:
                log_error("server sent PROTO_STATUS too early");
                goto error;
            case PROTO_RETURN_ARRAY:
                arr = pipe_client_read_return(pc);
                if (!arr)
                {
                    goto error;
                }
                return_set(ret, i, RETURN_TYPE_ARRAY, arr);
                return_unref(arr);
                break;
            case PROTO_RETURN_STRING:
                str = pipe_client_read_string(pc);
                if (!str)
                {
                    goto error;
                }
                m = mstr(str);
                free(str);
                if (!m)
                {
                    goto error;
                }
                return_set(ret, i, RETURN_TYPE_STRING, m);
                mstr_unref(m);
                break;
            case PROTO_RETURN_NONE:
                log_warn("received PROTO_RETURN_NONE");
                break;
            case PROTO_LOG:
                if (pipe_client_read_log(pc))
                {
                    goto error;
                }
                break;
            default:
                log_error("invalid server response");
                goto error;
        }
    }


    return ret;
error:
    return_unref(ret);
    return NULL;
}

int32_t NONNULL
pipe_client_command(client_t * client,
                    uint8_t argc,
                    const char ** argv,
                    return_t ** retp)
{
    int32_t         retval = -1;
    sigpipe_state_t spstate;
    pipe_client_t * pc;
    uint8_t         token = PROTO_CMD;
    uint8_t         res;

    sigpipe_block(&spstate);
    pc = client_data(client);

    if (pipe_client_write(pc, &token, sizeof(uint8_t)) ||
        pipe_client_write(pc, &argc, sizeof(uint8_t)))
    {
        goto end;
    }
    for (uint8_t i = 0; i < argc; ++i)
    {
        if (pipe_client_write_string(pc, argv[i]))
        {
            goto end;
        }
    }

    *retp = NULL;
    for (;;)
    {
        if (pipe_client_read(pc, &res, sizeof(uint8_t)))
        {
            goto end;
        }
        switch (res)
        {
            case PROTO_STATUS:
                if (pipe_client_read(pc, &retval, sizeof(int32_t)))
                {
                    goto end;
                }
                goto end;
            case PROTO_RETURN_ARRAY:
                if (*retp)
                {
                    log_error("received multiple return arrays");
                    goto end;
                }
                *retp = pipe_client_read_return(pc);
                if (!*retp)
                {
                    goto end;
                }
                break;
            case PROTO_LOG:
                if (pipe_client_read_log(pc))
                {
                    goto end;
                }
                break;
            default:
                log_error("invalid server response");
                goto end;
        }
    }

end:
    sigpipe_unblock(&spstate);
    return retval;
}
