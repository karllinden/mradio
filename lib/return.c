/*
 * This file is part of mradio.
 *
 * Copyright (C) 2017 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#define MRADIO_LOG_DOMAIN MRADIO_LOG_DOMAIN_RETURN

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <stdatomic.h>
#include <stdlib.h>
#include <string.h>

#include <mradio/log.h>
#include <mradio/mstr.h>
#include <mradio/return.h>

struct mradio_return_data_s
{
    mradio_return_type_t type;
    void *               ptr;
};

struct mradio_return_s
{
    atomic_uint          ref;
    unsigned             len;
    mradio_return_data_t data[];
};

return_t *
return_new(unsigned len)
{
    return_t * r;

    if (len == 0)
    {
        log_error("zero length return array is not allowed");
        return NULL;
    }

    r = malloc(sizeof(return_t) + len * sizeof(return_data_t));
    if (!r)
    {
        log_error_errno("could not allocate memory");
        return NULL;
    }

    atomic_init(&r->ref, 1);
    r->len = len;
    memset(r->data, 0, len * sizeof(return_data_t));

    return r;
}

return_t *
return_ref(return_t * r)
{
    if (r)
    {
        atomic_fetch_add(&r->ref, 1);
    }
    return r;
}

static void NONNULL
return_data_unref(return_data_t * d)
{
    switch (d->type)
    {
        case RETURN_TYPE_ARRAY:
            return_unref(d->ptr);
            break;
        case RETURN_TYPE_STRING:
            mstr_unref(d->ptr);
            break;
        default:
            break;
    }
    return;
}


static void NONNULL
return_free(return_t * r)
{
    for (unsigned i = 0; i < r->len; ++i)
    {
        return_data_unref(r->data + i);
    }
    free(r);
    return;
}

void
return_unref(return_t * r)
{
    unsigned ref;
    if (r)
    {
        ref = atomic_fetch_sub(&r->ref, 1);
        if (ref == 1)
        {
            return_free(r);
        }
    }
    return;
}

unsigned NONNULL
return_len(const return_t * r)
{
    return r->len;
}

static void *
return_ptr_ref(void * ptr, return_type_t type)
{
    switch (type)
    {
        case RETURN_TYPE_ARRAY:
            return return_ref(ptr);
        case RETURN_TYPE_STRING:
            return mstr_ref(ptr);
        default:
            return ptr;
    }
}

void NONNULL
return_set(return_t * r,
           unsigned index,
           return_type_t type,
           void * ptr)
{
    if (index >= r->len)
    {
        log_error("return array out of range");
        return;
    }

    return_ptr_ref(ptr, type);
    r->data[index].type = type;
    r->data[index].ptr  = ptr;

    return;
}

void * NONNULL
return_get(const return_t * r,
           unsigned index,
           return_type_t * typep)
{
    return_type_t type;
    void *        ptr;

    if (index >= r->len)
    {
        log_error("return array out of range");
        return NULL;
    }

    type = r->data[index].type;
    ptr  = r->data[index].ptr;
    *typep = type;
    return return_ptr_ref(ptr, type);
}
