/*
 * This file is part of mradio.
 *
 * Copyright (C) 2016-2017 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#ifndef CLIENT_H
# define CLIENT_H 1

# if HAVE_CONFIG_H
#  include <config.h>
# endif /* HAVE_CONFIG_H */

# include <stdint.h>

# include <mradio/common.h>
# include <mradio/info.h>
# include <mradio/return.h>

enum client_type_e
{
    CLIENT_PIPE
};
typedef enum client_type_e client_type_t;

typedef struct client_s client_t;

/* To be called from outside client code. */
client_t * client_open   (const info_t * info) NONNULL;
void       client_close  (client_t * client)   NONNULL;

int32_t    client_command(client_t * client,
                          uint8_t argc,
                          const char ** argv,
                          return_t ** retp) NONNULL;

/* To be called by client implementations. */
client_t * client_new    (client_type_t type,
                          size_t data_size);
void       client_destroy(client_t * client)   NONNULL;
void *     client_data   (client_t * client)   NONNULL;

#endif /* !CLIENT_H */
