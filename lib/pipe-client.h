/*
 * This file is part of mradio.
 *
 * Copyright (C) 2016-2017 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

/* FIXME: make sure callers can store data in the pipe client when that
 *        becomes necessary */

#ifndef PIPE_CLIENT_H
# define PIPE_CLIENT_H 1

# if HAVE_CONFIG_H
#  include <config.h>
# endif /* HAVE_CONFIG_H */

# include <stdint.h>

typedef void (pipe_client_close_func_t)(client_t * client);

client_t * pipe_client_open   (int rdfd,
                               int wrfd,
                               const char * password,
                               pipe_client_close_func_t * close_func);
void       pipe_client_close  (client_t * client) NONNULL;

int32_t    pipe_client_command(client_t * client,
                               uint8_t argc,
                               const char ** argv,
                               return_t ** retp) NONNULL;

int        pipe_client_rdfd   (client_t * client) NONNULL;
int        pipe_client_wrfd   (client_t * client) NONNULL;

#endif /* !PIPE_CLIENT_H */
