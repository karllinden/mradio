/*
 * This file is part of mradio.
 *
 * Copyright (C) 2014-2016 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#define MRADIO_LOG_DOMAIN MRADIO_LOG_DOMAIN_SOCK

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <errno.h>
#include <netdb.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/un.h>

#include <mradio/log.h>
#include <mradio/mstr.h>
#include <mradio/sock.h>

static int NONNULL
sock_parse_address(const char * const address, struct in_addr * addr)
{
    struct addrinfo hints;
    hints.ai_flags = 0;
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = 0;
    hints.ai_addrlen = 0;
    hints.ai_addr = NULL;
    hints.ai_canonname = NULL;
    hints.ai_addr = NULL;
    hints.ai_next = NULL;

    struct addrinfo * result;
    int error = getaddrinfo(address, NULL, &hints, &result);
    if (error != 0) {
        log_fatal("getaddrinfo: %s", gai_strerror(error));
        return 1;
    } else if (result == NULL) {
        log_fatal("getaddrinfo returned no result");
        return 1;
    }

    struct sockaddr_in * resaddr;
    resaddr = (struct sockaddr_in *)result->ai_addr;
    memcpy(addr, &resaddr->sin_addr, sizeof(struct in_addr));

    freeaddrinfo(result);

    return 0;
}

static int NONNULL
sock_parse_port(const char * const port, in_port_t * sin_port)
{
    char * endptr;
    unsigned long ul = 0;

    errno = 0;
    ul = strtoul(port, &endptr, 0);
    if (ul >= UINT16_MAX) {
        errno = ERANGE;
    }
    if (errno != 0) {
        log_fatal_errno("could not convert %s to a port number", port);
        return 1;
    } else if (*endptr != '\0') {
        log_fatal("could not convert %s to a port number", port);
        return 1;
    }

    *sin_port = htons((uint16_t)ul);

    return 0;
}

static int NONNULL
sock_parse_path(const char * const path, char sun_path[SOCK_PATH_MAX])
{
    const unsigned int len = (unsigned int)strlen(path);
    if (len >= SOCK_PATH_MAX) {
        log_fatal("the path %s is too long (%u, but max is %u)", path,
                  len, SOCK_PATH_MAX);
        return 1;
    }

    memcpy(sun_path, path, len + 1);

    return 0;
}

static int NONNULL
get_prop(const info_t * info,
         const char * prop,
         char ** valp)
{
    *valp = info_get_prop(info, prop);
    if (*valp == NULL)
    {
        *valp = info_default(prop);
        if (*valp == NULL)
        {
            return 1;
        }
    }
    return 0;
}

static int NONNULL
sockinfo_fill_inet(sockinfo_t * sockinfo,
                   const info_t * info)
{
    int    retval  = 1;
    char * addr    = NULL;
    char * port    = NULL;

    if (get_prop(info, "addr", &addr) ||
        get_prop(info, "port", &port))
    {
        goto error;
    }

    if (sock_parse_address(addr, &sockinfo->sockaddr.in.sin_addr) ||
        sock_parse_port(port, &sockinfo->sockaddr.in.sin_port))
    {
        goto error;
    }

    retval = 0;
error:
    mstr_unref(addr);
    mstr_unref(port);
    return retval;
}

static int
sockinfo_fill_unix(sockinfo_t * sockinfo,
                   const info_t * info)
{
    int    retval  = 1;
    char * path    = NULL;

    if (get_prop(info, "unix", &path))
    {
        goto error;
    }

    if (sock_parse_path(path, sockinfo->sockaddr.un.sun_path)) {
        goto error;
    }

    retval = 0;
error:
    mstr_unref(path);
    return retval;
}

int NONNULL
sockinfo_fill(sockinfo_t * sockinfo,
              const info_t * info)
{
    info_type_t  type;

    type = info_get_type(info);
    switch (type)
    {
        case INFO_TYPE_INET:
            sockinfo->sockaddr.addr.sa_family = AF_INET;
            sockinfo->size = sizeof(struct sockaddr_in);
            return sockinfo_fill_inet(sockinfo, info);
        case INFO_TYPE_UNIX:
            sockinfo->sockaddr.addr.sa_family = AF_UNIX;
            sockinfo->size = sizeof(struct sockaddr_un);
            return sockinfo_fill_unix(sockinfo, info);
        default:
            log_fatal("invalid server type %s", info_strtype(type));
            return 1;
    }
}
