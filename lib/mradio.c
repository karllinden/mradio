/*
 * This file is part of mradio.
 *
 * Copyright (C) 2016-2017 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#define MRADIO_LOG_DOMAIN MRADIO_LOG_DOMAIN_CLIENT

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <stdlib.h>
#include <unistd.h>

#include <mradio/common.h>
#include <mradio/log.h>
#include <mradio/mradio.h>
#include <mradio/sock.h>
#include <mradio/return.h>

#include "client.h"
#include "pipe-client.h"

struct mradio_s
{
    client_t * client;
};

mradio_t *
mradio_spawn(void)
{
    log_error("spawning not implemented");
    return NULL;
}

mradio_t *
mradio_spawn_priv(void)
{
    /* FIXME: set variables so that the server spawned is on stdio */
    log_error("spawning not implemented");
    return NULL;
}

mradio_t *
mradio_connect(mradio_info_t * info,
               int autospawn)
{
    client_t * client = NULL;
    mradio_t * mradio = NULL;
    info_t     myinfo;

    info_init(&myinfo);

    if (info == NULL)
    {
        info = &myinfo;
    }

    client = client_open(info);
    if (client == NULL)
    {
        if (autospawn)
        {
            mradio = mradio_spawn_priv();
        }
        goto end;
    }

    mradio = malloc(sizeof(mradio_t));
    if (mradio == NULL)
    {
        client_close(client);
        log_error_errno("could not allocate memory");
        return NULL;
    }

    mradio->client = client;

end:
    info_deinit(&myinfo);
    return mradio;
}

mradio_t *
mradio_stdio(void)
{
    client_t * client = NULL;
    mradio_t * mradio = NULL;

    client = pipe_client_open(STDIN_FILENO, STDOUT_FILENO, NULL, NULL);
    if (client == NULL)
    {
        goto end;
    }

    mradio = malloc(sizeof(mradio_t));
    if (mradio == NULL)
    {
        pipe_client_close(client);
        log_error_errno("could not allocate memory");
        return NULL;
    }

    mradio->client = client;
end:
    return mradio;
}

void
mradio_disconnect(mradio_t * mradio)
{
    if (mradio == NULL)
    {
        return;
    }
    /* FIXME: if the server is spawned make sure it stops */
    client_close(mradio->client);
    free(mradio);
    return;
}

int NONNULL
mradio_command(mradio_t * mradio,
               uint8_t argc,
               const char * argv[],
               return_t ** retp)
{
    return client_command(mradio->client, argc, argv, retp);
}
