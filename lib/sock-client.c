/*
 * This file is part of mradio.
 *
 * Copyright (C) 2016 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#define MRADIO_LOG_DOMAIN MRADIO_LOG_DOMAIN_CLIENT

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#include <mradio/common.h>
#include <mradio/info.h>
#include <mradio/log.h>
#include <mradio/mstr.h>
#include <mradio/sock.h>

#include "client.h"
#include "pipe-client.h"
#include "sock-client.h"

static void
sock_client_close_fd(int fd)
{
    if (shutdown(fd, SHUT_RDWR)) {
        log_error_errno("could not shutdown connection");
    }
    if (close(fd)) {
        log_error_errno("could not close socket");
    }
    return;
}

static void NONNULL
sock_client_close(client_t * client)
{
    int fd;
    fd = pipe_client_rdfd(client);
    sock_client_close_fd(fd);
    return;
}

client_t * NONNULL
sock_client_open(const info_t * info)
{
    int        fd;
    sockinfo_t sockinfo;
    char *     password;
    client_t * client;

    if (sockinfo_fill(&sockinfo, info))
    {
        return NULL;
    }

    fd = socket(sockinfo.sockaddr.addr.sa_family, SOCK_STREAM, 0);
    if (fd < 0)
    {
        log_error_errno("could not create socket");
        return NULL;
    }

    if (connect(fd, &sockinfo.sockaddr.addr, sockinfo.size))
    {
        log_error_errno("could not connect socket");
        if (close(fd))
        {
            log_error_errno("could not close socket");
        }
        return NULL;
    }

    password = info_get_password(info);
    client = pipe_client_open(fd, fd, password, sock_client_close);
    mstr_unref(password);
    if (client == NULL)
    {
        sock_client_close_fd(fd);
        return NULL;
    }

    return client;
}
