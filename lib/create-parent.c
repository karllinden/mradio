/*
 * This file is part of mradio.
 *
 * Copyright (C) 2016 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#define MRADIO_LOG_DOMAIN MRADIO_LOG_DOMAIN_COMMON

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>

#include <mradio/common.h>
#include <mradio/create-parent.h>
#include <mradio/log.h>

#include <common/massert.h>

#define MODE (S_IRWXU | S_IRWXG | S_IRWXO)

int NONNULL
create_parent(const char * filename)
{
    int          retval = 1;
    const char * c;
    size_t       len;
    char *       copy;
    char *       stop; /* pointer to the last byte in copy */
    char *       end;

    /* Do some initial checking. Ignore filenames with just a plain
     * filename or on the form /somefile. Those cases need not be
     * treated. */
    c = filename;
    while (*c == '/') {
        ++c;
    }
    if (strchr(filename, '/') == NULL) {
        /* Just a plain file with no directory component. */
        return 0;
    }

    len = strlen(filename);
    copy = malloc(len + 1);
    if (copy == NULL) {
        log_error_errno("could not allocate memory");
        goto error;
    }
    memcpy(copy, filename, len + 1);
    stop = copy + len;

    do {
        /* Do not assume mkdir sets errno on success. This is checked
         * after the loop. */
        errno = 0;

        end = strrchr(copy, '/');

        /* end != NULL can never happen since firstly the string must
         * contain a slash (that was checked earlier) so this call does
         * not fail on the first run. If it fails in any subsequent run
         * then copy is exhausted of slashes and there is some initial
         * directory component (end != copy in all previous runs), but
         * then the mkdir call should either have succeeded or failed.
         * In the first case the loop stops. In the latter case errno
         * can impossibly be ENOENT, since the copy was a relative path
         * with no slashes. Therefore the loop must have stopped before
         * this end == NULL. */
        massert(end != NULL);
        *end = '\0';

        /* Also this should never happen. */
        massert(end != copy);
    } while (mkdir(copy, MODE) && errno == ENOENT);
    if (errno != 0 && errno != EEXIST) {
        log_error_errno("could not create directory %s", copy);
        goto error;
    }

    do {
        *end = '/';
        end = rawmemchr(end + 1, '\0');
    } while (end != stop && mkdir(copy, MODE) == 0);
    if (end != stop) {
        log_error_errno("could not create directory %s", copy);
        goto error;
    }

    retval = 0;
error:
    free(copy);
    return retval;
}
