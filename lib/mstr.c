/*
 * This file is part of mradio.
 *
 * Copyright (C) 2016-2017 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#define MRADIO_LOG_DOMAIN MRADIO_LOG_DOMAIN_MSTR

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <stdatomic.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>

#include <mradio/common.h>
#include <mradio/log.h>
#include <mradio/mstr.h>

#include <common/log-debug.h>

struct mstr_s
{
    atomic_uint ref;
    unsigned    len;
    char        str[];
};
typedef struct mstr_s mstr_t;

static inline mstr_t * NONNULL
mstr_char_ptr_to_obj(char * p)
{
    return (mstr_t *)(p - offsetof(mstr_t, str));
}

char *
mstr_alloc(unsigned len)
{
    mstr_t * m;

    /* This can be interesting. */
    log_debug("sizeof(mstr_t) = %ld", sizeof(mstr_t));

    m = malloc(sizeof(mstr_t) + len + 1);
    if (m == NULL)
    {
        log_error_errno("could not allocate mstr");
        return NULL;
    }

    atomic_init(&m->ref, 1);
    m->len = len;

    return m->str;
}

char * NONNULL
mstr_with_len(const char * s,
              unsigned len)
{
    mstr_t * m;
    char *   p;
    p = mstr_alloc(len);
    if (p)
    {
        m = mstr_char_ptr_to_obj(p);
        memcpy(m->str, s, len);
        m->str[len] = '\0';
    }
    return p;
}

char * NONNULL
mstr(const char * s)
{
    unsigned len;
    len = (unsigned)strlen(s);
    return mstr_with_len(s, len);
}

char *
mstr_ref(char * p)
{
    mstr_t * m;
    if (p != NULL)
    {
        m = mstr_char_ptr_to_obj(p);
        atomic_fetch_add(&m->ref, 1);
    }
    return p;
}

void
mstr_unref(char * p)
{
    mstr_t * m;
    unsigned ref;
    if (p != NULL)
    {
        m = mstr_char_ptr_to_obj(p);
        ref = atomic_fetch_sub(&m->ref, 1);
        if (ref == 1)
        {
            free(m);
        }
    }
    return;
}

unsigned NONNULL PURE
mstr_len(char * p)
{
    mstr_t * m = mstr_char_ptr_to_obj(p);
    return m->len;
}
