/*
 * This file is part of mradio.
 *
 * Copyright (C) 2016 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#define MRADIO_LOG_DOMAIN MRADIO_LOG_DOMAIN_CLIENT

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <assert.h>
#include <errno.h>
#include <signal.h>
#include <time.h>

#include <mradio/common.h>

#include <common/call.h>

#include "sigpipe.h"

void NONNULL
sigpipe_block(sigpipe_state_t * state)
{
    sigset_t pending;
    sigset_t set;
    int      ret;

    CALL(sigpending, &pending);
    ret = sigismember(&pending, SIGPIPE);
    if (0)
    {
        /* empty */
    }
#if !NDEBUG
    else if (ret < 0)
    {
        log_error_errno("sigismember");
        abort();
    }
#endif /* !NDEBUG */
    else if (ret)
    {
        /* The signal is pending so it must already be blocked, thus no
         * call to pthread_sigmask is necessary. Also the library should
         * not wait for the signal, since then the library has consumed
         * a signal which was not directed to it. Therefore the
         * functions here can be made no-ops. */
        state->noop = 1;
    }
    else
    {
        /* The signal is not pending, so it may not be blocked. Thus,
         * make sure it is blocked with pthread_sigmask. Since there is
         * no signal pendnig now, the signal must be waited for when
         * unblocking so that the program does not see a signal which is
         * directed to this library. */
        state->noop = 0;
        CALL(sigemptyset, &set);
        CALL(sigaddset, &set, SIGPIPE);
        PCALL(pthread_sigmask, SIG_BLOCK, &set, &state->old);
    }

    return;
}

void NONNULL
sigpipe_unblock(sigpipe_state_t * state)
{
    sigset_t       set;
    struct timespec timeout;
    if (!state->noop)
    {
        CALL(sigemptyset, &set);
        CALL(sigaddset, &set, SIGPIPE);
        timeout.tv_sec = 0;
        timeout.tv_nsec = 0;
#if !NDEBUG
        errno = 0;
#endif /* !NDEBUG */
        while (sigtimedwait(&set, NULL, &timeout) < 0 && errno == EINTR)
        {
            ;
        }
        assert(errno != EINVAL);
        PCALL(pthread_sigmask, SIG_SETMASK, &state->old, NULL);
    }
    return;
}

