#
# This file is part of mradio.
#
# Copyright (C) 2015-2016 Karl Linden <karl.j.linden@gmail.com>
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA 02110-1301, USA.
# 

from mradio_waf.msg import print_error

class Deps:
    """
    This class is used to ease handling third party dependencies.
    """

    def __init__(self, name):
        # check hook to call upon configuration
        self.check_hook = None
        self.check_hook_error = None
        self.check_hook_found = True

        # required libraries
        self.libs = [] # elements on the form [lib,uselib_store]
        self.libs_not_found = [] # elements on the form lib

        # required headers
        self.headers = []
        self.headers_not_found = []

        # required packages (checked with pkg-config)
        # elements in packages are on the form
        # [package,uselib_store,atleast_version]
        # elements in packages_not_found are on the form
        # [package,atleast_version]
        self.packages = [] 
        self.packages_not_found = []

        # required programs
        self.programs = [] # elements on the form [program,var]
        self.programs_not_found = [] # elements on the form program

        # the result of the configuration (are all third party
        # dependencies available)
        self.result = False

        self.name = name

    def add_library(self, library, uselib_store=None):
        """
        Add a required library that should be checked during
        configuration. The library will be checked using the
        conf.check_cc function. If the uselib_store arugment is not
        given it defaults to LIBRARY (the uppercase of the library
        argument). The uselib_store argument will be passed to check_cc
        which means LIB_LIBRARY, CFLAGS_LIBRARY and DEFINES_LIBRARY,
        etc. will be defined if the option is enabled.
        """
        if not uselib_store:
            uselib_store = library.upper().replace('-', '_')
        self.libs.append([library, uselib_store])

    def add_header(self, header):
        """
        Add a required header that should be checked during
        configuration. The header will be checked using the
        conf.check_cc function which means HAVE_HEADER_H will be defined
        if found.
        """
        self.headers.append(header)

    def add_package(self, package, uselib_store=None,
                    atleast_version=None):
        """
        Add a required package that should be checked using pkg-config
        during configuration. The package will be checked using the
        conf.check_cfg function and the uselib_store and atleast_version
        will be passed to check_cfg. If uselib_store is None it defaults
        to PACKAGE (uppercase of the package argument) with hyphens and
        dots replaced with underscores. If atleast_version is None it
        defaults to '0'.
        """
        if not uselib_store:
            uselib_store = package.upper().replace('-', '_')
            uselib_store = uselib_store.replace('.', '_')
        if not atleast_version:
            atleast_version = '0'
        self.packages.append([package, uselib_store, atleast_version])

    def add_program(self, program, var=None):
        """
        Add a required program that should be checked during
        configuration. If var is not given it defaults to PROGRAM (the
        uppercase of the program argument). If the option is enabled the
        program is saved in conf.env.PROGRAM.
        """
        if not var:
            var = program.upper().replace('-', '_')
        self.programs.append([program, var])

    def set_check_hook(self, check_hook, check_hook_error):
        """
        Set the check hook and the corresponding error printing function
        to the configure step. The check_hook argument is a function
        that should return True if the extra prerequisites were found
        and False if not. The check_hook_error argument is an error
        printing function that should print an error message telling the
        user that --foo was explicitly requested but cannot be built
        since the extra prerequisites were not found. Both function
        should take a single argument that is the waf configuration
        context.
        """
        self.check_hook = check_hook
        self.check_hook_error = check_hook_error

    def _missing(self, kind, what):
        """
        This function prints an error message telling the user that what
        is missing.
        """
        print_error('%s requires the %s %s, but it is missing.' %
                    (self.name, kind, what))

    def _error(self, conf):
        """
        This is an internal function that prints errors for each missing
        dependency. The error messages tell the user that this option
        required some dependency, but it cannot be found.
        """

        for lib in self.libs_not_found:
            self._missing('library', lib)

        for header in self.headers_not_found:
            self._missing('header', header)

        for package,atleast_version in self.packages_not_found:
            string = package
            if atleast_version:
                string += ' >= ' + atleast_version
            self._missing('package', string)

        for program in self.programs_not_found:
            self._missing('program', program)

        if not self.check_hook_found:
            self.check_hook_error(conf)

    def check(self, conf, require):
        """
        This function runs all necessary configure checks. It checks all
        dependencies (even if some dependency was not found) so that the
        user can install all missing dependencies in one go, instead of
        playing the infamous hit-configure-hit-configure game.

        The result is set to whether all dependencies or not.

        This function returns True if the check succeeded, which means
        either all dependencies were found or require were False.
        """
        self.result = True

        # check for libraries
        for lib,uselib_store in self.libs:
            try:
                conf.check_cc(lib=lib, uselib_store=uselib_store)
            except conf.errors.ConfigurationError:
                self.result = False
                self.libs_not_found.append(lib)

        # check for headers
        for header in self.headers:
            try:
                conf.check_cc(header_name=header)
            except conf.errors.ConfigurationError:
                self.result = False
                self.headers_not_found.append(header)

        # check for packages
        for package,uselib_store,atleast_version in self.packages:
            try:
                conf.check_cfg(package=package,
                               uselib_store=uselib_store,
                               atleast_version=atleast_version,
                               args='--cflags --libs')
            except conf.errors.ConfigurationError:
                self.result = False
                self.packages_not_found.append([package,atleast_version])

        # check for programs
        for program,var in self.programs:
            try:
                conf.find_program(program, var=var)
            except conf.errors.ConfigurationError:
                self.result = False
                self.programs_not_found.append(program)

        # call hook (if specified)
        if self.check_hook:
            self.check_hook_found = self.check_hook(conf)
            if not self.check_hook_found:
                self.result = False

        if not self.result and require:
            self._error()

        if require:
            return self.result
        else:
            return True
