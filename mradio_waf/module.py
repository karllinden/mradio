#
# This file is part of mradio.
#
# Copyright (C) 2015-2017 Karl Linden <karl.j.linden@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA 02110-1301, USA.
#

from mradio_waf import deps
from mradio_waf.msg import print_error
from waflib import Options

in_main_dist = False
mradio_modules = {}
mradio_builtins = []
mradio_builtins_use = []

class MradioModule(deps.Deps):

    def __init__(self, opt, name):
        deps.Deps.__init__(self, name)

        self.name = name
        self.dest = 'module_' + name
        self.env = 'MRADIO_MODULE_' + name.upper()

        self.sources = []
        self.use = []

        mradio_modules[name] = self

        opt.load('compiler_c')

        if in_main_dist:
            opt.add_option('--' + self. name, type='string',
                           default='auto',
                           dest=self.dest,
                           help='Build ' + self.name + ' module',
                           metavar='builtin|module|no')
        else:
            self.add_package('mradio')

    def add_library(self, library, uselib_store=None):
        if not uselib_store:
            uselib_store = library.upper().replace('-', '_')
        deps.Deps.add_library(self, library, uselib_store)
        self.use.append(uselib_store)

    def add_package(self, package, uselib_store=None,
                    atleast_version=None):
        if not uselib_store:
            uselib_store = package.upper().replace('-', '_')
            uselib_store = uselib_store.replace('.', '_')
        deps.Deps.add_package(self, package, uselib_store,
                              atleast_version)
        self.use.append(uselib_store)

    def add_source(self, source):
        self.sources.append(source)

    def status(self, ctx):
        return ctx.env[self.env]

    def configure(self, conf):
        conf.load('compiler_c')
        conf.load('gnu_dirs')

        if in_main_dist:
            require = False

            argument = getattr(Options.options, self.dest)
            if argument == 'auto':
                conf.env[self.env] = 'builtin'
            elif argument == 'builtin':
                conf.env[self.env] = 'builtin'
                require = True
            elif argument == 'module':
                conf.env[self.env] = 'module'
                require = True
            elif argument == 'no':
                conf.env[self.env] = 'no'
            else:
                print_error('Invalid argument "' + argument + '" to ' +
                            '--' + name)
                return False

            retvalue = True
            if conf.env[self.env] != 'no':
                retvalue = self.check(conf, require=require)
                if not self.result:
                    conf.env[self.env] = 'no'
            return retvalue
        else:
            conf.env[self.env] = 'module'
            return self.check(conf, require=True)

    def build(self, bld):
        use = self.use
        if in_main_dist:
            use.append('mradio')
        else:
            use.append('MRADIO')
        name = 'mradio_module_' + self.name
        if self.status(bld) == 'builtin':
            # Use the 'mm-' prefix to avoid linker flag collisions.
            bld.stlib(
                name         = name,
                target       = 'mm-' + self.name,
                features     = 'c',
                source       = self.sources,
                use          = use,
                install_path = False
            )
            mradio_builtins.append(self.name)
            mradio_builtins_use.append(name)
        elif self.status(bld) == 'module':
            lib = bld(
                features     = 'c cshlib',
                name         = name,
                target       = self.name,
                source       = self.sources,
                use          = use,
                install_path = bld.env['LIBDIR'] + '/mradio'
            )
            lib.env['cshlib_PATTERN'] = '%s.so'
        else:
            pass

def configure(conf, name):
    mradio_modules[name].configure(conf)

def build(bld, name):
    mradio_modules[name].build(bld)
