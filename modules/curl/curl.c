/*
 * This file is part of mradio.
 *
 * Copyright (C) 2015-2017 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <assert.h>

#include <curl/curl.h>

#include <mradio/module.h>

static int  m_curl_init  (void);
static void m_curl_deinit(void);
static int  m_curl_fetch (player_t * player);

/*
 * From the libcurl programming tutorial:
 *   Repeated calls to curl_global_init and curl_global_cleanup should
 *   be avoided. They should only be called once each.
 * If debugging is turned on, make sure the module is initialized and
 * deinitialized only once.
 */
#if !NDEBUG
static int m_curl_inited   = 0;
static int m_curl_deinited = 0;
#endif /* !NDEBUG */

const mradio_fetcher_info_t curl_info = {
    MRADIO_MODULE_TYPE_FETCHER,
    &m_curl_init,
    &m_curl_deinit,
    &m_curl_fetch
};

static int
m_curl_init(void)
{
#if !NDEBUG
    assert(!m_curl_inited);
    m_curl_inited = 1;
#endif /* !NDEBUG */

    if (curl_global_init(CURL_GLOBAL_ALL))
    {
        return 1;
    }

    return 0;
}

static void
m_curl_deinit(void)
{
#if !NDEBUG
    assert(!m_curl_deinited);
    m_curl_deinited = 1;
#endif /* !NDEBUG */

    curl_global_cleanup();
    return;
}

static int
m_curl_fetch(player_t * player)
{
    /* FIXME */
    return 0;
}
